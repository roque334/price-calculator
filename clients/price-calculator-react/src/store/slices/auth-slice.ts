import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Dispatch } from "redux";
import axios, { AxiosError } from "axios";

interface AuthUser {
  firstname: string;
  lastname: string;
  token: string;
}

interface AuthState {
  loginError: string;
  registerError: string;
  error: string;
  token: string;
  user: AuthUser | null;
  loading: boolean;
}

const initialState: AuthState = {
  loginError: "",
  registerError: "",
  error: "",
  token: "",
  user: null,
  loading: true,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    startAuthAction(state, _) {
      state.loading = true;
      state.loginError = "";
      state.registerError = "";
      state.error = "";
    },
    authActionSuccess(state, action: PayloadAction<AuthUser>) {
      axios.defaults.headers.Authorization = `Bearer ${action.payload.token}`;
      localStorage.setItem("token", action.payload.token);
      state.token = action.payload.token;
      state.user = action.payload;
    },
    loginActionFail(state, action: PayloadAction<string>) {
      delete axios.defaults.headers.Authorization;
      state.loginError = action.payload;
      state.token = "";
      state.user = null;
    },
    registerActionFail(state, action: PayloadAction<string>) {
      delete axios.defaults.headers.Authorization;
      state.registerError = action.payload;
      state.token = "";
      state.user = null;
    },
    endAuthAction(state, _) {
      state.loading = false;
    },
    getUserActionSuccess(state, action: PayloadAction<AuthUser>) {
      axios.defaults.headers.Authorization = `Bearer ${action.payload.token}`;
      localStorage.setItem("token", action.payload.token);
      state.token = action.payload.token;
      state.user = action.payload;
    },
    getUserActionFail(state, action: PayloadAction<string>) {
      delete axios.defaults.headers.Authorization;
      state.error = "";
      state.token = "";
      state.user = null;
    },
    cleanErrorsAction(state, _) {
      state.loginError = "";
      state.registerError = "";
      state.error = "";
    }
  },
});

export const authActions = authSlice.actions;

export const authReducers = authSlice.reducer;

export interface registerActionParam {
  firstname: string;
  lastname: string;
  email: string;
  username: string;
  password: string;
}

export const registerAction = (signUpForm: registerActionParam) => {
  return async (dispatch: Dispatch) => {
    const registerRequest = async (
      signUpForm: registerActionParam
    ): Promise<AuthUser> => {
      const registerResponse = await axios.post(
        "http://localhost:5000/api/account/register",
        JSON.stringify(signUpForm),
        { headers: { "Content-Type": "application/json" } }
      );

      return registerResponse.data;
    };

    dispatch(authActions.startAuthAction(null));
    try {
      const user = await registerRequest(signUpForm);
      dispatch(authActions.authActionSuccess(user));
    } catch (error) {
      const err = error as AxiosError<{
        errors: {
          message: string;
        };
      }>;
      dispatch(authActions.registerActionFail(err.response!.data.errors.message));
    } finally {
      dispatch(authActions.endAuthAction(null));
    }
  };
};

export interface loginActionParam {
  email: string;
  password: string;
}

export const loginAction = (signInForm: loginActionParam) => {
  return async (dispatch: Dispatch) => {
    const loginRequest = async (signInForm: loginActionParam) => {
      const loginResponse = await axios.post(
        "http://localhost:5000/api/account/login",
        JSON.stringify(signInForm),
        { headers: { "Content-Type": "application/json" } }
      );

      return loginResponse.data;
    };

    dispatch(authActions.startAuthAction(null));
    try {
      const user = await loginRequest(signInForm);
      dispatch(authActions.authActionSuccess(user));
    } catch (error) {
      const err = error as AxiosError<{
        errors: {
          message: string;
        };
      }>;
      dispatch(authActions.loginActionFail(err.response!.data.errors.message));
    } finally {
      dispatch(authActions.endAuthAction(null));
    }
  };
};

export const getUserAction = () => {
  return async (dispatch: Dispatch) => {
    const getUserRequest = async () => {
      const getUserResponse = await axios.get(
        "http://localhost:5000/api/account"
      );

      return getUserResponse.data;
    };

    if (localStorage.getItem("token")) {
      axios.defaults.headers.Authorization = `Bearer ${localStorage.getItem(
        "token"
      )}`;
    } else {
      delete axios.defaults.headers.Authorization;
    }

    dispatch(authActions.startAuthAction(null));
    try {
      const user = await getUserRequest();
      dispatch(authActions.getUserActionSuccess(user));
    } catch (error) {
      const err = error as AxiosError<{
        errors: {
          message: string;
        };
      }>;
      dispatch(authActions.getUserActionFail(err.message));
    } finally {
      dispatch(authActions.endAuthAction(null));
    }
  };
};
