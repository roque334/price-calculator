import { createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import { GetMaterialsQueryResponse, Material, MaterialForm, MaterialSummarize } from "../../models/material";

interface MaterialState {
  materials: MaterialSummarize[];
  total: number;
  material: Material | null;
  loading: boolean;
  scrollY?: number;
  errorMessage: string;
  successMessage: string;
}

const initialState: MaterialState = {
  materials: [],
  total: 0,
  material: null,
  loading: true,
  scrollY: undefined,
  errorMessage: "",
  successMessage: "",
};

const materialSlice = createSlice({
  name: "material",
  initialState: initialState,
  reducers: {
    startMaterialAction(state: MaterialState, _) {
      state.loading = true;
      state.errorMessage = "";
      state.successMessage = "";
    },
    endMaterialAction(state: MaterialState, _) {
      state.loading = false;
    },
    getMaterialsSuccess(
      state: MaterialState,
      { payload }: PayloadAction<GetMaterialsQueryResponse>
    ) {
      const {materials, total} = payload;
      for (const material of materials) {
        if (!state.materials.some(m => m.id === material.id))
        {
          state.materials.push(material);
        }
      }
      state.total = total;
      state.errorMessage = "";
      state.successMessage = "";
    },
    getMaterialsFail(state: MaterialState, action: PayloadAction<string>) {
      state.errorMessage = action.payload;
      state.successMessage = "";
    },
    getMaterialSuccess(state: MaterialState, action: PayloadAction<Material>) {
      state.material = action.payload;
      state.errorMessage = "";
      state.successMessage = "";
    },
    getMaterialFail(state: MaterialState, action: PayloadAction<string>) {
      state.errorMessage = action.payload;
      state.successMessage = "";
    },
    upsertMaterialSuccess(
      state: MaterialState,
      action: PayloadAction<Material>
    ) {
      const material = action.payload;
      const found = state.materials.find((x) => x.id === material.id);
      if (found) {
        found.price = material.price;
        state.successMessage = "Material updated successfully";
      } else {
        state.materials.unshift({
          id: material.id,
          name: material.name,
          price: material.price,
        });
        state.successMessage = "Material created successfully";
      }
      state.errorMessage = "";
    },
    upsertMaterialFail(state: MaterialState, action: PayloadAction<string>) {
      state.errorMessage = action.payload;
    },
    removeMaterialSuccess(state: MaterialState, action: PayloadAction<number>) {
      const materialId = action.payload;
      state.materials = state.materials.filter((x) => x.id !== materialId);
      state.errorMessage = "";
    },
    removeMaterialFail(state: MaterialState, action: PayloadAction<string>) {
      state.errorMessage = action.payload;
    },
    saveScrollYPosition(state: MaterialState, action: PayloadAction<number>) {
      state.scrollY = action.payload;
    }
  },
});

const baseUrl = "http://localhost:5001";

export const getMaterialsThunk = (skip: number = 0, take: number = 20) => {
  return async (dispatch: Dispatch) => {
    const getMaterialsRequest = async (): Promise<GetMaterialsQueryResponse> => {
      const registerResponse = await axios.get(`${baseUrl}/api/materials?skip=${skip}&take=${take}`);

      return registerResponse.data;
    };

    dispatch(materialSlice.actions.startMaterialAction(null));
    try {
      const response = await getMaterialsRequest();
      dispatch(materialSlice.actions.getMaterialsSuccess(response));
    } catch (error) {
      console.log(error)
      const err = error as AxiosError;
      dispatch(
        materialSlice.actions.getMaterialsFail(
          err.response!.data.errors.message
        )
      );
    } finally {
      dispatch(materialSlice.actions.endMaterialAction(null));
    }
  };
};

export const getMaterialByIdThunk = (materiaId: number) => {
  return async (dispatch: Dispatch) => {
    const getMaterialRequest = async (materiaId: number): Promise<Material> => {
      const registerResponse = await axios.get(
        `${baseUrl}/api/materials/${materiaId}`
      );

      return registerResponse.data;
    };

    dispatch(materialSlice.actions.startMaterialAction(null));
    try {
      const material = await getMaterialRequest(materiaId);
      dispatch(materialSlice.actions.getMaterialSuccess(material));
    } catch (error) {
      const err = error as AxiosError;
      dispatch(
        materialSlice.actions.getMaterialFail(
          err.response!.data.errors.message
        )
      );
    } finally {
      dispatch(materialSlice.actions.endMaterialAction(null));
    }
  };
};

export const createMaterialThunk = (material: MaterialForm) => {
  return async (dispatch: Dispatch) => {
    const createMaterialsRequest = async (
      material: MaterialForm
    ): Promise<Material> => {
      const createMaterialResponse = await axios.post(
        `${baseUrl}/api/materials`,
        JSON.stringify(material),
        {
          headers: { "Content-Type": "application/json" },
        }
      );

      return createMaterialResponse.data;
    };

    dispatch(materialSlice.actions.startMaterialAction(null));
    try {
      const created = await createMaterialsRequest(material);
      dispatch(materialSlice.actions.upsertMaterialSuccess(created));
    } catch (error) {
      const err = error as AxiosError;
      dispatch(
        materialSlice.actions.upsertMaterialFail(
          err.response!.data.errors.message
        )
      );
    } finally {
      dispatch(materialSlice.actions.endMaterialAction(null));
    }
  };
};

export const updateMaterialThunk = (material: MaterialForm) => {
  return async (dispatch: Dispatch) => {
    const updateMaterialsRequest = async (
      material: MaterialForm
    ): Promise<Material> => {
      const updateMaterialResponse = await axios.put(
        `${baseUrl}/api/materials`,
        JSON.stringify(material),
        {
          headers: { "Content-Type": "application/json" },
        }
      );

      return updateMaterialResponse.data;
    };

    dispatch(materialSlice.actions.startMaterialAction(null));
    try {
      const updated = await updateMaterialsRequest(material);
      dispatch(materialSlice.actions.upsertMaterialSuccess(updated));
    } catch (error) {
      const err = error as AxiosError;
      dispatch(
        materialSlice.actions.upsertMaterialFail(
          err.response!.data.errors.message
        )
      );
    } finally {
      dispatch(materialSlice.actions.endMaterialAction(null));
    }
  };
};

export const removeMaterialThunk = (materialId: number) => {
  return async (dispatch: Dispatch) => {
    const removeMaterialsRequest = async (
      materialId: number
    ): Promise<void> => {
      await axios.delete(
        `${baseUrl}/api/materials/${materialId}`
      );
    };

    dispatch(materialSlice.actions.startMaterialAction(null));
    try {
      await removeMaterialsRequest(materialId);
      dispatch(materialSlice.actions.removeMaterialSuccess(materialId));
    } catch (error) {
      const err = error as AxiosError;
      dispatch(
        materialSlice.actions.removeMaterialFail(
          err.response!.data.errors.message
        )
      );
    } finally {
      dispatch(materialSlice.actions.endMaterialAction(null));
    }
  };
};

export const materialActions = materialSlice.actions;

export const materialReducers = materialSlice.reducer;
