import { createSlice, Dispatch, PayloadAction } from "@reduxjs/toolkit";
import axios, { AxiosError } from "axios";
import { GetProductsQueryResponse, Product, ProductListItem, ProductSummarize } from "../../models/product";

interface ProductState {
    products: ProductSummarize[];
    total: number;
    product: Product | null;
    loading: boolean;
    scrollY?: number;
    errorMessage: string;
    successMessage: string;
}

const initialState: ProductState = {
    products: [],
    total: 0,
    product: null,
    loading: true,
    scrollY: undefined,
    errorMessage: "",
    successMessage: "",
};

const productSlice = createSlice({
    name: "product",
    initialState: initialState,
    reducers: {
        startProductAction(state: ProductState, _) {
            state.loading = true;
            state.errorMessage = "";
            state.successMessage = "";
        },
        endProductAction(state: ProductState, _) {
            state.loading = false;
        },
        getProductsSuccess(
            state: ProductState,
            {payload} : PayloadAction<GetProductsQueryResponse>
        ) {
            const {products, total} = payload;
            for (const product of products) {
                if (!state.products.some(p => p.id === product.id)) {
                    state.products.push(product);
                }
            }
            state.total = total;
            state.errorMessage= "";
            state.successMessage = "";
        },
        getProductsFail(state: ProductState, action: PayloadAction<string>) {
            state.errorMessage = action.payload;
            state.successMessage = "";
        },
        getProductSuccess(state: ProductState, action: PayloadAction<Product>) {
          state.product = action.payload;
          state.errorMessage = "";
          state.successMessage = "";
        },
        getProductFail(state: ProductState, action: PayloadAction<string>) {
            state.errorMessage = action.payload;
            state.successMessage = "";
        },
        upsertProductSuccess(
          state: ProductState,
          action: PayloadAction<Product>
        ) {
          const product = action.payload;
          const found = state.products.find((x) => x.id === product.id);
          if (found) {
            found.price = product.price;
            state.successMessage = "Material updated successfully";
          } else {
            state.products.unshift({
              id: product.id,
              name: product.name,
              price: product.price,
            });
            state.successMessage = "Material created successfully";
          }
          state.errorMessage = "";
        },
        upsertMaterialFail(state: ProductState, action: PayloadAction<string>) {
          state.errorMessage = action.payload;
        },
        removeProductSuccess(state: ProductState, action: PayloadAction<number>) {
          const productId = action.payload;
          state.products = state.products.filter((x) => x.id !== productId);
          state.successMessage = "";
          state.errorMessage = "";
        },
        removeProductFail(state: ProductState, action: PayloadAction<string>) {
          state.errorMessage = action.payload;
          state.successMessage = "";
        },
        saveScrollYPosition(state: ProductState, action: PayloadAction<number>) {
          state.scrollY = action.payload;
        }
    }
});

const baseUrl = "http://localhost:5002";

export const getProductsThunk = (skip: number = 0, take: number = 20) => {
    return async (dispatch: Dispatch) => {
        const getProductsRequest = async (): Promise<GetProductsQueryResponse> => {
            const registerResponse = await axios.get(`${baseUrl}/api/products?skip=${skip}&take=${take}`);
            return registerResponse.data;
        };

        dispatch(productSlice.actions.startProductAction(null));
        try {
        const response = await getProductsRequest();
        dispatch(productSlice.actions.getProductsSuccess(response));
        } catch (error) {
        console.log(error)
        const err = error as AxiosError;
        dispatch(
            productSlice.actions.getProductsFail(
            err.response!.data.errors.message
            )
        );
        } finally {
        dispatch(productSlice.actions.endProductAction(null));
        }
    };
};

export const getProductByIdThunk = (productId: number) => {
    return async (dispatch: Dispatch) => {
        const getProductRequest = async (productId: number): Promise<Product> => {
            const registerResponse = await axios.get(`${baseUrl}/api/products/${productId}`);
            return registerResponse.data;
        };

        dispatch(productSlice.actions.startProductAction(null));
        try {
        const product = await getProductRequest(productId);
        dispatch(productSlice.actions.getProductSuccess(product));
        } catch (error) {
        const err = error as AxiosError;
        dispatch(
            productSlice.actions.getProductFail(
            err.response!.data.errors.message
            )
        );
        } finally {
        dispatch(productSlice.actions.endProductAction(null));
        }
    };
};

export const removeProductThunk = (productId: number) => {
    return async (dispatch: Dispatch) => {
        const removeProductRequest = async (materialId: number): Promise<void> => {
            await axios.delete(`${baseUrl}/api/products/${materialId}`);
        };

        dispatch(productSlice.actions.startProductAction(null));
        try {
        await removeProductRequest(productId);
        dispatch(productSlice.actions.removeProductSuccess(productId));
        } catch (error) {
        const err = error as AxiosError;
        dispatch(
            productSlice.actions.removeProductFail(
            err.response!.data.errors.message
            )
        );
        } finally {
        dispatch(productSlice.actions.endProductAction(null));
        }
    };
};

export const productActions = productSlice.actions;

export const productReducers = productSlice.reducer;