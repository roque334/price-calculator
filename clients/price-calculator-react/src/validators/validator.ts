export interface ValidationProps {
  required?: boolean | undefined;
  isEmail?: boolean | undefined;
  minLength?: number | undefined;
}

export const checkValidity = (value: any, rules: ValidationProps): boolean => {
  let result = true;
  if (rules) {
    if (rules.required !== undefined && rules.required) {
      result =
        (result && typeof value === "string" && value.trim() !== "") ||
        (typeof value === "object" && value.length > 0);
    }
    if (rules.minLength !== undefined && rules.minLength) {
      result = result && value.trim().length >= rules.minLength;
    }
    if (rules.isEmail !== undefined && rules.isEmail) {
      const pattern =
        /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
      result = result && pattern.test(value);
    }
  }
  return result;
};
