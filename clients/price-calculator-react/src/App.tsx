import React, { useEffect } from "react";
import { Route, Switch } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "./hooks";
import { getUserAction } from "./store/slices/auth-slice";
import NavBar from "./components/NavBar/NavBar";
import PrivateRoute from "./components/Routes/PrivateRoute/PrivateRoute";
import SignUp from "./components/SignUp/SignUp";
import SignIn from "./components/SignIn/SignIn";
import Materials from "./components/Materials/Materials";
import Products from "./components/Products/Products";
import ProductForm from "./components/Products/ProductForm/ProductForm";
import Spinner from "./components/UI/Spinner/Spinner";
import MaterialForm from "./components/Materials/MaterialForm/MaterialForm";
import 'bootstrap/dist/js/bootstrap.bundle';
import "bootstrap/dist/css/bootstrap.min.css";

const App: React.FC = () => {
  const dispatch = useAppDispatch();
  const authLoading = useAppSelector((state) => state.auth.loading);

  useEffect(() => {
      dispatch(getUserAction());
  }, [dispatch]);

  return (
    <div className="container">
      <NavBar />
      {authLoading ? (
        <Spinner />
      ) : (
        <Switch>
          <Route exact path="/register" component={SignUp} />
          <Route exact path="/login" component={SignIn} />
          <PrivateRoute exact path="/" component={Materials} />
          <PrivateRoute exact path="/material" component={Materials} />
          <PrivateRoute exact path="/materials/:id?" component={MaterialForm} />
          <PrivateRoute exact path="/product" component={Products} />
          <PrivateRoute exact path="/products/:id?" component={ProductForm} />
        </Switch>
      )}
    </div>
  );
};

export default App;
