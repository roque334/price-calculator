import { ListItem } from "./list";

export interface MeasurementUnit {
  id: number;
  name: string;
}

export interface Material {
  id: number;
  name: string;
  measurementUnit: MeasurementUnit;
  measurementValue: number;
  price: number;
  userName?: string;
  version?: number;
}

export interface MaterialForm {
  name: string;
  measurementUnit: string;
  measurementValue: number;
  price: number;
}

export interface MaterialSummarize {
  id: number;
  name: string;
  price: number;
}

export interface MaterialListItem extends ListItem {
  id: number;
  name: string;
  description: string;
  price: number;
}

export interface GetMaterialsQueryResponse {
  materials: MaterialSummarize[];
  total: number;
}
