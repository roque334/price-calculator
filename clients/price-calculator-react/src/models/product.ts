import { ListItem } from "./list";

export interface ProductMaterialLink {
    productId: number;
    materialId: number;
    quantity: number;
};

export interface Product {
    id: number;
    name: string;
    laborValue: number;
    productMaterialLinks: ProductMaterialLink[];
    price: number;
    userName?: string;
    version?: number;
};

export interface ProductMaterialForm {
    id: number;
    quantity: number;
};

export interface ProductForm {
    name: string;
    laborValue: number;
};

export interface ProductSummarize {
    id: number;
    name: string;
    price: number;
}
  
export interface ProductListItem extends ListItem {
    id: number;
    name: string;
    description: string;
    price: number;
}

export interface GetProductsQueryResponse {
  products: ProductSummarize[];
  total: number;
};
