import { MemoryRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import store from "../../store/store";
import SignUp from "./SignUp";
import Materials from "../Materials/Materials";
import PrivateRoute from "../Routes/PrivateRoute/PrivateRoute";

const server = setupServer(
  rest.post("http://localhost:5000/api/account/register", (req, res, ctx) => {
    return res(
      ctx.json({
        firstName: "test3",
        lastName: "test3",
        token:
          "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJ0ZXN0MyIsIm5iZiI6MTYyODUzMTUzOCwiZXhwIjoxNjI5MTM2MzM4LCJpYXQiOjE2Mjg1MzE1Mzh9.CSyM4r3dN3gv2dShQvwvrALNKznxesXp-FlGQGzltN-9FOWABAO34pOSGz8xq4BPZjdk8OzuCBM_zX7quOsmVg",
      })
    );
  }),
  rest.get("http://localhost:5001/api/materials", (req, res, ctx) => {
    return res(
      ctx.json({
        $id: "1",
        materials: [
          { $id: "2", id: 2, name: "Material A", price: 120.0 },
          { $id: "3", id: 4, name: "Material B", price: 100.0 },
          { $id: "4", id: 5, name: "Material C", price: 200.0 },
          { $id: "5", id: 6, name: "Material D", price: 300.0 },
          { $id: "6", id: 7, name: "Material E", price: 400.0 },
          { $id: "7", id: 8, name: "Material F", price: 500.0 },
          { $id: "8", id: 9, name: "Material G", price: 600.0 },
          { $id: "9", id: 10, name: "Material H", price: 700.0 },
          { $id: "10", id: 11, name: "Material I", price: 800.0 },
          { $id: "11", id: 12, name: "Material J", price: 900.0 },
        ],
        total: 26,
      })
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("form validations", () => {
  afterEach(cleanup);
  global.alert = jest.fn();

  it("disables the 'Register' button when form is incomplete", () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/register"]}>
          <SignUp />
        </MemoryRouter>
      </Provider>
    );
    expect(screen.getByText(/Register/i)).toBeDisabled();

    fireEvent.change(screen.getByLabelText("Firstname"), {
      target: { value: "John" },
    });
    fireEvent.change(screen.getByLabelText("Lastname"), {
      target: { value: "Doe" },
    });
    fireEvent.change(screen.getByLabelText("Username"), {
      target: { value: "john.doe" },
    });
    fireEvent.change(screen.getByLabelText("Password"), {
      target: { value: "Passw0rd!" },
    });
    fireEvent.change(screen.getByLabelText("Confirm Password"), {
      target: { value: "Passw0rd!" },
    });

    expect(screen.getByDisplayValue("John")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Doe")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe")).toBeInTheDocument();

    expect(screen.getByText(/Register/i)).toBeDisabled();
  });

  it("disables the 'Register' button when an inputs is invalid", () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/register"]}>
          <SignUp />
        </MemoryRouter>
      </Provider>
    );
    expect(screen.getByText(/Register/i)).toBeDisabled();

    fireEvent.change(screen.getByLabelText("Firstname"), {
      target: { value: "John" },
    });
    fireEvent.change(screen.getByLabelText("Lastname"), {
      target: { value: "Doe" },
    });
    fireEvent.change(screen.getByLabelText("Email Address"), {
      target: { value: "john.doe.bad.email" },
    });
    fireEvent.change(screen.getByLabelText("Username"), {
      target: { value: "john.doe" },
    });
    fireEvent.change(screen.getByLabelText("Password"), {
      target: { value: "Passw0rd!" },
    });
    fireEvent.change(screen.getByLabelText("Confirm Password"), {
      target: { value: "Passw0rd!" },
    });

    expect(screen.getByDisplayValue("John")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Doe")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe.bad.email")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe")).toBeInTheDocument();

    expect(screen.getByText(/Register/i)).toBeDisabled();
  });

  it("pops up an aler message when passwords do not match", async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/register"]}>
          <SignUp />
        </MemoryRouter>
      </Provider>
    );
    expect(screen.getByText(/Register/i)).toBeDisabled();

    fireEvent.change(screen.getByLabelText("Firstname"), {
      target: { value: "John" },
    });
    fireEvent.change(screen.getByLabelText("Lastname"), {
      target: { value: "Doe" },
    });
    fireEvent.change(screen.getByLabelText("Email Address"), {
      target: { value: "john.doe@abc.com" },
    });
    fireEvent.change(screen.getByLabelText("Username"), {
      target: { value: "john.doe" },
    });
    fireEvent.change(screen.getByLabelText("Password"), {
      target: { value: "password" },
    });
    fireEvent.change(screen.getByLabelText("Confirm Password"), {
      target: { value: "Passw0rd!" },
    });

    fireEvent.submit(screen.getByTestId("form"), {
      target: {
        firstname: { value: "John" },
        lastname: { value: "Doe" },
        "email-address": { value: "john.doe@abc.com" },
        username: { value: "john.doe" },
        password: { value: "password" },
        "confirm-password": { value: "Passw0rd!" },
      },
    });
    
    await waitFor(() => {
      expect(global.alert).toHaveBeenCalledTimes(1);
    });

    expect(screen.getByDisplayValue("John")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Doe")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe@abc.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe")).toBeInTheDocument();
    expect(screen.getByText(/Register/i)).toBeEnabled();
  });

  it("enables the 'Register' button when all inputs are valid", async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/register"]}>
          <SignUp />
          <Switch>
            <PrivateRoute exact path="/" component={Materials} />
          </Switch>
        </MemoryRouter>
      </Provider>
    );
    expect(screen.getByText(/Register/i)).toBeDisabled();

    fireEvent.change(screen.getByLabelText("Firstname"), {
      target: { value: "John" },
    });
    fireEvent.change(screen.getByLabelText("Lastname"), {
      target: { value: "Doe" },
    });
    fireEvent.change(screen.getByLabelText("Email Address"), {
      target: { value: "john.doe@abc.com" },
    });
    fireEvent.change(screen.getByLabelText("Username"), {
      target: { value: "john.doe" },
    });
    fireEvent.change(screen.getByLabelText("Password"), {
      target: { value: "Passw0rd!" },
    });
    fireEvent.change(screen.getByLabelText("Confirm Password"), {
      target: { value: "Passw0rd!" },
    });

    fireEvent.submit(screen.getByTestId("form"), {
      target: {
        firstname: { value: "John" },
        lastname: { value: "Doe" },
        "email-address": { value: "john.doe@abc.com" },
        username: { value: "john.doe" },
        password: { value: "Passw0rd!" },
        "confirm-password": { value: "Passw0rd!" },
      },
    });

    expect(screen.getByDisplayValue("John")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Doe")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe@abc.com")).toBeInTheDocument();
    expect(screen.getByDisplayValue("john.doe")).toBeInTheDocument();
    expect(screen.getByText(/Register/i)).toBeEnabled();

    await waitFor(() => {
      expect(screen.getByText(/Materials/i)).toBeInTheDocument();
    });
  });
});
