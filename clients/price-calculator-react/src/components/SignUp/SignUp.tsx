import React, { Fragment, useEffect, useState } from "react";
import { Link, Redirect } from "react-router-dom";
import { useAppDispatch, useAppSelector } from "../../hooks";
import { authActions, registerAction, registerActionParam } from "../../store/slices/auth-slice";
import { checkValidity, ValidationProps } from "../../validators/validator";
import Input, {
  CheckBoxProps,
  FormInputProps,
  InputProps,
  InputType,
  SelectProps,
  TextAreaProps,
} from "../UI/Input/Input";

const SignUp: React.FC = () => {
  const [signUpForm, setSignUpForm] = useState({
    firstname: {
      configuration: {
        elementType: InputType.Input,
        name: "firstname",
        label: "Firstname",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    lastname: {
      configuration: {
        elementType: InputType.Input,
        name: "lastname",
        label: "Lastname",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    email: {
      configuration: {
        elementType: InputType.Input,
        name: "email-address",
        label: "Email Address",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "email" },
      } as InputProps,
      validation: {
        required: true,
        isEmail: true,
      } as ValidationProps,
    } as FormInputProps,
    username: {
      configuration: {
        elementType: InputType.Input,
        name: "username",
        label: "Username",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    password: {
      configuration: {
        elementType: InputType.Input,
        name: "password",
        label: "Password",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "password" },
      } as InputProps,
      validation: {
        required: true,
        minLength: 6,
      } as ValidationProps,
    } as FormInputProps,
    confirmPassword: {
      configuration: {
        elementType: InputType.Input,
        name: "confirm-password",
        label: "Confirm Password",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "password" },
      } as InputProps,
      validation: {
        required: true,
        minLength: 6,
      } as ValidationProps,
    } as FormInputProps,
  });
  const [isFormValid, setIsFormValid] = useState(false);
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  const authError = useAppSelector((state) => state.auth.registerError);

  useEffect(() => {
    if (authError && authError.length > 0) {
      alert(authError);
      dispatch(authActions.cleanErrorsAction(null));
    }
  }, [authError]);

  if (user) {
    return <Redirect to="/" />;
  }

  const inputChangeHandler = (
    event:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLSelectElement>
      | React.ChangeEvent<HTMLTextAreaElement>,
    id: keyof typeof signUpForm
  ) => {
    const updatedSignUpForm = { ...signUpForm };
    const updatedElementSignUpForm = { ...signUpForm[id] };
    updatedElementSignUpForm.configuration.value = event.target.value;
    updatedElementSignUpForm.configuration.touched = true;
    updatedElementSignUpForm.configuration.valid =
      updatedElementSignUpForm.validation === undefined
        ? true
        : checkValidity(
            updatedElementSignUpForm.configuration.value,
            updatedElementSignUpForm.validation
          );
    updatedSignUpForm[id] = updatedElementSignUpForm;

    let formValid = true;
    let key: keyof typeof updatedSignUpForm;
    for (key in updatedSignUpForm) {
      if (!updatedSignUpForm[key].configuration.valid) {
        formValid = false;
        break;
      }
    }

    setSignUpForm(updatedSignUpForm);
    setIsFormValid(formValid);
  };

  const onSignUpHandler: React.FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();
    if (
      signUpForm.password.configuration.value !==
      signUpForm.confirmPassword.configuration.value
    ) {
      alert("Password do not match");
    } else {
      const userToRegister: registerActionParam = {
        firstname: (signUpForm.firstname.configuration as InputProps)
          .value as string,
        lastname: (signUpForm.lastname.configuration as InputProps)
          .value as string,
        email: (signUpForm.email.configuration as InputProps)
          .value as string,
        username: (signUpForm.username.configuration as InputProps)
          .value as string,
        password: (signUpForm.password.configuration as InputProps)
          .value as string,
      };
      
      dispatch(
        registerAction(userToRegister)
      );
    }
  };

  const formElementArray: {
    id: string;
    config: InputProps | CheckBoxProps | SelectProps | TextAreaProps;
  }[] = [];
  let key: keyof typeof signUpForm;
  for (key in signUpForm) {
    formElementArray.push({
      id: key,
      config: signUpForm[key].configuration,
    });
  }

  return (
    <Fragment>
      <h1>Sign Up</h1>
      <h2>Create an Account</h2>
      <form onSubmit={onSignUpHandler} data-testid="form">
        {formElementArray.map((element) => {
          return (
            <Input
              key={element.id}
              {...element.config}
              onChange={(
                event:
                  | React.ChangeEvent<HTMLInputElement>
                  | React.ChangeEvent<HTMLTextAreaElement>
                  | React.ChangeEvent<HTMLSelectElement>
              ) =>
                inputChangeHandler(event, element.id as keyof typeof signUpForm)
              }
            />
          );
        })}
        <button
          type="submit"
          className="btn btn-primary"
          disabled={!isFormValid}
        >
          Register
        </button>
      </form>
      <span>
        Already have an account? <Link to="/login">Sign In</Link>
      </span>
    </Fragment>
  );
};

export default SignUp;
