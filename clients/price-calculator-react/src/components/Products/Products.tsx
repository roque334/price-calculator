import React, { Fragment, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import styles from "./Products.module.css";
import { useAppDispatch, useAppSelector } from "../../hooks";
import {
  getProductsThunk,
  removeProductThunk,
} from "../../store/slices/product-slice";
import ConfirmationModal from "../UI/Modal/ConfirmationModal/ConfirmationModal";
import ListView from "../UI/ListView/ListView";
import { ListItem } from "../../models/list";
import { ProductSummarize } from "../../models/product";

interface ProductsProps extends RouteComponentProps {}

const Products: React.FC<ProductsProps> = ({ history }) => {
  const [showModal, setShowModal] = useState(false);
  const [elementOnFocus, setElementOnFocus] = useState<ListItem | null>(
    null
  );
  const dispatch = useAppDispatch();
  const loading = useAppSelector((state) => state.product.loading);
  const products = useAppSelector((state) =>
    state.product.products.map((product: ProductSummarize): ListItem => {
      return {
        id: product.id,
        name: product.name,
        description: `Price: $${product.price}`
      }
  }));
  const total = useAppSelector((state) => state.product.total);

  const onEditHandler = (product: ListItem) => {
    setElementOnFocus(product);
    history.push(`/product/${product.id}`);
  };

  const onRemoveHandler = (product: ListItem) => {
    setElementOnFocus(product);
    setShowModal(true);
  };

  const onLoadMoreDataHandler = (skip: number, take: number) => {
    dispatch(getProductsThunk(skip, take))
  }

  const confimationOkHandler = () => {
    dispatch(removeProductThunk(elementOnFocus!.id));
    setShowModal(false);
  };

  const confimationCancelHandler = () => {
    setShowModal(false);
  };

  const modal = (
    <ConfirmationModal
      show={showModal}
      onClose={() => setShowModal(false)}
      message={`Are you sure you want to remove the item: ${elementOnFocus?.name}`}
      okButtonText="Yes"
      okButtonClickHandler={confimationOkHandler}
      cancelButtonText="No"
      cancelButtonClickHandler={confimationCancelHandler}
    />
  );

  return (
    <Fragment>
      <h1>Products</h1>
      {showModal && modal}
      <Link
        to="/products"
        className={`btn btn-primary ${styles["btn-create"]}`}
      >
        Create
      </Link>
      <ListView
        elements={products}
        total={total}
        loading={loading}
        onEdit={onEditHandler}
        onRemove={onRemoveHandler}
        onLoadMoreData={onLoadMoreDataHandler}
      />
    </Fragment>
  );
};

export default Products;
