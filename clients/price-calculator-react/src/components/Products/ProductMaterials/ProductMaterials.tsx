import { Autocomplete, AutocompleteValue, TextField } from '@mui/material'
import React, { useState } from 'react'
import { Material } from '../../../models/material';

interface AutocompleteOption {
  id: number;
  label: string;
  price: number;
}

interface ProductMaterialsProps {
  materials: Material[];
}

const ProductMaterials: React.FC<ProductMaterialsProps> = ({ materials }) => {
  const [selected, setSelected] = useState<AutocompleteOption | null>(null);

  const materialOptions = materials.map((material: Material): AutocompleteOption => {
    return { id: material.id, label: material.name, price: material.price };
  });

  const onAutocompleteChange = (event: React.SyntheticEvent, value: AutocompleteOption | null) => {
    setSelected(value);
  };

  return (
    <Autocomplete
      disablePortal
      id="combo-box-demo"
      onChange={onAutocompleteChange}
      options={materialOptions}
      renderInput={(params) => <TextField {...params} label="Materials" />}
      sx={{ width: 300 }}
    />
  )
}

export default ProductMaterials