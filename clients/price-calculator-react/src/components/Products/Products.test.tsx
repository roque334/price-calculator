import { MemoryRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import store from "../../store/store";
import Products from "./Products";

const server = setupServer(
  rest.get("http://localhost:5002/api/products", (req, res, ctx) => {
    const query = req.url.searchParams;
    const skip = query.get("skip");
    const take = query.get("take");
    return res(
      ctx.json({
        $id: "1",
        products: [
          { $id: "2", id: 2, name: "Product A", price: 120.0 },
          { $id: "3", id: 4, name: "Product B", price: 100.0 },
          { $id: "4", id: 5, name: "Product C", price: 200.0 },
          { $id: "5", id: 6, name: "Product D", price: 300.0 },
          { $id: "6", id: 7, name: "Product E", price: 400.0 },
          { $id: "7", id: 8, name: "Product F", price: 500.0 },
          { $id: "8", id: 9, name: "Product G", price: 600.0 },
          { $id: "9", id: 10, name: "Product H", price: 700.0 },
          { $id: "10", id: 11, name: "Product I", price: 800.0 },
          { $id: "11", id: 12, name: "Product J", price: 900.0 },
        ],
        total: 10,
      })
    );
  }),
  rest.delete("http://localhost:5002/api/products/12", (req, res, ctx) => {
    return res(ctx.status(200));
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe("Products tests", () => {
  beforeEach(() => {
    const mockIntersectionObserver = jest.fn();
    mockIntersectionObserver.mockReturnValue({
      observe: () => null,
      unobserve: () => null,
      disconnect: () => null,
    });
    window.IntersectionObserver = mockIntersectionObserver;
  });
  afterEach(cleanup);
  global.alert = jest.fn();

  it("returns 10 products", async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/"]}>
          <Route path="/" component={Products} />
        </MemoryRouter>
      </Provider>
    );

    expect(await screen.findByText(/^Create$/)).toBeEnabled();
    expect(await screen.findByText(/Product A/i)).toBeEnabled();
    expect(await screen.findByText(/Product B/i)).toBeEnabled();
    expect(await screen.findByText(/Product C/i)).toBeEnabled();
    expect(await screen.findByText(/Product D/i)).toBeEnabled();
    expect(await screen.findByText(/Product E/i)).toBeEnabled();
    expect(await screen.findByText(/Product F/i)).toBeEnabled();
    expect(await screen.findByText(/Product G/i)).toBeEnabled();
    expect(await screen.findByText(/Product H/i)).toBeEnabled();
    expect(await screen.findByText(/Product I/i)).toBeEnabled();
    expect(await screen.findByText(/Product J/i)).toBeEnabled();
  });

  it("delete last element", async () => {
    render(
      <Provider store={store}>
        <MemoryRouter initialEntries={["/"]}>
          <Route path="/" component={Products} />
        </MemoryRouter>
      </Provider>
    );

    expect(await screen.findByText(/^Create$/)).toBeEnabled();
    expect(await screen.findByText(/Product A/i)).toBeEnabled();
    expect(await screen.findByText(/Product B/i)).toBeEnabled();
    expect(await screen.findByText(/Product C/i)).toBeEnabled();
    expect(await screen.findByText(/Product D/i)).toBeEnabled();
    expect(await screen.findByText(/Product E/i)).toBeEnabled();
    expect(await screen.findByText(/Product F/i)).toBeEnabled();
    expect(await screen.findByText(/Product G/i)).toBeEnabled();
    expect(await screen.findByText(/Product H/i)).toBeEnabled();
    expect(await screen.findByText(/Product I/i)).toBeEnabled();
    expect(await screen.findByText(/Product J/i)).toBeEnabled();

    const removeButton = await screen.findByTestId(/^Remove-12$/);
    expect(removeButton).toBeEnabled();
    fireEvent.click(removeButton);

    const okButton = await screen.findByTestId(/^ok_button$/);
    await expect(okButton).toBeEnabled();
    fireEvent.click(okButton);

    await waitFor(() => {
      expect(screen.queryByText(/Product J/i)).not.toBeInTheDocument();
    });
  });
});
