import React from 'react'
import { RouteComponentProps } from "react-router";
import ProductMaterials from '../ProductMaterials/ProductMaterials';

interface MatchParams {
    id?: string | undefined;
  }

interface ProductFormProps extends RouteComponentProps<MatchParams> {}


const ProductForm: React.FC<ProductFormProps> = ({ match }) => {
  return (
    <ProductMaterials />
  )
}

export default ProductForm