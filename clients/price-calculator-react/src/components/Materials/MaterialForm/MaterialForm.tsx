import React, { Fragment, useEffect, useState } from "react";
import { RouteComponentProps } from "react-router";
import { useAppDispatch, useAppSelector } from "../../../hooks";
import { MaterialForm as MaterialFormModel} from "../../../models/material";
import {
  createMaterialThunk,
  getMaterialByIdThunk,
  updateMaterialThunk,
} from "../../../store/slices/material-slice";
import { checkValidity, ValidationProps } from "../../../validators/validator";
import Input, {
  CheckBoxProps,
  FormInputProps,
  InputProps,
  InputType,
  SelectProps,
  TextAreaProps,
} from "../../UI/Input/Input";
import Spinner from "../../UI/Spinner/Spinner";
import {materialActions} from "../../../store/slices/material-slice";
import { useHistory } from 'react-router-dom';

interface MatchParams {
  id?: string | undefined;
}

interface MaterialFormProps extends RouteComponentProps<MatchParams> {}

const MaterialForm: React.FC<MaterialFormProps> = ({ match }) => {
  const [materialForm, setMaterialForm] = useState({
    name: {
      configuration: {
        elementType: InputType.Input,
        name: "name",
        label: "Name",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    measurementValue: {
      configuration: {
        elementType: InputType.Input,
        name: "measurementValue",
        label: "Quantity",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    measurementUnit: {
      configuration: {
        elementType: InputType.Select,
        name: "measurementUnit",
        label: "Measurement",
        touched: false,
        valid: true,
        autoComplete: true,
        options: [
          {
            displayValue: "Units",
            value: "Units",
          },
          {
            displayValue: "Grams",
            value: "Grams",
          },
          {
            displayValue: "Milliliters",
            value: "Milliliters",
          },
        ],
        value: "Units",
        htmlAttributes: {},
      } as SelectProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
    price: {
      configuration: {
        elementType: InputType.Input,
        name: "price",
        label: "Price",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "text" },
      } as InputProps,
      validation: {
        required: true,
      } as ValidationProps,
    } as FormInputProps,
  });
  const [isFormValid, setIsFormValid] = useState(false);
  const dispatch = useAppDispatch();
  const loading = useAppSelector((state) => state.material.loading);
  const initialMaterial = useAppSelector((state) => state.material.material);
  const errorMessage = useAppSelector((state) => state.material.errorMessage);
  const successMessage = useAppSelector((state) => state.material.successMessage);
  const materialId = match.params.id;
  const history = useHistory()

  useEffect(() => {
    if (materialId) {
      dispatch(getMaterialByIdThunk(parseInt(materialId)));
    }
    else {
      dispatch(materialActions.endMaterialAction(null));
    }
  }, []);

  useEffect(() => {
    if (materialId && !loading && errorMessage) {
      history.push("/material/");
    }
  }, [materialId, loading, errorMessage]);

  useEffect(() => {
    if (materialId && initialMaterial) {
      const updatedMaterialForm = { ...materialForm };

      let key: keyof typeof materialForm;
      for (key in materialForm) {
        if (key === "measurementUnit") {
          updatedMaterialForm[key].configuration.value =
            initialMaterial[key].name;
        } else {
          updatedMaterialForm[key].configuration.value = initialMaterial[key];
        }
        updatedMaterialForm[key].configuration.valid = true;
      }
      updatedMaterialForm.name.configuration.htmlAttributes.disabled = true;

      let formValid = true;
      for (key in updatedMaterialForm) {
        if (!updatedMaterialForm[key].configuration.valid) {
          formValid = false;
          break;
        }
      }
      
      setMaterialForm(updatedMaterialForm);
      setIsFormValid(formValid);
    }
  }, [materialId, initialMaterial, setMaterialForm]);

  useEffect(() => {
    if (errorMessage && errorMessage.length > 0) {
      alert(errorMessage);
    }
  }, [errorMessage]);

  useEffect(() => {
    if (successMessage && successMessage.length > 0) {
      alert(successMessage);
    }
  }, [successMessage]);

  const onSubmitHandler: React.FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();
    const material: MaterialFormModel = {
      name: (materialForm.name.configuration as InputProps).value as string,
      measurementValue: (
        materialForm.measurementValue.configuration as InputProps
      ).value as number,
      measurementUnit: (
        materialForm.measurementUnit.configuration as SelectProps
      ).value as string,
      price: (materialForm.price.configuration as InputProps).value as number,
    };

    if (initialMaterial) {
      dispatch(updateMaterialThunk(material));
      return;
    }

    dispatch(createMaterialThunk(material));
  };

  const inputChangeHandler = (
    event:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLSelectElement>
      | React.ChangeEvent<HTMLTextAreaElement>,
    id: keyof typeof materialForm
  ) => {
    const updatedMaterialForm = { ...materialForm };
    const updatedElementMaterialForm = { ...materialForm[id] };
    updatedElementMaterialForm.configuration.value = event.target.value;
    updatedElementMaterialForm.configuration.touched = true;
    updatedElementMaterialForm.configuration.valid =
      updatedElementMaterialForm.validation === undefined
        ? true
        : checkValidity(
            updatedElementMaterialForm.configuration.value,
            updatedElementMaterialForm.validation
          );
    updatedMaterialForm[id] = updatedElementMaterialForm;

    let formValid = true;
    let key: keyof typeof updatedMaterialForm;
    for (key in updatedMaterialForm) {
      if (!updatedMaterialForm[key].configuration.valid) {
        formValid = false;
        break;
      }
    }

    setMaterialForm(updatedMaterialForm);
    setIsFormValid(formValid);
  };

  const formElementArray: {
    id: string;
    config: InputProps | CheckBoxProps | SelectProps | TextAreaProps;
  }[] = [];
  let key: keyof typeof materialForm;
  for (key in materialForm) {
    formElementArray.push({
      id: key,
      config: materialForm[key].configuration,
    });
  }

  if (loading) return <Spinner />;

  return (
    <Fragment>
      <h1>Material</h1>
      <h2>{materialId ? "Update" : "Create"} a Material</h2>
      <form onSubmit={onSubmitHandler} data-testid="form">
        {formElementArray.map((element) => {
          return (
            <Input
              key={element.id}
              {...element.config}
              onChange={(
                event:
                  | React.ChangeEvent<HTMLInputElement>
                  | React.ChangeEvent<HTMLTextAreaElement>
                  | React.ChangeEvent<HTMLSelectElement>
              ) =>
                inputChangeHandler(
                  event,
                  element.id as keyof typeof materialForm
                )
              }
            />
          );
        })}
        <button
          type="submit"
          className="btn btn-primary"
          disabled={!isFormValid}
        >
          {materialId ? "Update" : "Create"}
        </button>
      </form>
    </Fragment>
  );
};

export default MaterialForm;
