import { MemoryRouter, Route } from "react-router-dom";
import { Provider } from "react-redux";
import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import store from "../../../store/store";
import MaterialForm from "./MaterialForm";
import { getMaterialsThunk } from "../../../store/slices/material-slice";

describe("MaterialForm", () => {
  describe("create material", () => {
    const server = setupServer(
      rest.post("http://localhost:5001/api/materials", (req, res, ctx) => {
        return res(
          ctx.json({
            $id: "1",
            id: 29,
            name: "Material A",
            measurementUnit: {
              $id: "2",
              id: 1,
              name: "Units",
            },
            measurementValue: 10,
            price: 100,
            userName: "test3",
            version: 0,
          })
        );
      })
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    describe("form validations", () => {
      afterEach(cleanup);
      global.alert = jest.fn();

      it("disables the 'Create' button when form is incomplete", () => {
        render(
          <Provider store={store}>
            <MemoryRouter initialEntries={["/material/"]}>
              <Route path="/material/:id?" component={MaterialForm} />
            </MemoryRouter>
          </Provider>
        );
        expect(screen.getByText(/^Create$/)).toBeDisabled();

        fireEvent.change(screen.getByLabelText("Name"), {
          target: { value: "" },
        });
        fireEvent.change(screen.getByLabelText("Quantity"), {
          target: { value: "10" },
        });
        fireEvent.change(screen.getByLabelText("Measurement"), {
          target: { value: "Units" },
        });
        fireEvent.change(screen.getByLabelText("Price"), {
          target: { value: "100.0" },
        });

        expect(screen.getByDisplayValue("10")).toBeInTheDocument();
        expect(screen.getByDisplayValue("Units")).toBeInTheDocument();
        expect(screen.getByDisplayValue("100.0")).toBeInTheDocument();

        expect(screen.getByText(/^Create$/)).toBeDisabled();
      });

      it("pops up an aler message when error in backend", async () => {
        const message = "The material Material A already exists.";
        server.use(
          rest.post("http://localhost:5001/api/materials", (req, res, ctx) => {
            return res(
              ctx.status(400),
              ctx.json({
                $id: "1",
                errors: {
                  $id: "2",
                  message,
                },
              })
            );
          })
        );
        render(
          <Provider store={store}>
            <MemoryRouter initialEntries={["/material"]}>
              <Route path="/material/:id?" component={MaterialForm} />
            </MemoryRouter>
          </Provider>
        );
        expect(screen.getByText(/^Create$/)).toBeDisabled();

        fireEvent.change(screen.getByLabelText("Name"), {
          target: { value: "Material A" },
        });
        fireEvent.change(screen.getByLabelText("Quantity"), {
          target: { value: "10" },
        });
        fireEvent.change(screen.getByLabelText("Measurement"), {
          target: { value: "Units" },
        });
        fireEvent.change(screen.getByLabelText("Price"), {
          target: { value: "100.0" },
        });

        fireEvent.submit(screen.getByTestId("form"), {
          target: {
            name: { value: "Material A" },
            measurementValue: { value: "10" },
            measurementUnit: { value: "Units" },
            price: { value: "100.0" },
          },
        });

        await waitFor(() => {
          expect(global.alert).toBeCalledWith(message);
        });

        expect(screen.getByDisplayValue("Material A")).toBeInTheDocument();
        expect(screen.getByDisplayValue("10")).toBeInTheDocument();
        expect(screen.getByDisplayValue("Units")).toBeInTheDocument();
        expect(screen.getByDisplayValue("100.0")).toBeInTheDocument();
        expect(screen.getByText(/^Create$/)).toBeEnabled();
      });

      it("creates material", async () => {
        render(
          <Provider store={store}>
            <MemoryRouter initialEntries={["/material"]}>
              <Route path="/material/:id?" component={MaterialForm} />
            </MemoryRouter>
          </Provider>
        );
        expect(screen.getByText(/^Create$/)).toBeDisabled();

        fireEvent.change(screen.getByLabelText("Name"), {
          target: { value: "Material A" },
        });
        fireEvent.change(screen.getByLabelText("Quantity"), {
          target: { value: "10" },
        });
        fireEvent.change(screen.getByLabelText("Measurement"), {
          target: { value: "Units" },
        });
        fireEvent.change(screen.getByLabelText("Price"), {
          target: { value: "100.0" },
        });

        fireEvent.submit(screen.getByTestId("form"), {
          target: {
            name: { value: "Material A" },
            measurementValue: { value: "10" },
            measurementUnit: { value: "Units" },
            price: { value: "100.0" },
          },
        });

        await waitFor(() => {
          expect(global.alert).toBeCalledWith("Material created successfully");
        });

        expect(screen.getByDisplayValue("Material A")).toBeInTheDocument();
        expect(screen.getByDisplayValue("10")).toBeInTheDocument();
        expect(screen.getByDisplayValue("Units")).toBeInTheDocument();
        expect(screen.getByDisplayValue("100.0")).toBeInTheDocument();
        expect(screen.getByText(/^Create$/)).not.toBeDisabled();
      });
    });
  });

  describe("update material", () => {
    const server = setupServer(
      rest.get("http://localhost:5001/api/materials", (req, res, ctx) => {
        return res(
          ctx.json({
            $id: "1",
            materials: [{ $id: "1", id: 1, name: "Material A", price: 100.0 }],
            total: 1,
          })
        );
      }),
      rest.get("http://localhost:5001/api/materials/1", (req, res, ctx) => {
        return res(
          ctx.json({
            $id: "1",
            id: 1,
            name: "Material A",
            measurementUnit: {
              $id: "2",
              id: 1,
              name: "Units",
            },
            measurementValue: 100,
            price: 100.0,
            userName: "test3",
            version: 0,
          })
        );
      }),
      rest.put("http://localhost:5001/api/materials", (req, res, ctx) => {
        return res(
          ctx.json({
            $id: "1",
            id: 1,
            name: "Material A",
            measurementUnit: {
              $id: "2",
              id: 1,
              name: "Units",
            },
            measurementValue: 20,
            price: 200.0,
            userName: "test3",
            version: 1,
          })
        );
      })
    );

    beforeAll(() => server.listen());
    afterEach(() => server.resetHandlers());
    afterAll(() => server.close());

    describe("form validations", () => {
      afterEach(cleanup);
      global.alert = jest.fn();

      it("updates material", async () => {
        store.dispatch(getMaterialsThunk());
        render(
          <Provider store={store}>
            <MemoryRouter initialEntries={["/material/1"]}>
              <Route path="/material/:id?" component={MaterialForm} />
            </MemoryRouter>
          </Provider>
        );

        await waitFor(() => {
          expect(screen.getByText(/^Update$/)).toBeEnabled();
          expect(screen.getByLabelText("Name")).toBeDisabled();
        });

        fireEvent.change(screen.getByLabelText("Quantity"), {
          target: { value: "20" },
        });
        fireEvent.change(screen.getByLabelText("Measurement"), {
          target: { value: "Units" },
        });
        fireEvent.change(screen.getByLabelText("Price"), {
          target: { value: "200.0" },
        });

        fireEvent.submit(screen.getByTestId("form"), {
          target: {
            name: { value: "Material A" },
            measurementValue: { value: "20" },
            measurementUnit: { value: "Units" },
            price: { value: "200.0" },
          },
        });

        await waitFor(() => {
          expect(global.alert).toBeCalledWith("Material updated successfully");
        });

        expect(screen.getByDisplayValue("Material A")).toBeInTheDocument();
        expect(screen.getByDisplayValue("20")).toBeInTheDocument();
        expect(screen.getByDisplayValue("Units")).toBeInTheDocument();
        expect(screen.getByDisplayValue("200.0")).toBeInTheDocument();
      });

      it("load material fails", async () => {
        const message = "Material not found.";
        server.use(
          rest.get("http://localhost:5001/api/materials/1", (req, res, ctx) => {
            return res(
              ctx.status(400),
              ctx.json({
                $id: "1",
                errors: {
                  $id: "2",
                  message,
                },
              })
            );
          })
        );
        render(
          <Provider store={store}>
            <MemoryRouter initialEntries={["/material/1"]}>
              <Route path="/material/:id?" component={MaterialForm} />
            </MemoryRouter>
          </Provider>
        );

        await waitFor(() => {
          expect(global.alert).toBeCalledWith(message);
          expect(screen.getByText(/^Create$/)).toBeDefined();
        });
      });
    });
  });
});
