import React, { Fragment, useState } from "react";
import { Link, RouteComponentProps } from "react-router-dom";
import styles from "./Materials.module.css";
import { useAppDispatch, useAppSelector } from "../../hooks";
import {
  getMaterialsThunk,
  removeMaterialThunk,
} from "../../store/slices/material-slice";
import ConfirmationModal from "../UI/Modal/ConfirmationModal/ConfirmationModal";
import ListView from "../UI/ListView/ListView";
import { ListItem } from "../../models/list";
import { MaterialSummarize } from "../../models/material";

interface MaterialsProps extends RouteComponentProps {}

const Materials: React.FC<MaterialsProps> = ({ history }) => {
  const [showModal, setShowModal] = useState(false);
  const [elementOnFocus, setElementOnFocus] = useState<ListItem | null>(
    null
  );
  const dispatch = useAppDispatch();
  const loading = useAppSelector((state) => state.material.loading);
  const materials = useAppSelector((state) =>
    state.material.materials.map((material: MaterialSummarize): ListItem => {
      return {
        id: material.id,
        name: material.name,
        description: `Price: $${material.price}`
      }
    })
  );
  const total = useAppSelector((state) => state.material.total);

  const onEditHandler = (material: ListItem) => {
    setElementOnFocus(material);
    history.push(`/material/${material.id}`);
  };

  const onRemoveHandler = (material: ListItem) => {
    setElementOnFocus(material);
    setShowModal(true);
  };

  const onLoadMoreDataHandler = (skip: number, take: number) => {
    dispatch(getMaterialsThunk(skip, take))
  }

  const confimationOkHandler = () => {
    dispatch(removeMaterialThunk(elementOnFocus!.id));
    setShowModal(false);
  };

  const confimationCancelHandler = () => {
    setShowModal(false);
  };

  const modal = (
    <ConfirmationModal
      show={showModal}
      onClose={() => setShowModal(false)}
      message={`Are you sure you want to remove the item: ${elementOnFocus?.name}`}
      okButtonText="Yes"
      okButtonClickHandler={confimationOkHandler}
      cancelButtonText="No"
      cancelButtonClickHandler={confimationCancelHandler}
    />
  );

  return (
    <Fragment>
      <h1>Materials</h1>
      {showModal && modal}
      <Link
        to="/materials"
        className={`btn btn-primary ${styles["btn-create"]}`}
      >
        Create
      </Link>
      <ListView
        elements={materials}
        total={total}
        loading={loading}
        onEdit={onEditHandler}
        onRemove={onRemoveHandler}
        onLoadMoreData={onLoadMoreDataHandler}
      />
    </Fragment>
  );
};

export default Materials;
