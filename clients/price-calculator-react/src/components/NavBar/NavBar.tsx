import React from "react";

const NavBar: React.FC = () => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <div className="container-fluid">
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <a className="nav-link" href="/material">Materials</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/product">Products</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
