import React from "react";
import styles from "./ConfirmationModal.module.css";
import Modal, { ModalProps } from "../Modal";

interface ConfirmationModalProps extends ModalProps {
  message: string;
  okButtonText: string;
  okButtonClickHandler: () => void;
  cancelButtonText: string;
  cancelButtonClickHandler: () => void;
}

const ConfirmationModal: React.FC<ConfirmationModalProps> = ({
  message,
  okButtonText,
  cancelButtonText,
  show,
  okButtonClickHandler,
  cancelButtonClickHandler,
  onClose,
}) => {
  return (
    <Modal show={show} onClose={onClose}>
      <div className={[styles["modal-main-message"]].join(" ")}>
        <h5>{message}</h5>
      </div>
      <div className={[styles["modal-main-actions"]].join(" ")}>
        <button
          data-testid="ok_button"
          type="button"
          className="btn btn-primary"
          onClick={okButtonClickHandler}
        >
          {okButtonText}
        </button>
        <button
          data-testid="cancel_button"
          type="button"
          className="btn btn-danger"
          onClick={cancelButtonClickHandler}
        >
          {cancelButtonText}
        </button>
      </div>
    </Modal>
  );
};

export default ConfirmationModal;
