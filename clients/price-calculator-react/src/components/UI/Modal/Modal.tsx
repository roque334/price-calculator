import React from "react";
import styles from "./Modal.module.css";

export interface ModalProps {
  show: boolean;
  onClose: () => void;
}

const Modal: React.FC<ModalProps> = ({ show, onClose, children }) => {
  const modalStyles = [styles["modal"]];
  if (show) {
    modalStyles.push(styles["display-block"]);
  } else {
    modalStyles.push(styles["display-none"]);
  }

  return (
    <div className={modalStyles.join(" ")}>
      <section className={styles["modal-main"]}>
        <button
          type="button"
          className={["btn-close", styles["modal-main-btn-close"]].join(" ")}
          onClick={onClose}
        ></button>
        {children}
      </section>
    </div>
  );
};

export default Modal;
