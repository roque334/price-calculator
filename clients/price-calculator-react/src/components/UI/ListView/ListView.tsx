import React, { Fragment, useCallback, useEffect, useRef, useState } from "react";
import { ListItem } from "../../../models/list";
import ListViewItem from "./ListViewItem/ListViewItem";
import Spinner from "../Spinner/Spinner";

interface ListViewProps {
  elements: ListItem[] ;
  total: number;
  loading: boolean;
  onEdit: (element: ListItem) => void;
  onRemove: (element: ListItem) => void;
  onLoadMoreData: (skip: number, take: number) => void;
}

const ListView: React.FC<ListViewProps> = ({ elements, total, loading, onEdit, onRemove, onLoadMoreData }) => {
  const [scrollY, setScrollY] = useState<number | undefined>(undefined);
  const [take, setTake] = useState(10);
  const loader = useRef<HTMLDivElement>(null);

  useEffect(() => {
    setScrollY(window.scrollY);
    onLoadMoreData(0, take);
  }, [take, setScrollY]);

  useEffect(() => {
    if(scrollY) {
      setTimeout(() => {
        window.scrollTo(0, scrollY);
      }, 250);
    }
  }, [elements, scrollY]);

  const handleObserver = useCallback((entries) => {
    const target = entries[0];
    if (elements && elements.length > 0 && elements.length < total && !loading && target.isIntersecting ) {
      setTake((prevTake) => prevTake + 10);
    }
  }, [elements, total, loading]);

  useEffect(() => {
    if (!loading) {
      const option = {
        root: null,
        rootMargin: "20px",
        threshold: 0
      };
      const observer = new IntersectionObserver(handleObserver, option);
      if (loader.current) observer.observe(loader.current);
    }
  }, [handleObserver, loading]);

  if (loading) return <Spinner />;

  const items = elements.map((element) => {
    return (
        <ListViewItem
          key={element.id}
          item={element}
          onEdit={() => onEdit(element)}
          onRemove={() => onRemove(element)}
        />
    );
  });

  return (
    <Fragment>
      {items && items.length > 0 ? items : null}
      <div ref={loader} />
    </Fragment>
  );
};

export default ListView;
