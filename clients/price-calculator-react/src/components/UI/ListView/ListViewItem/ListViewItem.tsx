import React from "react";
import { ListItem } from "../../../../models/list";
import styles from "./ListViewItem.module.css";

interface ListViewItemProps {
  item: ListItem;
  onEdit: (id: number) => void;
  onRemove: (id: number) => void;
}

const ListViewItem: React.FC<ListViewItemProps> = ({
  item,
  onEdit,
  onRemove,
}) => {
  return (
    <div className="card">
      <div className={styles["card-body"]}>
        <div className={styles["card-body-elements"]}>
          <h5 className="card-title">{item.name}</h5>
          <p className="card-text">{item.description}</p>
        </div>
        <div className={styles["card-body-actions"]}>
          <button data-testid={`Update-${item.id}`} className={`btn btn-primary ${styles["card-body-action"]}`} onClick={() => onEdit(item.id)}>
            Update
          </button>
          <button data-testid={`Remove-${item.id}`} className="btn btn-danger" onClick={() => onRemove(item.id)}>
            Remove
          </button>
        </div>
      </div>
    </div>
  );
};

export default ListViewItem;
