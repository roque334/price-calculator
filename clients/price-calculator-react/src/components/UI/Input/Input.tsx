import React, {
  InputHTMLAttributes,
  SelectHTMLAttributes,
  TextareaHTMLAttributes,
} from "react";
import { ValidationProps } from "../../../validators/validator";
import styles from "./Input.module.css";

export enum InputType {
  Input = "input",
  TextArea = "textarea",
  Select = "select",
  CheckBox = "checkbox",
}

interface BasicProps {
  name: string;
  elementType: InputType;
  label: string;
  touched: boolean;
  valid: boolean;
  value?: string | ReadonlyArray<string> | number | boolean | undefined;
}

export interface InputProps extends BasicProps {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  htmlAttributes: Omit<
    InputHTMLAttributes<any>,
    "onChange" | "value" | "checked"
  >;
}

export interface CheckBoxProps extends BasicProps {
  onChange: React.ChangeEventHandler<HTMLInputElement>;
  htmlAttributes: Omit<
    InputHTMLAttributes<any>,
    "onChange" | "value" | "checked"
  >;
}

export interface TextAreaProps extends BasicProps {
  onChange: React.ChangeEventHandler<HTMLTextAreaElement>;
  htmlAttributes: Omit<TextareaHTMLAttributes<any>, "onChange" | "value">;
}

export interface SelectProps extends BasicProps {
  autoComplete: boolean,
  options: {
    displayValue: string;
    value: string | number;
  }[];
  onChange: React.ChangeEventHandler<HTMLSelectElement>;
  htmlAttributes: Omit<SelectHTMLAttributes<any>, "onChange" | "value" | "autoComplete">;
}

const Input: React.FC<InputProps | TextAreaProps | SelectProps> = (props) => {
  let inputElement:
    | React.DetailedHTMLProps<
        React.InputHTMLAttributes<HTMLInputElement>,
        HTMLInputElement
      >
    | React.DetailedHTMLProps<
        React.TextareaHTMLAttributes<HTMLTextAreaElement>,
        HTMLTextAreaElement
      >
    | React.DetailedHTMLProps<
        React.SelectHTMLAttributes<HTMLSelectElement>,
        HTMLSelectElement
      >;
  const inputStyles = ["form-control"];

  if (props.touched && !props.valid) {
    inputStyles.push(styles["form-control-invalid"]);
  }

  switch (props.elementType) {
    case InputType.TextArea:
      const textareaProps = props as TextAreaProps;

      inputElement = (
        <textarea
          id={textareaProps.name}
          cols={30}
          rows={5}
          className={inputStyles.join(" ")}
          value={
            textareaProps.value as
              | string
              | ReadonlyArray<string>
              | number
              | undefined
          }
          onChange={textareaProps.onChange}
          {...textareaProps.htmlAttributes}
        />
      );
      break;
    case InputType.Select:
      const selectProps = props as SelectProps;

      inputElement = (
        <select
          id={selectProps.name}
          className={inputStyles.join(" ")}
          autoComplete={selectProps.autoComplete ? "on" : "off"}
          value={
            selectProps.value as
              | string
              | ReadonlyArray<string>
              | number
              | undefined
          }
          onChange={selectProps.onChange}
          {...selectProps.htmlAttributes}
        >
          {selectProps.options.map((element) => {
            return (
              <option key={element.value} value={element.value}>
                {element.displayValue}
              </option>
            );
          })}
        </select>
      );
      break;
    case InputType.CheckBox:
      const checkBoxProps = props as CheckBoxProps;

      inputElement = (
        <input
          id={checkBoxProps.name}
          className={inputStyles.join(" ")}
          checked={checkBoxProps.value as boolean | undefined}
          onChange={checkBoxProps.onChange}
          {...checkBoxProps.htmlAttributes}
        />
      );
      break;

    default:
      const inputProps = props as InputProps;

      inputElement = (
        <input
          id={inputProps.name}
          className={inputStyles.join(" ")}
          value={
            inputProps.value as
              | string
              | ReadonlyArray<string>
              | number
              | undefined
          }
          onChange={inputProps.onChange}
          {...inputProps.htmlAttributes}
        />
      );
      break;
  }

  return (
    <div className="mb-3">
      <label htmlFor={props.name} className={["form-label"].join(" ")}>
        {props.label}
      </label>
      {inputElement}
    </div>
  );
};

export default Input;

export interface FormInputProps {
  configuration: InputProps | CheckBoxProps | SelectProps | TextAreaProps;
  validation?: ValidationProps | undefined;
}
