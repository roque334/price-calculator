import React from "react";
import {
  cleanup,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import Input, {
  InputProps,
  InputType,
  SelectProps,
  TextAreaProps,
} from "./Input";

afterEach(cleanup);

describe("Regular input field", () => {
  const inputProps: InputProps = {
    name: "firstname",
    elementType: InputType.Input,
    label: "Firstname",
    touched: false,
    valid: false,
    value: "John",
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => {},
    htmlAttributes: { type: "text", role: "firstname" },
  };

  it("should render string value", () => {
    render(<Input {...inputProps} />);

    expect(screen.getByLabelText("Firstname")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John")).toHaveAttribute("type", "text");
    expect(screen.getByDisplayValue("John").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should render number value", () => {
    inputProps.value = 5;
    render(<Input {...inputProps} />);

    expect(screen.getByLabelText("Firstname")).toBeInTheDocument();
    expect(screen.getByDisplayValue("5")).toBeInTheDocument();
    expect(screen.getByDisplayValue("5")).toHaveAttribute("type", "text");
    expect(screen.getByDisplayValue("5").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should render number value", () => {
    inputProps.value = ["John", "Mary"];
    render(<Input {...inputProps} />);

    expect(screen.getByLabelText("Firstname")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John,Mary")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John,Mary")).toHaveAttribute(
      "type",
      "text"
    );
    expect(screen.getByDisplayValue("John,Mary").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should have specific style when input is touched and invalid", () => {
    const props: InputProps = {
      ...inputProps,
      value: "Invalid",
      touched: true,
      valid: false,
    };
    render(<Input {...props} />);

    expect(screen.getByLabelText("Firstname")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Invalid")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Invalid").className).toContain(
      "form-control-invalid"
    );
  });
});

describe("Textarea field", () => {
  const textAreaProps: TextAreaProps = {
    name: "comment",
    elementType: InputType.TextArea,
    label: "Comment",
    touched: false,
    valid: false,
    value: "This is a comment example",
    onChange: (event: React.ChangeEvent<HTMLTextAreaElement>) => {},
    htmlAttributes: { role: "comment" },
  };

  it("should render string value", () => {
    render(<Input {...textAreaProps} />);

    expect(screen.getByLabelText("Comment")).toBeInTheDocument();
    expect(
      screen.getByDisplayValue("This is a comment example")
    ).toBeInTheDocument();
    expect(
      screen.getByDisplayValue("This is a comment example").className
    ).not.toContain("form-control-invalid");
  });

  it("should render number value", () => {
    textAreaProps.value = 5;
    render(<Input {...textAreaProps} />);

    expect(screen.getByLabelText("Comment")).toBeInTheDocument();
    expect(screen.getByDisplayValue("5")).toBeInTheDocument();
    expect(screen.getByDisplayValue("5").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should render number value", () => {
    textAreaProps.value = ["John", "Mary"];
    render(<Input {...textAreaProps} />);

    expect(screen.getByLabelText("Comment")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John,Mary")).toBeInTheDocument();
    expect(screen.getByDisplayValue("John,Mary").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should have specific style when input is touched and invalid", () => {
    const props: TextAreaProps = {
      ...textAreaProps,
      value: "Invalid",
      touched: true,
      valid: false,
    };
    render(<Input {...props} />);

    expect(screen.getByLabelText("Comment")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Invalid")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Invalid").className).toContain(
      "form-control-invalid"
    );
  });
});

describe("Select field", () => {
  const selectProps: SelectProps = {
    name: "option",
    elementType: InputType.Select,
    label: "Option",
    touched: false,
    valid: false,
    options: [
      {
        displayValue: "Option 1",
        value: "option1",
      },
      {
        displayValue: "Option 2",
        value: "option2",
      },
      {
        displayValue: "Option 3",
        value: "option3",
      },
    ],
    value: "option1",
    onChange: (event: React.ChangeEvent<HTMLSelectElement>) => {},
    htmlAttributes: { role: "option" },
  };

  it("should render string value", () => {
    render(<Input {...selectProps} />);

    expect(screen.getByLabelText("Option")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should render number value", () => {
    selectProps.value = 1;
    selectProps.options = [
      {
        displayValue: "Option 1",
        value: 1,
      },
      {
        displayValue: "Option 2",
        value: 2,
      },
      {
        displayValue: "Option 3",
        value: 3,
      },
    ];

    render(<Input {...selectProps} />);

    expect(screen.getByLabelText("Option")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1").className).not.toContain(
      "form-control-invalid"
    );
  });

  it("should have specific style when input is touched and invalid", () => {
    const props: SelectProps = {
      ...selectProps,
      touched: true,
      valid: false,
    };
    render(<Input {...props} />);

    expect(screen.getByLabelText("Option")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1")).toBeInTheDocument();
    expect(screen.getByDisplayValue("Option 1").className).toContain(
      "form-control-invalid"
    );
  });
});
