import React from "react";
import { Redirect, Route } from "react-router";
import { RouteProps } from "react-router-dom";
import { useAppSelector } from "../../../hooks";

interface PrivateRouteProps extends RouteProps {}

const PrivateRoute: React.FC<PrivateRouteProps> = (props) => {
  const user = useAppSelector((state) => state.auth.user);
  if (user) {
    return <Route {...props} />
  }

  return <Redirect to="/login" />;
};

export default PrivateRoute;
