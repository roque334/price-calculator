import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { cleanup, render, screen, waitFor } from "@testing-library/react";
import store from "../../../store/store";
import { useAppDispatch } from "../../../hooks";
import { authActions } from "../../../store/slices/auth-slice";
import PrivateRoute from "./PrivateRoute";
import Materials from "../../Materials/Materials";

const FakeLoginComponent: React.FC<{ doLogin: boolean }> = ({ doLogin }) => {
  const dispatch = useAppDispatch();

  if (doLogin) {
    dispatch(
      authActions.authActionSuccess({
        firstname: "test3",
        lastname: "test3",
        token:
          "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiJ0ZXN0MyIsIm5iZiI6MTYyODUzMTUzOCwiZXhwIjoxNjI5MTM2MzM4LCJpYXQiOjE2Mjg1MzE1Mzh9.CSyM4r3dN3gv2dShQvwvrALNKznxesXp-FlGQGzltN-9FOWABAO34pOSGz8xq4BPZjdk8OzuCBM_zX7quOsmVg",
      })
    );
  }

  return <h1>Test</h1>;
};

describe("PrivateRoute Test", () => {
  afterEach(cleanup);

  it("should navigate to the right component", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <FakeLoginComponent doLogin={true} />
          <Switch>
            <PrivateRoute exact path="/" component={Materials} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );

    expect(screen.getByText(/Test/i)).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByText(/Materials/i)).toBeInTheDocument();
    });
  });

  it("should navigate to the SignIn component", async () => {
    render(
      <Provider store={store}>
        <BrowserRouter>
          <FakeLoginComponent doLogin={false} />
          <Switch>
            <PrivateRoute exact path="/" component={Materials} />
          </Switch>
        </BrowserRouter>
      </Provider>
    );

    expect(screen.getByText(/Test/i)).toBeInTheDocument();

    await waitFor(() => {
      expect(screen.getByText(/Test/i)).toBeInTheDocument();
    });
  });
});
