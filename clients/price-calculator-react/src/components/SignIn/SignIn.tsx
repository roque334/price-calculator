import React, { Fragment, useEffect, useState } from "react";
import { Redirect } from "react-router";
import { Link } from "react-router-dom";
import { useAppSelector, useAppDispatch } from "../../hooks";
import { authActions, loginAction } from "../../store/slices/auth-slice";
import { checkValidity, ValidationProps } from "../../validators/validator";
import Input, {
  CheckBoxProps,
  FormInputProps,
  InputProps,
  InputType,
  SelectProps,
  TextAreaProps,
} from "../UI/Input/Input";

const SignIn: React.FC = () => {
  const [signInForm, setSignInForm] = useState({
    email: {
      configuration: {
        elementType: InputType.Input,
        name: "email-address",
        label: "Email Address",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "email" },
      } as InputProps,
      validation: {
        required: true,
        isEmail: true,
      } as ValidationProps,
    } as FormInputProps,
    password: {
      configuration: {
        elementType: InputType.Input,
        name: "password",
        label: "Password",
        touched: false,
        valid: false,
        value: "",
        htmlAttributes: { type: "password" },
      } as InputProps,
      validation: {
        required: true,
        minLength: 6,
      } as ValidationProps,
    } as FormInputProps,
  });
  const [isFormValid, setIsFormValid] = useState(false);
  const dispatch = useAppDispatch();
  const user = useAppSelector((state) => state.auth.user);
  const authError = useAppSelector((state) => state.auth.loginError);

  useEffect(() => {
    if (authError && authError.length > 0) {
      alert(authError);
      dispatch(authActions.cleanErrorsAction(null));
    }
  }, [authError, dispatch]);

  const inputChangeHandler = (
    event:
      | React.ChangeEvent<HTMLInputElement>
      | React.ChangeEvent<HTMLSelectElement>
      | React.ChangeEvent<HTMLTextAreaElement>,
    id: keyof typeof signInForm
  ) => {
    const updatedSignInForm = { ...signInForm };
    const updatedElementSignInForm = { ...signInForm[id] };
    updatedElementSignInForm.configuration.value = event.target.value;
    updatedElementSignInForm.configuration.touched = true;
    updatedElementSignInForm.configuration.valid =
      updatedElementSignInForm.validation === undefined
        ? true
        : checkValidity(
            updatedElementSignInForm.configuration.value,
            updatedElementSignInForm.validation
          );
    updatedSignInForm[id] = updatedElementSignInForm;

    let formValid = true;
    let key: keyof typeof updatedSignInForm;
    for (key in updatedSignInForm) {
      if (!updatedSignInForm[key].configuration.valid) {
        formValid = false;
        break;
      }
    }

    setSignInForm(updatedSignInForm);
    setIsFormValid(formValid);
  };

  if (user) {
    return <Redirect to="/" />;
  }

  const onSignInHandler: React.FormEventHandler<HTMLFormElement> = (event) => {
    event.preventDefault();
    dispatch(
      loginAction({
        email: (signInForm.email.configuration as InputProps).value as string,
        password: (signInForm.password.configuration as InputProps)
          .value as string,
      })
    );
  };

  const formElementArray: {
    id: string;
    config: InputProps | CheckBoxProps | SelectProps | TextAreaProps;
  }[] = [];
  let key: keyof typeof signInForm;
  for (key in signInForm) {
    formElementArray.push({
      id: key,
      config: signInForm[key].configuration,
    });
  }

  return (
    <Fragment>
      <h1>Sign In</h1>
      <h2>Create an Account</h2>
      <form data-testid="form" onSubmit={onSignInHandler}>
        {formElementArray.map((element) => {
          return (
            <Input
              key={element.id}
              {...element.config}
              onChange={(
                event:
                  | React.ChangeEvent<HTMLInputElement>
                  | React.ChangeEvent<HTMLTextAreaElement>
                  | React.ChangeEvent<HTMLSelectElement>
              ) =>
                inputChangeHandler(event, element.id as keyof typeof signInForm)
              }
            />
          );
        })}
        <button
          type="submit"
          className="btn btn-primary"
          disabled={!isFormValid}
        >
          Login
        </button>
      </form>
      <span>
        Don't have an account? <Link to="/register">Sign Up</Link>
      </span>
    </Fragment>
  );
};

export default SignIn;