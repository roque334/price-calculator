namespace Material.ComponentTests.Repositories;

using System.Threading.Tasks;

using AutoFixture;

using Customizations;

using Material.Application.Specifications;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

using static Constants;

using static Testing;

[TestFixture]
public class MaterialRepositoryTests : TestBase
{
    private readonly Fixture _fixture = new();

    [SetUp]
    public void TestSetUp() => _fixture.Customize(new JohnUnitMaterialCustomization());

    [Test]
    public async Task Create_HappyFlow_CreatesMaterialEntity()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();

        // Act
        var created = await AddMaterialAsync(material);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBeGreaterThan(0),
            () => found.Name.ShouldBe(material.Name),
            () => found.MeasurementUnit.Id.ShouldBe(material.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(material.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(material.MeasurementValue),
            () => found.Price.ShouldBe(material.Price),
            () => found.UserName.ShouldBe(material.UserName),
            () => found.Version.ShouldBe(0)
        );
    }

    [Test]
    public async Task Create_MaterialEntityAlreadyExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateException>(() => AddMaterialAsync(material));
    }

    [Test]
    public async Task Update_HappyFlow_UpdatesMaterialEntity()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        // Act
        var updated = await UpdateMaterialAsync(created);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBe(created.Id),
            () => found.Name.ShouldBe(material.Name),
            () => found.MeasurementUnit.Id.ShouldBe(material.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(material.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(material.MeasurementValue),
            () => found.Price.ShouldBe(material.Price),
            () => found.UserName.ShouldBe(material.UserName),
            () => found.Version.ShouldBe(1)
        );
    }

    [Test]
    public async Task Update_MaterialEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);
        var deleted = await DeleteMaterialAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateMaterialAsync(created));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));
        found.ShouldBeNull();
    }

    [Test]
    public async Task Update_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[materials] SET MeasurementValue = 50, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateMaterialAsync(created));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBe(created.Id),
            () => found.Name.ShouldBe(material.Name),
            () => found.MeasurementUnit.Id.ShouldBe(material.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(material.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(50),
            () => found.Price.ShouldBe(material.Price),
            () => found.UserName.ShouldBe(material.UserName),
            () => found.Version.ShouldBe(2)
        );
    }


    [Test]
    public async Task Delete_HappyFlow_UpdatesMaterialEntity()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        // Act
        var updated = await DeleteMaterialAsync(created);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_MaterialEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);
        var updated = await DeleteMaterialAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteMaterialAsync(updated));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[materials] SET MeasurementValue = 50, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteMaterialAsync(created));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBe(created.Id),
            () => found.Name.ShouldBe(material.Name),
            () => found.MeasurementUnit.Id.ShouldBe(material.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(material.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(50),
            () => found.Price.ShouldBe(material.Price),
            () => found.UserName.ShouldBe(material.UserName),
            () => found.Version.ShouldBe(2)
        );
    }

    [Test]
    public async Task Find_MaterialWithIdAndUsernameSpecification_FindsMaterialEntity()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        // Act    
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBe(created.Id),
            () => found.Name.ShouldBe(created.Name),
            () => found.MeasurementUnit.Id.ShouldBe(created.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(created.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(created.MeasurementValue),
            () => found.Price.ShouldBe(created.Price),
            () => found.UserName.ShouldBe(created.UserName),
            () => found.Version.ShouldBe(0)
        );
    }

    [Test]
    public async Task Find_MaterialWithNameAndUsernameSpecification_FindsMaterialEntity()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        var created = await AddMaterialAsync(material);

        // Act    
        var found = await FindMaterialAsync(new MaterialWithNameAndUsernameSpecification(created.Name, created.UserName));

        found.ShouldSatisfyAllConditions(
            () => found.Id.ShouldBe(created.Id),
            () => found.Name.ShouldBe(created.Name),
            () => found.MeasurementUnit.Id.ShouldBe(created.MeasurementUnit.Id),
            () => found.MeasurementUnit.Name.ShouldBe(created.MeasurementUnit.Name),
            () => found.MeasurementValue.ShouldBe(created.MeasurementValue),
            () => found.Price.ShouldBe(created.Price),
            () => found.UserName.ShouldBe(created.UserName),
            () => found.Version.ShouldBe(0)
        );
    }

    [Test]
    public async Task Find_MaterialsWithUsernameSpecification_FindsMaterialEntity()
    {
        // Arrange
        var material1 = _fixture.Create<MaterialEntity>();
        await AddMaterialAsync(material1);
        var material2 = _fixture.Create<MaterialEntity>();
        await AddMaterialAsync(material2);
        var material3 = _fixture.Create<MaterialEntity>();
        material3.UserName = MARYDOE_USERNAME;
        await AddMaterialAsync(material3);

        // Act    
        var found = await FindMaterialsAsync(new MaterialsWithUsernameSpecification(JOHNDOE_USERNAME));

        // Assert
        found.ShouldNotBeNull();
        found.Length.ShouldBe(2);

        found.ShouldContain(m => m.Name == material1.Name);
        found.ShouldContain(m => m.Name == material2.Name);
        found.ShouldNotContain(m => m.Name == material3.Name);
    }

    [Test]
    public async Task Find_MaterialsContainsNameWithUsernameSpecification_FindsMaterialEntities()
    {
        var material1 = _fixture.Create<MaterialEntity>();
        await AddMaterialAsync(material1);
        var material2 = _fixture.Create<MaterialEntity>();
        await AddMaterialAsync(material2);
        var material3 = _fixture.Create<MaterialEntity>();
        material3.UserName = MARYDOE_USERNAME;
        await AddMaterialAsync(material3);

        // Act    
        var found = await FindMaterialsAsync(new MaterialsLikeNameWithUsernameSpecification(material1.Name[0..^1], JOHNDOE_USERNAME));

        // Assert
        found.ShouldNotBeNull();
        found.Length.ShouldBe(1);
        found.ShouldContain(m => m.Name == material1.Name);
    }
}
