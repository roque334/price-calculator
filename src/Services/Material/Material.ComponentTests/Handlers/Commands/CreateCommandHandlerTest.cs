namespace Material.ComponentTests.Handlers;

using System;
using System.Threading.Tasks;

using AutoFixture;

using Core.Domain.Models;

using Customizations;

using Material.Application.Handlers.Commands;
using Material.Application.Exceptions;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

using static Testing;

using static Constants;

[TestFixture]
public class CreateCommandHandlerTest : TestBase
{
    private readonly Fixture _fixture = new();

    [SetUp]
    public void TestSetUp() => _fixture.Customize(new CreateCommandCustomization());

    [Test]
    public async Task CreateCommandHandler_HappyFlow_MaterialCreated()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();

        // Act
        var created = await SendAsync(createCommand);

        // Assert
        created.ShouldSatisfyAllConditions(
            () => created.Id.ShouldBeGreaterThan(0),
            () => created.Name.ShouldBe(createCommand.Name),
            () => created.MeasurementUnit.Id.ShouldBe(Enumeration.FromDisplayName<MeasurementUnit>(createCommand.MeasurementUnit).Id),
            () => created.MeasurementUnit.Name.ShouldBe(Enumeration.FromDisplayName<MeasurementUnit>(createCommand.MeasurementUnit).Name),
            () => created.MeasurementValue.ShouldBe(createCommand.MeasurementValue),
            () => created.Price.ShouldBe(createCommand.Price),
            () => created.UserName.ShouldBe(JOHNDOE_USERNAME),
            () => created.Version.ShouldBe(0)
        );

        (await ShouldHaveReceivedMaterialCreatedIntegrationEventHandler(created.Name, TimeSpan.FromSeconds(3))).ShouldBeTrue();
    }

    [Test]
    public async Task CreateCommandHandler_MaterialAlreadyExists_ThrowsMaterialException()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        var created = await SendAsync(createCommand);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => SendAsync(createCommand));
    }
}
