namespace Material.ComponentTests.Handlers;

using System;
using System.Threading.Tasks;

using AutoFixture;

using Customizations;

using Material.Application.Handlers.Commands;
using Material.Application.Exceptions;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

using static Constants;

using static Testing;
using Core.Domain.Models;

[TestFixture]
public class UpdateCommandHandlerTest : TestBase
{
    private readonly Fixture _fixture = new();
    [SetUp]
    public void TestSetUp() => _fixture.Customize(new JohnUnitMaterialCustomization());

    [Test]
    public async Task UpdateCommandHandler_HappyFlow_MaterialCreated()
    {
        // Arrange
        var material = _fixture.Create<MaterialEntity>();
        await AddMaterialAsync(material);

        var updateCommand = new UpdateCommand
        {
            Name = material.Name,
            MeasurementUnit = material.MeasurementUnit.Name,
            MeasurementValue = material.MeasurementValue,
            Price = material.Price
        };

        // Act
        var updated = await SendAsync(updateCommand);

        // Assert
        updated.ShouldSatisfyAllConditions(
            () => updated.Id.ShouldBeGreaterThan(0),
            () => updated.Name.ShouldBe(updateCommand.Name),
            () => updated.MeasurementUnit.Id.ShouldBe(Enumeration.FromDisplayName<MeasurementUnit>(updateCommand.MeasurementUnit).Id),
            () => updated.MeasurementUnit.Name.ShouldBe(Enumeration.FromDisplayName<MeasurementUnit>(updateCommand.MeasurementUnit).Name),
            () => updated.MeasurementValue.ShouldBe(updateCommand.MeasurementValue),
            () => updated.Price.ShouldBe(updateCommand.Price),
            () => updated.UserName.ShouldBe(JOHNDOE_USERNAME),
            () => updated.Version.ShouldBe(1)
        );

        (await ShouldHaveReceivedMaterialUpdatedIntegrationEventHandler(updated.Name, TimeSpan.FromSeconds(3))).ShouldBeTrue();
    }

    [Test]
    public async Task UpdateCommandHandlerMaterialDoesNotExists_ThrowsMaterialException()
    {
        // Arrange
        var updateCommand = _fixture.Create<UpdateCommand>();

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => SendAsync(updateCommand));
    }
}
