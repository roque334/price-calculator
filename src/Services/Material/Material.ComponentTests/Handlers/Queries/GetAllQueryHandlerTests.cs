using System.Linq;
using System.Threading.Tasks;

using Material.Application.Handlers.Queries;
using Material.Application.Models;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

namespace Material.ComponentTests.Handlers.Queries
{
    using static Constants;
    using static Testing;

    [TestFixture]
    public class GetAllQueryHandlerTests : TestBase
    {
        [Test]
        public async Task Handle_HappyFlow_GetsMaterials()
        {
            // Arrange
            var paginationParams = new PaginationParams(0, 20);
            var material = new MaterialEntity("Envelope", MeasurementUnit.Units.Id, 100, 5.99M, JOHNDOE_USERNAME);
            await AddMaterialAsync(material);

            material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 150, 1.99M, JOHNDOE_USERNAME);
            await AddMaterialAsync(material);

            material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 200, 2.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material);

            // Act
            var found = await SendAsync(new GetAllQuery(paginationParams));

            // Assert
            found.ShouldNotBeNull();
            found.Total.ShouldBe(2);
            found.Materials.Length.ShouldBe(2);
            found.Materials[0].Name.ShouldBe("Envelope");
            found.Materials[1].Name.ShouldBe("Paper");
        }

        [Test]
        public async Task Handle_MaterialsDoesNotExists_ReturnsEmptyMaterialCollection()
        {
            // Arrange
            var paginationParams = new PaginationParams(0, 20);
            var material = new MaterialEntity("Envelope", MeasurementUnit.Units.Id, 100, 5.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material); material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 200, 2.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material);

            // Act
            var found = await SendAsync(new GetAllQuery(paginationParams));

            // Assert
            found.ShouldNotBeNull();
            found.Total.ShouldBe(0);
            found.Materials.ShouldBeEmpty();
        }
    }
}
