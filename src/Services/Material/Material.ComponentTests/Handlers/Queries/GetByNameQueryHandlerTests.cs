using System.Linq;
using System.Threading.Tasks;

using Material.Application.Handlers.Queries;
using Material.Application.Models;
using Material.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

namespace Material.ComponentTests.Handlers.Queries
{
    using static Constants;
    using static Testing;

    [TestFixture]
    public class GetByNameQueryHandlerTests : TestBase
    {
        [Test]
        public async Task Handle_HappyFlow_GetsMaterials()
        {
            // Arrange
            var material = new MaterialEntity("Envelope", MeasurementUnit.Units.Id, 100, 5.99M, JOHNDOE_USERNAME);
            await AddMaterialAsync(material);

            material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 150, 1.99M, JOHNDOE_USERNAME);
            await AddMaterialAsync(material);

            material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 200, 2.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material);

            // Act
            var found = await SendAsync(new GetByNameQuery("pe"));

            // Assert
            found.ShouldNotBeNull();
            found.Length.ShouldBe(2);
            found[0].Name.ShouldBe("Envelope");
            found[1].Name.ShouldBe("Paper");
        }

        [Test]
        public async Task Handle_MaterialsDoesNotExists_ReturnsEmptyMaterialCollection()
        {
            // Arrange
            var material = new MaterialEntity("Envelope", MeasurementUnit.Units.Id, 100, 5.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material); material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 200, 2.99M, MARYDOE_USERNAME);
            await AddMaterialAsync(material);

            // Act
            var found = await SendAsync(new GetByNameQuery("pe"));

            // Assert
            found.ShouldNotBeNull();
            found.ShouldBeEmpty();
        }
    }
}
