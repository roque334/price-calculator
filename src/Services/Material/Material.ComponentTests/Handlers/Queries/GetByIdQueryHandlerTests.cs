using System;
using System.Threading.Tasks;
using Material.Application.Exceptions;
using Material.Application.Handlers.Queries;
using Material.Domain.Models.MaterialAggregate;
using NUnit.Framework;
using Shouldly;

namespace Material.ComponentTests.Handlers.Queries
{
    using static Constants;
    using static Testing;

    [TestFixture]
    public class GetByIdQueryHandlerTests : TestBase
    {
        [Test]
        public async Task Handle_HappyFlow_GetsMaterial()
        {
            // Arrange
            var material = new MaterialEntity("Paper", MeasurementUnit.Units.Id, 150, 1.56M, JOHNDOE_USERNAME);
            var created = await AddMaterialAsync(material);

            var query = new GetByIdQuery(created.Id);
            // Act
            var found = await SendAsync(query);

            // Assert
            found.ShouldNotBeNull();
            found.Id.ShouldBeGreaterThan(0);
            found.Name.ShouldBe(created.Name);
            found.MeasurementUnit.Id.ShouldBe(created.MeasurementUnit.Id);
            found.MeasurementUnit.Name.ShouldBe(created.MeasurementUnit.Name);
            found.Price.ShouldBe(created.Price);
            found.UserName.ShouldBe(JOHNDOE_USERNAME);
            found.Version.ShouldBe(0);
        }

        [Test]
        public async Task Handle_MaterialDoesNotExists_ThrowsException()
        {
            // Arrange
            var query = new GetByIdQuery(new Random().Next());

            // Act + Assert
            await Should.ThrowAsync<MaterialException>(() =>  SendAsync(query));
        }
    }
}
