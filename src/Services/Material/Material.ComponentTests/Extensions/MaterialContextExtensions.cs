namespace Material.ComponentTests.Extensions;

using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

using Material.Data.Context;
using Material.Data.Seeds;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using Polly;
using Polly.Retry;

public static class MaterialContextExtensions
{
    public async static Task ApplyMigrationsAsync(this MaterialContext context)
    {
        try
        {
            var policy = RetryPolicy
            .Handle<SqlException>()
            .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
            {
            });

            await policy.Execute(async () =>
            {
                await context.Database.MigrateAsync().ConfigureAwait(false);
            });
        }
        catch (Exception)
        {
            throw;
        }
    }

    public async static Task ApplySeedsAsync(this MaterialContext context, IServiceProvider services)
    {
        try
        {
            var policy = RetryPolicy
            .Handle<SqlException>()
            .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
            {
            });

            await policy.Execute(async () =>
            {
                await new MaterialContextSeed()
                    .SeedAsync(context, services.GetRequiredService<ILogger<MaterialContextSeed>>())
                    .ConfigureAwait(false);
            });
        }
        catch (Exception)
        {
            throw;
        }
    }
}
