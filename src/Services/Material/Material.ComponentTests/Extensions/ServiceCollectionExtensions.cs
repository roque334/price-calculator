namespace Material.ComponentTests.Extensions;

using Material.Application.UserAccessors;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Moq;

using static Material.ComponentTests.Constants;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddWebHostEnvironment(this IServiceCollection services)
    {
        var webHostEnvironmentMock = new Mock<IWebHostEnvironment>();
        webHostEnvironmentMock.Setup(w => w.EnvironmentName).Returns("Development");
        webHostEnvironmentMock.Setup(w => w.ApplicationName).Returns("Material.API");
        services.AddSingleton(webHostEnvironmentMock.Object);

        return services;
    }

    public static IServiceCollection AddMockedCustomServices(this IServiceCollection services)
    {
        var userAccessorMock = new Mock<IUserAccessor>();
        userAccessorMock.Setup(o => o.GetCurrentUsername()).Returns(JOHNDOE_USERNAME);
        services.AddScoped(_ => userAccessorMock.Object);

        return services;
    }
}
