namespace Material.ComponentTests;

using System.Threading.Tasks;

using NUnit.Framework;

using static Material.ComponentTests.Testing;

[TestFixture]
public class TestBase
{
    [SetUp]
    public async Task SetUp() => await ResetState();
}
