namespace Material.ComponentTests.Customizations;

using AutoFixture;

using Domain.Models.MaterialAggregate;

public class JohnUnitMaterialCustomization : ICustomization
{
    public void Customize(IFixture fixture) =>
        fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .With(e => e.UserName, Constants.JOHNDOE_USERNAME)
            .Without(e => e.MeasurementUnit));
}
