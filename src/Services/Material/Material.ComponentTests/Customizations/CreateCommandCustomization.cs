namespace Material.ComponentTests.Customizations;

using AutoFixture;

using Material.Application.Handlers.Commands;
using Material.Domain.Models.MaterialAggregate;

public class CreateCommandCustomization : ICustomization
{
    public void Customize(IFixture fixture) =>
        fixture.Customize<CreateCommand>(composer => composer
            .With(e => e.MeasurementUnit, MeasurementUnit.Units.Name));
}
