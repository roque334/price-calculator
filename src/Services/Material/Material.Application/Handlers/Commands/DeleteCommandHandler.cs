namespace Material.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using Exceptions;

using Material.Domain.Models.MaterialAggregate;

using MaterialIntegrationEventServices;

using MediatR;

using Specifications;

using UserAccessors;

public class DeleteCommandHandler : IRequestHandler<DeleteCommand, Unit>
{
    private readonly IMaterialRepository _repository;
    private readonly IUserAccessor _userAccessor;
    public readonly IMaterialIntegrationEventService _materialIntegrationEventService;

    public DeleteCommandHandler(IMaterialRepository materialRepository, IUserAccessor userAccessor, IMaterialIntegrationEventService materialIntegrationEventService)
    {
        _repository = materialRepository;
        _userAccessor = userAccessor;
        _materialIntegrationEventService = materialIntegrationEventService;
    }

    public async Task<Unit> Handle(DeleteCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var material = await _repository.FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(request.Id, username));
        if (material == null)
        {
            throw new MaterialException(HttpStatusCode.NotFound, new { message = $"Material not found." });
        }

        var deleted = _repository.Delete(material);

        var result = await _repository.UnitOfWork
            .SaveEntitiesAsync(cancellationToken);

        if (result)
        {
            var integrationEvent = new MaterialDeletedIntegrationEvent(deleted.Id, deleted.Name, username);
            await _materialIntegrationEventService
                .PublishThroughEventBusAsync
                (
                    integrationEvent
                );

            return Unit.Value;
        }

        throw new Exception("Problem saving changes");
    }
}
