namespace Material.Application.Handlers.Commands;

using Application.Models;

using MediatR;

public class UpdateCommand : IRequest<MaterialDetailDTO>
{
    public string Name { get; set; }
    public string MeasurementUnit { get; set; }
    public int MeasurementValue { get; set; }
    public decimal Price { get; set; }
}
