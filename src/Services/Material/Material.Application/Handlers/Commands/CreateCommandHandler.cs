namespace Material.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using Common.Events.Models;

using Domain.Models.MaterialAggregate;

using Exceptions;

using MaterialIntegrationEventServices;

using MediatR;

using Microsoft.Extensions.Logging;

using Models;

using Specifications;

using UserAccessors;

public class CreateCommandHandler : IRequestHandler<CreateCommand, MaterialDetailDTO>
{
    private readonly IMaterialRepository _materialRepository;
    private readonly IUserAccessor _userAccessor;
    private readonly IMaterialIntegrationEventService _materialIntegrationEventService;
    private readonly IMapper _mapper;
    public CreateCommandHandler(IMaterialRepository materialRepository, IUserAccessor userAccessor, IMaterialIntegrationEventService materialIntegrationEventService, IMapper mapper)
    {
        _materialRepository = materialRepository;
        _userAccessor = userAccessor;
        _materialIntegrationEventService = materialIntegrationEventService;
        _mapper = mapper;
    }

    public async Task<MaterialDetailDTO> Handle(CreateCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        if ((await _materialRepository.FindMaterialAsync(new MaterialWithNameAndUsernameSpecification(request.Name, username))) != null)
        {
            throw new MaterialException(HttpStatusCode.BadRequest, new { message = $"The material {request.Name} already exists." });
        }

        var material = _mapper.Map<CreateCommand, MaterialEntity>(request);
        material.UserName = username;

        var created = _materialRepository.Add(material);

        var result = await _materialRepository.UnitOfWork
            .SaveEntitiesAsync(cancellationToken);

        if (result)
        {
            var integrationEvent = new MaterialCreatedIntegrationEvent
            (
                created.Id,
                created.Name,
                MeasurementUnit.FromValue<MeasurementUnit>(created.MeasurementUnitId).Name,
                created.Price / created.MeasurementValue,
                created.UserName,
                created.Version
            );

            await _materialIntegrationEventService
                .PublishThroughEventBusAsync
                (
                    integrationEvent
                );

            return MaterialDetailDTO.MapFromMaterialEntity(created);
        }

        throw new Exception("Problem saving changes");
    }
}
