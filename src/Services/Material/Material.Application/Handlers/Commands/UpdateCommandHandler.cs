namespace Material.Application.Handlers.Commands;

using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using Exceptions;

using Material.Domain.Models.MaterialAggregate;

using MaterialIntegrationEventServices;

using MediatR;

using Models;

using Specifications;

using UserAccessors;

public class UpdateCommandHandler : IRequestHandler<UpdateCommand, MaterialDetailDTO>
{
    private readonly IMaterialRepository _materialRepository;
    private readonly IUserAccessor _userAccessor;
    private readonly IMaterialIntegrationEventService _materialIntegrationEventService;

    public UpdateCommandHandler(IMaterialRepository materialRepository, IUserAccessor userAccessor, IMaterialIntegrationEventService materialIntegrationEventService)
    {
        _materialRepository = materialRepository;
        _userAccessor = userAccessor;
        _materialIntegrationEventService = materialIntegrationEventService;
    }

    public async Task<MaterialDetailDTO> Handle(UpdateCommand request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();
        var material = await _materialRepository.FindMaterialAsync(new MaterialWithNameAndUsernameSpecification(request.Name, username));

        if (material == null)
        {
            throw new MaterialException(HttpStatusCode.NotFound, new { message = $"Material {request.Name} not found." });
        }

        var measurementUnit = MeasurementUnit.FromDisplayName<MeasurementUnit>(request.MeasurementUnit);
        material.MeasurementUnitId = measurementUnit.Id;
        material.MeasurementValue = request.MeasurementValue;
        material.Price = request.Price;

        var updated = _materialRepository.Update(material);

        var result = await _materialRepository.UnitOfWork
            .SaveEntitiesAsync(cancellationToken);

        if (result)
        {
            var integrationEvent = new MaterialUpdatedIntegrationEvent
            (
                updated.Id,
                updated.Name,
                MeasurementUnit.FromValue<MeasurementUnit>(updated.MeasurementUnitId).Name,
                updated.Price / updated.MeasurementValue,
                updated.UserName,
                updated.Version
            );

            await _materialIntegrationEventService
                .PublishThroughEventBusAsync
                (
                    integrationEvent
                );

            return MaterialDetailDTO.MapFromMaterialEntity(updated);
        }

        throw new Exception("Problem saving changes");
    }
}
