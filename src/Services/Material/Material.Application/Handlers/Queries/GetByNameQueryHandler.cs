namespace Material.Application.Handlers.Queries;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.MaterialAggregate;

using MediatR;

using Models;

using Specifications;

using UserAccessors;

public class GetByNameQueryHandler : IRequestHandler<GetByNameQuery, MaterialDetailDTO[]>
{
    private readonly IMaterialRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public GetByNameQueryHandler(IMaterialRepository repository, IUserAccessor userAccessor)
    {
        _repository = repository;
        _userAccessor = userAccessor;
    }

    public async Task<MaterialDetailDTO[]> Handle(GetByNameQuery request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var materials = await _repository.FindMaterialsAsync(new MaterialsLikeNameWithUsernameSpecification(request.Word, username));

        return materials.Select(x => MaterialDetailDTO.MapFromMaterialEntity(x)).ToArray();
    }
}
