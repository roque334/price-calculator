namespace Material.Application.Handlers.Queries;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.MaterialAggregate;

using MediatR;

using Models;

using Specifications;

using UserAccessors;

public class GetAllQueryHandler : IRequestHandler<GetAllQuery, GetAllQueryResponse>
{
    private readonly IMaterialRepository _repository;
    private readonly IUserAccessor _userAccessor;

    public GetAllQueryHandler(IMaterialRepository repository, IUserAccessor userAccessor)
    {
        _repository = repository;
        _userAccessor = userAccessor;
    }

    public async Task<GetAllQueryResponse> Handle(GetAllQuery request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var count = await _repository.CountMaterialsAsync(username);

        var materials = await _repository.FindMaterialsAsync(new MaterialsWithUsernameSpecification(username, request.PaginationParams.Skip, request.PaginationParams.Take));

        return new GetAllQueryResponse(
            materials.Select(x => MaterialDTO.MapFromMaterialEntity(x)).ToArray(),
            count
        );
    }
}
