namespace Material.Application.Handlers.Queries;

using MediatR;

using Models;

public class GetByNameQuery : IRequest<MaterialDetailDTO[]>
{
    public string Word { get; }
    public GetByNameQuery(string word) => Word = word;
}
