namespace Material.Application.Handlers.Queries;

using MediatR;

using Models;

public class GetAllQuery : IRequest<GetAllQueryResponse>
{
    public PaginationParams PaginationParams { get; }
    public GetAllQuery(PaginationParams paginationParams) => PaginationParams = paginationParams;
}
