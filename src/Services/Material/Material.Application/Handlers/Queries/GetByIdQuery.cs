namespace Material.Application.Handlers.Queries;

using MediatR;

using Models;

public class GetByIdQuery : IRequest<MaterialDetailDTO>
{
    public int Id { get; }

    public GetByIdQuery(int id) => Id = id;
}
