namespace Material.Application.Handlers.Queries;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.MaterialAggregate;

using Exceptions;

using MediatR;

using Models;

using Specifications;

using UserAccessors;

public class GetByIdQueryHandler : IRequestHandler<GetByIdQuery, MaterialDetailDTO>
{
    private readonly IMaterialRepository _repository;
    private readonly IUserAccessor _userAccessor;
    public GetByIdQueryHandler(IMaterialRepository repository, IUserAccessor userAccessor)
    {
        _repository = repository;
        _userAccessor = userAccessor;
    }

    public async Task<MaterialDetailDTO> Handle(GetByIdQuery request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var material = await _repository.FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(request.Id, username));

        if (material == null)
        {
            throw new MaterialException(HttpStatusCode.NotFound, new { message = "Material not found." });
        }

        return MaterialDetailDTO.MapFromMaterialEntity(material);
    }
}
