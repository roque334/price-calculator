namespace Material.Application.MaterialIntegrationEventServices;

using System.Threading.Tasks;

using Core.EventBus.Domain.Events;

public interface IMaterialIntegrationEventService
{
    Task PublishThroughEventBusAsync(IntegrationEvent evt);
}
