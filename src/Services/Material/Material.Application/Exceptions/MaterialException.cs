namespace Material.Application.Exceptions;

using System;
using System.Net;

public class MaterialException : Exception
{
    public readonly HttpStatusCode Code;
    public readonly object Error;

    public MaterialException(HttpStatusCode code, object error = null)
    {
        Code = code;
        Error = error;
    }
}
