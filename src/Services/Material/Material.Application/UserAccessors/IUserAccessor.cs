namespace Material.Application.UserAccessors;

public interface IUserAccessor
{
    string GetCurrentUsername();
    string GetCurrentToken();
}
