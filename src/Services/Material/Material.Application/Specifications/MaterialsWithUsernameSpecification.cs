namespace Material.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.MaterialAggregate;

public class MaterialsWithUsernameSpecification : BaseSpecification<MaterialEntity>
{
    public MaterialsWithUsernameSpecification(string username, int skip = 0, int take = 20)
        : base(m => m.UserName == username)
    {
        AddInclude(m => m.MeasurementUnit);
        Skip = skip;
        Take = take;
    }
}
