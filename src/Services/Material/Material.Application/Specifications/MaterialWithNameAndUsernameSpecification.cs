namespace Material.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.MaterialAggregate;

public class MaterialWithNameAndUsernameSpecification : BaseSpecification<MaterialEntity>
{
    public MaterialWithNameAndUsernameSpecification(string name, string username)
        : base(m => m.Name == name && m.UserName == username) =>
            AddInclude(m => m.MeasurementUnit);
}
