namespace Material.Application.Models;

using Domain.Models.MaterialAggregate;

public class MaterialDTO
{
    public int Id { get; }
    public string Name { get; }
    public decimal Price { get; }

    public MaterialDTO(int id, string name, decimal price)
    {
        Id = id;
        Name = name;
        Price = price;
    }

    public static MaterialDTO MapFromMaterialEntity(MaterialEntity material) =>
        new(material.Id, material.Name, material.Price);
}
