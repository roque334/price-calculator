namespace Material.Application.Models;

using Domain.Models.MaterialAggregate;

using MeasurementUnitEnum = Domain.Models.MaterialAggregate.MeasurementUnit;

public class MaterialDetailDTO
{
    public int Id { get; }
    public string Name { get; }
    public MeasurementUnitDTO MeasurementUnit { get; }
    public int MeasurementValue { get; }
    public decimal Price { get; }
    public string UserName { get; }
    public long Version { get; }

    public MaterialDetailDTO(int id, string name, int measurementUnitId, string measurementUnitName, int measurementValue, decimal price, string userName, long version)
    {
        Id = id;
        Name = name;
        MeasurementUnit = new MeasurementUnitDTO(measurementUnitId, measurementUnitName);
        MeasurementValue = measurementValue;
        Price = price;
        UserName = userName;
        Version = version;
    }

    public static MaterialDetailDTO MapFromMaterialEntity(MaterialEntity material)
    {
        var measurementUnit = MeasurementUnitEnum.FromValue<MeasurementUnit>(material.MeasurementUnitId);
        return new MaterialDetailDTO(
            material.Id,
            material.Name,
            measurementUnit.Id,
            measurementUnit.Name,
            material.MeasurementValue,
            material.Price,
            material.UserName,
            material.Version
        );
    }
}
