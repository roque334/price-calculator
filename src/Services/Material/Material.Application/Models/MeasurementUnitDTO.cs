namespace Material.Application.Models;

public class MeasurementUnitDTO
{
    public int Id { get; }
    public string Name { get; }

    public MeasurementUnitDTO(int id, string name)
    {
        Id = id;
        Name = name;
    }
}
