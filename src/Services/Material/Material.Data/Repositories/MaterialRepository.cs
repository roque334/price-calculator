namespace Material.Data.Repositories;

using System;
using System.Linq;
using System.Threading.Tasks;

using Core.Domain.Models;

using Data.Context;
using Data.Specifications;

using Domain.Models.MaterialAggregate;

using Microsoft.EntityFrameworkCore;

public class MaterialRepository : IMaterialRepository
{
    private readonly MaterialContext _context;

    public IUnitOfWork UnitOfWork => _context;

    public MaterialRepository(MaterialContext context) =>
        _context = context ?? throw new ArgumentNullException(nameof(context));

    public MaterialEntity Add(MaterialEntity material)
    {
        var added = _context
            .Materials
            .Add(material).Entity;
        _context
            .Entry(added)
            .Reference(m => m.MeasurementUnit)
            .Load();

        return added;
    }

    public MaterialEntity Update(MaterialEntity material)
    {
        material.IncreaseVersion();
        var updated = _context
            .Materials
            .Update(material).Entity;
        _context.Entry(updated).Reference(m => m.MeasurementUnit).Load();

        return updated;
    }

    public MaterialEntity Delete(MaterialEntity material)
    {
        var deleted = _context
            .Materials
            .Remove(material).Entity;

        return deleted;
    }

    public async Task<MaterialEntity> FindMaterialAsync(ISpecification<MaterialEntity> spec)
    {
        var query = ApplySpecification(spec);
        var material = await query.FirstOrDefaultAsync();

        if (material != null)
        {
            // Forces to get the entity from the db
            await _context.Entry<MaterialEntity>(material).ReloadAsync();
        }

        return material;
    }

    public async Task<MaterialEntity[]> FindMaterialsAsync(ISpecification<MaterialEntity> spec)
    {
        var query = ApplySpecification(spec);
        var materials = await query.ToArrayAsync();

        foreach (var material in materials)
        {
            // Forces to get the entity from the db
            await _context.Entry<MaterialEntity>(material).ReloadAsync();
        }

        return materials;
    }

    public async Task<int> CountMaterialsAsync(string username) =>
        await _context.Materials.CountAsync((x) => x.UserName == username);

    private IQueryable<MaterialEntity> ApplySpecification(ISpecification<MaterialEntity> spec) =>
        SpecificationEvaluator<MaterialEntity>.GetQuery(_context.Materials.AsQueryable(), spec);
}
