namespace Material.Data.Specifications;

using System.Linq;

using Core.Domain.Models;

using Microsoft.EntityFrameworkCore;

public static class SpecificationEvaluator<T> where T : Entity
{
    public static IQueryable<T> GetQuery(IQueryable<T> inputQuery, ISpecification<T> specification)
    {
        var query = inputQuery;

        // modify the IQueryable using the specification's criteria expression
        if (specification.Criteria != null)
        {
            query = query.Where(specification.Criteria);
        }

        // Includes all expression-based includes
        query = specification.Includes.Aggregate(query,
                                (current, include) => current.Include(include));

        // Pagination skip and take
        if (specification.Skip >= 0 && specification.Take >= 0)
        {
            query = query.Skip(specification.Skip).Take(specification.Take);
        }

        return query;
    }
}
