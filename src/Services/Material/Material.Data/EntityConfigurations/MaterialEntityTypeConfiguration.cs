namespace Material.Data.EntityConfigurations;

using Domain.Models.MaterialAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

public class MaterialEntityTypeConfiguration : IEntityTypeConfiguration<MaterialEntity>
{
    public void Configure(EntityTypeBuilder<MaterialEntity> builder)
    {
        builder.ToTable("materials");

        builder.HasKey(o => o.Id);
        builder.Property(o => o.Id)
            .IsRequired();

        builder.Property(o => o.Name)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property(o => o.MeasurementUnitId);

        builder.Property(o => o.MeasurementValue)
            .IsRequired();

        builder.Property(o => o.Price)
            .HasPrecision(10, 2)
            .IsRequired();

        builder.Property(o => o.UserName)
            .HasMaxLength(256)
            .IsRequired();

        builder.Property(o => o.Version)
            .HasDefaultValue(0)
            .IsConcurrencyToken(true);

        builder.HasIndex(o => new { o.UserName, o.Name }).IsUnique();

        builder.HasOne(o => o.MeasurementUnit)
            .WithMany(b => b.MaterialEntities)
            .HasForeignKey(o => o.MeasurementUnitId);
    }
}
