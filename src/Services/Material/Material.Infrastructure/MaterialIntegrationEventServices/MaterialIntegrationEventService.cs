using System;
using System.Threading.Tasks;
using Core.EventBus.Domain.Bus;
using Core.EventBus.Domain.Events;
using Material.Application.MaterialIntegrationEventServices;
using Microsoft.Extensions.Logging;

namespace Material.Infrastructure.MaterialIntegrationEventServices
{
    public class MaterialIntegrationEventService : IMaterialIntegrationEventService
    {

        private readonly ILogger<MaterialIntegrationEventService> _logger;
        private readonly IEventBus _eventBus;

        public MaterialIntegrationEventService(IEventBus eventBus, ILogger<MaterialIntegrationEventService> logger)
        {
            _eventBus = eventBus;
            _logger = logger;
        }
        
        public Task PublishThroughEventBusAsync(IntegrationEvent evt)
        {
            try
            {
                _logger.LogInformation($"----- Publishing event: {evt.Id} from {this.GetType().Assembly.GetName()} - {evt}");
                _eventBus.Publish(evt);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"ERROR Publishing event: {evt.Id} from {this.GetType().Assembly.GetName()} - {evt}");
            }

            return Task.CompletedTask;
        }
    }
}