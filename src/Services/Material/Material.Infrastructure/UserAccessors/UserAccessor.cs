using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using Material.Application.UserAccessors;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

namespace Material.Infrastructure.UserAccessors
{
    public class UserAccessor : IUserAccessor
    {
        private IHttpContextAccessor _httpContextAccessor;

        public UserAccessor(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string GetCurrentToken()
        {
            var authorization = _httpContextAccessor
                .HttpContext
                .Request
                .Headers[HeaderNames.Authorization];

            if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
            {
                return headerValue.Parameter;
            }

            return string.Empty;
        }

        public string GetCurrentUsername()
        {
            return _httpContextAccessor
                .HttpContext
                .User?
                .Claims?
                .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?
                .Value;
        }
    }
}