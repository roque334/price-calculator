namespace Material.API.Controllers;

using MediatR;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;

[ApiController]
[Route("api/[controller]")]
public class ControllerBaseCustom : ControllerBase
{
    protected IMediator Mediator { get; }
    public ControllerBaseCustom(IMediator mediator) =>
        Mediator = mediator ?? (HttpContext.RequestServices.GetService<IMediator>());

}
