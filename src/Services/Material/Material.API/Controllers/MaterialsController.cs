namespace Material.API.Controllers;

using System.Net;
using System.Threading.Tasks;

using Application.Handlers.Commands;
using Application.Handlers.Queries;
using Application.Models;

using MediatR;

using Microsoft.AspNetCore.Mvc;

public class MaterialsController : ControllerBaseCustom
{
    public MaterialsController(IMediator mediator) : base(mediator)
    {
    }

    [HttpGet]
    [ProducesResponseType(typeof(MaterialDTO[]), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<GetAllQueryResponse>> GetAll([FromQuery] int skip, [FromQuery] int take) =>
        await Mediator.Send(new GetAllQuery(new PaginationParams(skip, take)));

    [HttpGet("{id:int}")]
    [ProducesResponseType(typeof(MaterialDetailDTO), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<MaterialDetailDTO>> GetById([FromRoute] int id) =>
        await Mediator.Send(new GetByIdQuery(id));

    [HttpGet("search")]
    [ProducesResponseType(typeof(MaterialDTO[]), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<MaterialDetailDTO[]>> GetByName([FromQuery] string name) =>
        await Mediator.Send(new GetByNameQuery(name));

    [HttpPost]
    [ProducesResponseType(typeof(MaterialDetailDTO), (int)HttpStatusCode.Created)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<MaterialDetailDTO>> Create([FromBody] CreateCommand request)
    {
        var material = await Mediator.Send(request);

        return CreatedAtAction(nameof(GetById), new { material.Id }, material);
    }

    [HttpPut]
    [ProducesResponseType(typeof(MaterialDetailDTO), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<MaterialDetailDTO>> Update([FromBody] UpdateCommand request) =>
        await Mediator.Send(request);

    [HttpDelete("{id:int}")]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    public async Task<ActionResult<Unit>> Delete([FromRoute] int id) =>
        await Mediator.Send(new DeleteCommand(id));
}
