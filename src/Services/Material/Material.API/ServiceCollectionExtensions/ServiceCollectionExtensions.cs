namespace Material.API.ServiceCollectionExtensions;

using System;
using System.Text;
using System.Text.Json.Serialization;

using Application.MaterialIntegrationEventServices;
using Application.UserAccessors;

using Core.EventBus.Domain.Bus;
using Core.EventBus.Infrastructure.Bus;
using Core.EventBus.Infrastructure.Bus.PersistentConnections;
using Core.EventBus.Infrastructure.Bus.SubscriptionsManagers;

using Data.Context;
using Data.Repositories;

using Domain.Models.MaterialAggregate;

using Filters;

using FluentValidation.AspNetCore;

using Infrastructure.MaterialIntegrationEventServices;
using Infrastructure.UserAccessors;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

using RabbitMQ.Client;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDbContext(this IServiceCollection services)
    {
        var connectionString = Environment.GetEnvironmentVariable("MSSQL_CONNECTIONSTRING") ?? "defaultConnectionString";
        services.AddDbContext<MaterialContext>(options =>
        {
            options.UseSqlServer(connectionString);
        });

        return services;
    }

    public static IServiceCollection AddCustomControllers(this IServiceCollection services)
    {
        var mvcBuilder = services
            .AddControllers(options =>
            {
                var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
                options.Filters.Add(new AuthorizeFilter(policy));
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
            .AddJsonOptions(options =>
                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.Preserve
            );

        mvcBuilder.AddFluentValidation(configuration =>
        {
            configuration.RegisterValidatorsFromAssemblyContaining<Application.Program>();
        });

        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy",
                builder => builder
                .SetIsOriginAllowed((host) => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
        });

        return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "Material.API", Version = "v1" });
        });

        return services;
    }

    public static IServiceCollection AddCustomServices(this IServiceCollection services)
    {
        services.AddMediatR(typeof(Application.Program).Assembly);
        services.AddAutoMapper(typeof(Application.Program).Assembly);
        services.AddScoped<IMaterialRepository, MaterialRepository>();
        services.AddScoped<IUserAccessor, UserAccessor>();

        return services;
    }

    public static IServiceCollection AddAuthenticationBuilder(this IServiceCollection services)
    {
        var jwtKey = Environment.GetEnvironmentVariable("JWT_KEY") ?? "asdfasdfasdf";
        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
        var authenticationBuilder = services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);
        authenticationBuilder.AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,
                ValidateAudience = false,
                ValidateIssuer = false,
            };
        });

        return services;
    }

    public static IServiceCollection AddIntegrationServices(this IServiceCollection services)
    {
        services.AddTransient<IMaterialIntegrationEventService, MaterialIntegrationEventService>();
        services.AddSingleton<IRabbitMQPersistentConnection>(sp =>
        {
            var logger = sp.GetRequiredService<ILogger<DefaultRabbitMQPersistentConnection>>();
            var connectionFactory = new ConnectionFactory
            {
                HostName = Environment.GetEnvironmentVariable("EVENT_BUS_HOSTNAME"),
                DispatchConsumersAsync = true
            };

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("EVENT_BUS_USERNAME")))
            {
                connectionFactory.UserName = Environment.GetEnvironmentVariable("EVENT_BUS_USERNAME");
            }

            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("EVENT_BUS_PASSWORD")))
            {
                connectionFactory.Password = Environment.GetEnvironmentVariable("EVENT_BUS_PASSWORD");
            }

            var retryCount = 5;
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("EVENT_BUS_RETRYCOUNT")))
            {
                retryCount = int.Parse(Environment.GetEnvironmentVariable("EVENT_BUS_RETRYCOUNT"));
            }
            return new DefaultRabbitMQPersistentConnection(connectionFactory, logger, retryCount);
        });
        return services;
    }

    public static IServiceCollection AddEventBus(this IServiceCollection services)
    {
        var subscriptionClientName = Environment.GetEnvironmentVariable("EVENT_BUS_SUBSCRIPTION_CLIENTNAME");
        services.AddSingleton<IEventBus>(sp =>
        {
            var rabbitMQPersistentConnection = sp.GetRequiredService<IRabbitMQPersistentConnection>();
            var serviceScopeFactory = sp.GetRequiredService<IServiceScopeFactory>();
            var logger = sp.GetRequiredService<ILogger<EventBusRabbitMQ>>();
            var eventBusSubscriptionsManager = sp.GetRequiredService<IEventBusSubscriptionsManager>();
            var retryCount = 5;
            if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("EVENT_BUS_RETRYCOUNT")))
            {
                retryCount = int.Parse(Environment.GetEnvironmentVariable("EVENT_BUS_RETRYCOUNT"));
            }

            return new EventBusRabbitMQ(rabbitMQPersistentConnection, logger, eventBusSubscriptionsManager, serviceScopeFactory, subscriptionClientName, retryCount);
        });

        services.AddSingleton<IEventBusSubscriptionsManager, InMemoryEventBusSubscriptionsManager>();
        return services;
    }
}
