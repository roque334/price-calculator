namespace Material.API.Filters;

using System;
using System.Net;

using Application.Exceptions;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

public class HttpGlobalExceptionFilter : IExceptionFilter
{
    private readonly IWebHostEnvironment _env;
    private readonly ILogger<HttpGlobalExceptionFilter> _logger;

    public HttpGlobalExceptionFilter(IWebHostEnvironment env, ILogger<HttpGlobalExceptionFilter> logger)
    {
        _env = env;
        _logger = logger;
    }

    public void OnException(ExceptionContext context)
    {
        _logger.LogError(new EventId(context.Exception.HResult),
            context.Exception,
            "{exceptionMessage}", context.Exception.Message);

        switch (context.Exception)
        {
            case MaterialException me:
                context.Result = new BadRequestObjectResult(new { Errors = me.Error });
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                break;
            case Exception e:
                var developmentMessage = string.IsNullOrWhiteSpace(e.Message) ? "An error ocurred." : e.Message;
                var message = _env.IsDevelopment() ? developmentMessage : "An error ocurred.";

                context.Result = new InternalServerErrorObjectResult(new { Errors = message });
                context.HttpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                break;
        }
    }
}
