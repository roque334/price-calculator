namespace Material.Domain.Models.MaterialAggregate;

using System.Collections.Generic;

using Core.Domain.Models;

public class MeasurementUnit : Enumeration
{
    public static readonly MeasurementUnit Units = new(1, "Units");
    public static readonly MeasurementUnit Grams = new(2, "Grams");
    public static readonly MeasurementUnit Milliliters = new(3, "Milliliters");

    public ICollection<MaterialEntity> MaterialEntities { get; set; }

    protected MeasurementUnit(int id, string name) : base(id, name)
    {
    }
}
