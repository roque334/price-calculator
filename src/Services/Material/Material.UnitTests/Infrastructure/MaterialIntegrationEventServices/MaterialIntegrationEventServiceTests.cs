using System;
using System.Threading.Tasks;
using Common.Events.Models;
using Core.EventBus.Domain.Bus;
using Core.EventBus.Domain.Events;
using Material.Infrastructure.MaterialIntegrationEventServices;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using Shouldly;

namespace Material.UnitTests.Infrastructure.MaterialIntegrationEventServices
{
    
    [TestFixture]
    public class MaterialIntegrationEventServiceTests
    {
        private Mock<IEventBus> _eventBusMock;
        private ILogger<MaterialIntegrationEventService> _logger;
        
        [SetUp]
        public void SetUp()
        {
            _logger = new LoggerFactory().CreateLogger<MaterialIntegrationEventService>();
            _eventBusMock = new Mock<IEventBus>();
            _eventBusMock.Setup(o => o.Publish(It.IsAny<IntegrationEvent>())).Verifiable();
        }

        [Test]
        public async Task PublishThroughEventBusAsync_MaterialCreatedIntegrationEvent_Success()
        {
            // Arrange
            var materialIntegrationEventService = new MaterialIntegrationEventService(_eventBusMock.Object, _logger);
            var integrationEvent = new MaterialCreatedIntegrationEvent(1,"Paper", "Units", 0.25M, "John", 0);

            // Act
            await materialIntegrationEventService.PublishThroughEventBusAsync(integrationEvent);
        }

        [Test]
        public async Task PublishThroughEventBusAsync_MaterialCreatedIntegrationEvent_Fail()
        {
            // Arrange
            _eventBusMock.Setup(o => o.Publish(It.IsAny<IntegrationEvent>())).Throws<Exception>();
            var materialIntegrationEventService = new MaterialIntegrationEventService(_eventBusMock.Object, _logger);
            var integrationEvent = new MaterialCreatedIntegrationEvent(1,"Paper", "Units", 0.25M, "John", 0);

            // Act + Assert
            await Should.ThrowAsync<Exception>(() => materialIntegrationEventService.PublishThroughEventBusAsync(integrationEvent));
        }
    }
}