namespace Material.UnitTests.Application.Specifications;

using System.Linq;

using Material.Application.Specifications;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class MaterialsLikeNameWithUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_MaterialsExists_Success()
    {
        // Act
        var result = materials.Where(new MaterialsLikeNameWithUsernameSpecification("p", "John").Criteria.Compile()).ToArray();

        // Assert
        result.ShouldNotBeNull();
        result.Length.ShouldBe(2);
        result[0].Name.ShouldBe("Envelope");
        result[1].Name.ShouldBe("Stamp");
    }
}
