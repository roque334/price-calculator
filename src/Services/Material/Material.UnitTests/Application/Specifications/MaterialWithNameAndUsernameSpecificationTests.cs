namespace Material.UnitTests.Application.Specifications;

using System.Linq;

using Material.Application.Specifications;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class MaterialWithNameAndUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_MaterialExists_Success()
    {
        // Act
        var material = materials.FirstOrDefault(new MaterialWithNameAndUsernameSpecification("Envelope", "John").Criteria.Compile());

        // Assert
        material.ShouldSatisfyAllConditions(
            () => material.ShouldNotBeNull(),
            () => material.Name.ShouldBe("Envelope"),
            () => material.MeasurementUnitId.ShouldBe(1),
            () => material.MeasurementValue.ShouldBe(50),
            () => material.Price.ShouldBe(6.99M),
            () => material.UserName.ShouldBe("John"),
            () => material.Version.ShouldBe(0));
    }
}
