namespace Material.UnitTests.Application.Validators;

using NUnit.Framework;

using FluentValidation.TestHelper;

using Material.Application.Handlers.Commands;
using Material.Application.Validators;
using AutoFixture;

[TestFixture]
public class CreateCommandValidatorTests
{
    private readonly Fixture _fixture = new();
    private CreateCommandValidator _validator;

    [SetUp]
    public void SetUp()
    {
        _validator = new CreateCommandValidator();
    }

    [Test]
    public void CreateCommandValidator_ValidCreateCommand_Success()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithEmptyName_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.Name = string.Empty;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithNullName_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.Name = null;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithEmptyMeasurementUnit_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.MeasurementUnit = string.Empty;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithNullMeasurementUnit_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.MeasurementUnit = null;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithZeroMeasurementValue_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.MeasurementValue = 0;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithNegativeMeasurementValue_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.MeasurementValue = -1;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldNotHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithZeroPrice_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.Price = 0.0M;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldHaveValidationErrorFor(c => c.Price);
    }

    [Test]
    public void CreateCommandValidator_CreateCommandWithNegativePrice_Fail()
    {
        // Arrange
        var createCommand = _fixture.Create<CreateCommand>();
        createCommand.Price = -1.0M;

        // Act
        var result = _validator.TestValidate(createCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Name);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementUnit);
        result.ShouldNotHaveValidationErrorFor(c => c.MeasurementValue);
        result.ShouldHaveValidationErrorFor(c => c.Price);
    }
}
