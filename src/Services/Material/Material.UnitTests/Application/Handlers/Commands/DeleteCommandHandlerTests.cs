namespace Material.UnitTests.Application.Handlers.Commands;

using System;
using System.Threading.Tasks;

using AutoFixture;

using Common.Events.Models;

using Core.Domain.Models;

using Material.Application.Exceptions;
using Material.Application.Handlers.Commands;
using Material.Application.MaterialIntegrationEventServices;
using Material.Application.Specifications;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;

using MediatR;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class DeleteCommandHandlerTests
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Loose);
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Mock<IMaterialIntegrationEventService> _materialIntegrationEventServiceMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private DeleteCommandHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new DeleteCommandHandler(_materialRepositoryMock.Object, _userAccessorMock.Object, _materialIntegrationEventServiceMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var deleteCommand = new DeleteCommand(materialEntity.Id);

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        _materialRepositoryMock.Setup(o => o.Delete(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        _materialIntegrationEventServiceMock.Setup(o => o.PublishThroughEventBusAsync(It.Is<MaterialDeletedIntegrationEvent>(x => x.MaterialId == materialEntity.Id))).Returns(Task.CompletedTask);

        // Act
        var result = await _sut.Handle(deleteCommand, default);

        // Assert
        result.ShouldBe(Unit.Value);

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialDoesNotExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var deleteCommand = new DeleteCommand(materialEntity.Id);

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => _sut.Handle(deleteCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialProblemSavingChanges_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var deleteCommand = new DeleteCommand(materialEntity.Id);

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        _materialRepositoryMock.Setup(o => o.Delete(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(deleteCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }
}
