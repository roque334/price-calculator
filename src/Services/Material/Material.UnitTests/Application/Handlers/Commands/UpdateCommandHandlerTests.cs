namespace Material.UnitTests.Application.Handlers.Commands;

using System;
using System.Threading;
using System.Threading.Tasks;

using AutoFixture;

using Common.Events.Models;

using Core.Domain.Models;

using Material.Application.Exceptions;
using Material.Application.Handlers.Commands;
using Material.Application.MaterialIntegrationEventServices;
using Material.Application.Specifications;
using Material.Application.UserAccessors;
using Material.Domain.Models.MaterialAggregate;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class UpdateCommandHandlerTests
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Loose);
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<IUserAccessor> _userAccessorMock = new(MockBehavior.Strict);
    private readonly Mock<IMaterialIntegrationEventService> _materialIntegrationEventServiceMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private UpdateCommandHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _fixture.Customize<MaterialEntity>(composer => composer
            .With(e => e.MeasurementUnitId, MeasurementUnit.Units.Id)
            .Without(e => e.MeasurementUnit));

        _sut = new UpdateCommandHandler(_materialRepositoryMock.Object, _userAccessorMock.Object, _materialIntegrationEventServiceMock.Object);
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var updateCommand = new UpdateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        _materialRepositoryMock.Setup(x => x.Update(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        _materialIntegrationEventServiceMock.Setup(o => o.PublishThroughEventBusAsync(It.Is<MaterialUpdatedIntegrationEvent>(x => x.MaterialId == materialEntity.Id))).Returns(Task.CompletedTask);

        // Act
        var updated = await _sut.Handle(updateCommand, default);

        // Assert
        // Assert
        updated.ShouldSatisfyAllConditions(
            () => updated.Id.ShouldBe(materialEntity.Id),
            () => updated.Name.ShouldBe(materialEntity.Name),
            () => updated.MeasurementUnit.Id.ShouldBe(MeasurementUnit.Units.Id),
            () => updated.MeasurementUnit.Name.ShouldBe(MeasurementUnit.Units.Name),
            () => updated.MeasurementValue.ShouldBe(materialEntity.MeasurementValue),
            () => updated.Price.ShouldBe(materialEntity.Price)
        );

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialDoesNotExists_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var updateCommand = new UpdateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        // Act + Assert
        await Should.ThrowAsync<MaterialException>(() => _sut.Handle(updateCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialProblemSavingChanges_Fail()
    {
        // Arrange
        var username = _fixture.Create<string>();
        var materialEntity = _fixture.Create<MaterialEntity>();
        var updateCommand = new UpdateCommand
        {
            Name = materialEntity.Name,
            MeasurementUnit = MeasurementUnit.Units.Name,
            MeasurementValue = materialEntity.MeasurementValue,
            Price = materialEntity.Price
        };

        _userAccessorMock.Setup(x => x.GetCurrentUsername()).Returns(username);

        _materialRepositoryMock.Setup(x => x.FindMaterialAsync(It.IsAny<MaterialWithNameAndUsernameSpecification>())).ReturnsAsync(materialEntity);

        _materialRepositoryMock.Setup(x => x.Update(materialEntity)).Returns(materialEntity);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(updateCommand, default));

        _userAccessorMock.VerifyAll();
        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
        _materialIntegrationEventServiceMock.VerifyAll();
    }
}
