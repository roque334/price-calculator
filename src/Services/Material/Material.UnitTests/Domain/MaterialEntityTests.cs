namespace Material.UnitTests.Domain;

using NUnit.Framework;
using Shouldly;

using Material.Domain.Models.MaterialAggregate;

[TestFixture]
public class MaterialEntityTests
{
    [Test]
    public void Material_IncreaseVersion_Success()
    {
        // Arrange
        var material = new MaterialEntity("Paper", 0, 50, 2.50M);

        // Act
        material.IncreaseVersion();

        // Assert
        material.ShouldNotBeNull();
        material.Name.ShouldBe("Paper");
        material.MeasurementUnitId.ShouldBe(0);
        material.MeasurementValue.ShouldBe(50);
        material.Price.ShouldBe(2.50M);
        material.UserName.ShouldBe(default);
        material.Version.ShouldBe(1);
    }
}
