namespace Identity.UnitTests.Infrastructure.JwtGenerators;

using System;

using AutoFixture;

using Identity.Domain.Models;
using Identity.Infrastructure.JwtGenerators;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class JwtGeneratorTests
{
    [SetUp]
    public void SetUp() => Environment.SetEnvironmentVariable("JWT_KEY", "ThisIsTheSecretJwt");

    [TearDown]
    public void TearDown() => Environment.SetEnvironmentVariable("JWT_KEY", null);

    [Test]
    public void CreateToken_ApplicationUser_Success()
    {
        // Arrange
        var fixture = new Fixture();
        var user = fixture.Create<ApplicationUser>();

        var jwtGenerator = new JwtGenerator();

        // Act
        var token = jwtGenerator.CreateToken(user);

        // Assert
        token.ShouldNotBeNull();
    }
}
