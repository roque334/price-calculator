namespace Identity.UnitTests.Application.Mappers;

using AutoFixture;

using AutoMapper;

using Identity.Application.Handlers.Commands;
using Identity.Application.Mappers;
using Identity.Application.Model;
using Identity.Domain.Models;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class MappingProfileTests
{
    private readonly Fixture _fixture = new();

    private IMapper _mapper;

    [SetUp]
    public void SetUp()
    {
        var config = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>());
        _mapper = config.CreateMapper();
    }

    [Test]
    public void Map_ApplicationUserToUser_Success()
    {
        // Arrange
        var applicationUser = _fixture.Create<ApplicationUser>();

        // Act
        var user = _mapper.Map<ApplicationUser, User>(applicationUser);

        // Assert
        user.ShouldSatisfyAllConditions(
            () => user.FirstName.ShouldBe(applicationUser.FirstName),
            () => user.LastName.ShouldBe(applicationUser.LastName),
            () => user.Token.ShouldBe(default));
    }

    [Test]
    public void Map_RegisterCommand_To_ApplicationUser_Success()
    {
        // Arrange
        var registerCommand = _fixture.Create<RegisterCommand>();

        // Act
        var applicationUser = _mapper.Map<RegisterCommand, ApplicationUser>(registerCommand);

        // Assert
        applicationUser.ShouldSatisfyAllConditions(
            () => applicationUser.FirstName.ShouldBe(registerCommand.FirstName),
            () => applicationUser.LastName.ShouldBe(registerCommand.LastName),
            () => applicationUser.UserName.ShouldBe(registerCommand.Username),
            () => applicationUser.Email.ShouldBe(registerCommand.Email));
    }
}
