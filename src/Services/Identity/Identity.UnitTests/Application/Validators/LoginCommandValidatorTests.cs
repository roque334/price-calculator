namespace Identity.UnitTests.Application.Validators;

using NUnit.Framework;
using FluentValidation.TestHelper;

using Identity.Application.Handlers.Commands;
using Identity.Application.Validators;

[TestFixture]
public class LoginCommandValidatorTests
{
    private const string VALID_EMAIL = "john.doe@abc.com";
    private const string VALID_PASSWORD = "Pa$$word123";
    private LoginCommandValidator _validator;

    [SetUp]
    public void SetUp() => _validator = new LoginCommandValidator();

    [Test]
    public void LoginCommandValidator_ValidLoginCommand_Success()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = VALID_EMAIL,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void LoginCommandValidator_LoginCommandWithEmptyEmail_Fail()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = string.Empty,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void LoginCommandValidator_LoginCommandWithNullEmail_Fail()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = null,
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void LoginCommandValidator_LoginCommandWithIncorrectEmail_Fail()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = "john.doeabc.com",
            Password = VALID_PASSWORD
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldHaveValidationErrorFor(c => c.Email);
        result.ShouldNotHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void LoginCommandValidator_CreateCommandWithEmptyPassword_Fail()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = VALID_EMAIL,
            Password = string.Empty
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldHaveValidationErrorFor(c => c.Password);
    }

    [Test]
    public void LoginCommandValidator_CreateCommandWithNullPassword_Fail()
    {
        // Arrange
        var loginCommand = new LoginCommand
        {
            Email = VALID_EMAIL,
            Password = null
        };

        // Act
        var result = _validator.TestValidate(loginCommand);

        // Assert
        result.ShouldNotHaveValidationErrorFor(c => c.Email);
        result.ShouldHaveValidationErrorFor(c => c.Password);
    }
}
