namespace Identity.UnitTests.Application.Handlers.Commands;

using System;
using System.Threading.Tasks;

using AutoFixture;

using AutoMapper;

using Identity.Application.Exceptions;
using Identity.Application.Handlers.Commands;
using Identity.Application.JwtGenerators;
using Identity.Application.Model;

using Identity.Domain.Models;

using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using Moq;

using NUnit.Framework;

using Shouldly;

[TestFixture]
public class RegisterCommandHandlerTests
{
    private readonly Mock<IJwtGenerator> _jwtGeneratorMock = new(MockBehavior.Strict);
    private readonly Mock<IMapper> _mapperMock = new(MockBehavior.Strict);
    private readonly Fixture _fixture = new();
    private Mock<UserManager<ApplicationUser>> _userManagerMock;
    private RegisterCommandHandler _sut;

    [SetUp]
    public void SetUp()
    {
        _userManagerMock = new(behavior: MockBehavior.Strict,
            new Mock<IUserStore<ApplicationUser>>(MockBehavior.Loose).Object,
            new Mock<IOptions<IdentityOptions>>(MockBehavior.Loose).Object,
            new Mock<IPasswordHasher<ApplicationUser>>(MockBehavior.Loose).Object,
            Array.Empty<IUserValidator<ApplicationUser>>(),
            Array.Empty<IPasswordValidator<ApplicationUser>>(),
            new Mock<ILookupNormalizer>(MockBehavior.Loose).Object,
            new Mock<IdentityErrorDescriber>(MockBehavior.Loose).Object,
            new Mock<IServiceProvider>(MockBehavior.Loose).Object,
            new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _userManagerMock.SetupProperty(x => x.Logger, new Mock<ILogger<UserManager<ApplicationUser>>>(MockBehavior.Loose).Object);

        _sut = new RegisterCommandHandler(_userManagerMock.Object, _jwtGeneratorMock.Object, _mapperMock.Object);
    }

    [Test]
    public async Task Handler_HappyFlow_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var registerCommand = _fixture.Create<RegisterCommand>();
        var applicationUser = new ApplicationUser
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            UserName = registerCommand.Username,
            Email = registerCommand.Email,
        };
        var user = new User
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            Token = token
        };

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(registerCommand.Email))
            .ReturnsAsync((ApplicationUser)null);
        _userManagerMock
            .Setup(x => x.FindByNameAsync(registerCommand.Username))
            .ReturnsAsync((ApplicationUser)null);
        _userManagerMock
            .Setup(x => x.CreateAsync(applicationUser, registerCommand.Password))
            .ReturnsAsync(IdentityResult.Success);

        _mapperMock
            .Setup(x => x.Map<RegisterCommand, ApplicationUser>(registerCommand))
            .Returns(applicationUser);
        _mapperMock
            .Setup(x => x.Map<ApplicationUser, User>(applicationUser))
            .Returns(user);

        _jwtGeneratorMock
            .Setup(x => x.CreateToken(applicationUser))
            .Returns(token);

        // Act
        var result = await _sut.Handle(registerCommand, default);

        // Assert
        result.ShouldSatisfyAllConditions(
            () => result.FirstName.ShouldBe(user.FirstName),
            () => result.LastName.ShouldBe(user.LastName),
            () => result.Token.ShouldBe(token));

        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }

    [Test]
    public async Task Handler_EmailAlreadyExists_Success()
    {
        // Arrange
        var registerCommand = _fixture.Create<RegisterCommand>();
        var applicationUser = new ApplicationUser
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            UserName = registerCommand.Username,
            Email = registerCommand.Email,
        };

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(registerCommand.Email))
            .ReturnsAsync(applicationUser);

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(registerCommand, default));

        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }

    [Test]
    public async Task Handler_UsernameAlreadyExists_Success()
    {
        // Arrange
        var registerCommand = _fixture.Create<RegisterCommand>();
        var applicationUser = new ApplicationUser
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            UserName = registerCommand.Username,
            Email = registerCommand.Email,
        };

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(registerCommand.Email))
            .ReturnsAsync((ApplicationUser)null);
        _userManagerMock
            .Setup(x => x.FindByNameAsync(registerCommand.Username))
            .ReturnsAsync(applicationUser);

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => _sut.Handle(registerCommand, default));

        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }

    [Test]
    public async Task Handler_CreationFails_Success()
    {
        // Arrange
        var token = _fixture.Create<string>();
        var registerCommand = _fixture.Create<RegisterCommand>();
        var applicationUser = new ApplicationUser
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            UserName = registerCommand.Username,
            Email = registerCommand.Email,
        };
        var user = new User
        {
            FirstName = registerCommand.FirstName,
            LastName = registerCommand.LastName,
            Token = token
        };

        _userManagerMock
            .Setup(x => x.FindByEmailAsync(registerCommand.Email))
            .ReturnsAsync((ApplicationUser)null);
        _userManagerMock
            .Setup(x => x.FindByNameAsync(registerCommand.Username))
            .ReturnsAsync((ApplicationUser)null);
        _userManagerMock
            .Setup(x => x.CreateAsync(applicationUser, registerCommand.Password))
            .ReturnsAsync(IdentityResult.Failed());

        _mapperMock
            .Setup(x => x.Map<RegisterCommand, ApplicationUser>(registerCommand))
            .Returns(applicationUser);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(registerCommand, default));

        _userManagerMock.VerifyAll();
        _mapperMock.VerifyAll();
        _jwtGeneratorMock.VerifyAll();
    }
}
