namespace Identity.API;

using System;

using Data.Context;

using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using Polly;
using Polly.Retry;

public class Program
{
    public static void Main(string[] args)
    {
        var host = CreateWebHostBuilder(args).Build();

        RecreateDb(host);

        host.Run();
    }

    private static void RecreateDb(IWebHost host)
    {
        using var scope = host.Services.CreateScope();
        var services = scope.ServiceProvider;
        var logger = services.GetRequiredService<ILogger<Program>>();

        try
        {
            var context = services.GetRequiredService<ApplicationDbContext>();

            var policy = RetryPolicy
            .Handle<SqlException>()
            .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
            {
                logger.LogWarning(ex, "An error occurred creating the DB.");
            });

            policy.Execute(() =>
            {
                context.Database.MigrateAsync().ConfigureAwait(false).GetAwaiter().GetResult();
                logger.LogInformation("Migration done.");
            });
        }
        catch (Exception ex)
        {
            logger.LogError(ex, "An error occurred creating the DB.");
            throw;
        }
    }

    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>();
}
