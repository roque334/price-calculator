namespace Identity.API.Filters;

using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

internal class InternalServerErrorObjectResult : ObjectResult
{
    public InternalServerErrorObjectResult(object value) : base(value)
    {
        StatusCode = StatusCodes.Status500InternalServerError;
    }
}
