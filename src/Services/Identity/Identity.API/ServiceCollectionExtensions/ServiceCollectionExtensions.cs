namespace Identity.API.ServiceCollectionExtensions;

using System;
using System.Text;

using API.Filters;

using Application.JwtGenerators;
using Application.UserAccessors;

using Data.Context;

using Domain.Models;

using Infrastructure.JwtGenerators;
using Infrastructure.UserAccessors;

using FluentValidation.AspNetCore;

using MediatR;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDbContext(this IServiceCollection services)
    {
        var connectionString = Environment.GetEnvironmentVariable("MSSQL_CONNECTIONSTRING") ?? "defaultConnectionString";
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseSqlServer(connectionString);
        });

        return services;
    }

    public static IServiceCollection AddCustomControllers(this IServiceCollection services)
    {
        var mvcBuilder = services.AddControllers(options =>
        {
            var policy = new AuthorizationPolicyBuilder().RequireAuthenticatedUser().Build();
            options.Filters.Add(new AuthorizeFilter(policy));
            options.Filters.Add(typeof(HttpGlobalExceptionFilter));
        });
        mvcBuilder.AddFluentValidation(configuration =>
        {
            configuration.RegisterValidatorsFromAssemblyContaining<Identity.Application.Program>();
        });

        services.AddCors(options =>
        {
            options.AddPolicy("CorsPolicy",
                builder => builder
                .SetIsOriginAllowed((host) => true)
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
        });

        return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "Identity.API", Version = "v1" });
        });

        return services;
    }

    public static IServiceCollection AddCustomServices(this IServiceCollection services)
    {
        services.AddMediatR(typeof(Identity.Application.Program).Assembly);
        services.AddAutoMapper(typeof(Identity.Application.Program).Assembly);
        services.AddScoped<IJwtGenerator, JwtGenerator>();
        services.AddScoped<IUserAccessor, UserAccessor>();

        return services;
    }

    public static IServiceCollection AddIdendityBuilder(this IServiceCollection services)
    {
        var builder = services.AddIdentityCore<ApplicationUser>();
        var identityBuilder = new IdentityBuilder(builder.UserType, builder.Services);
        identityBuilder.AddEntityFrameworkStores<ApplicationDbContext>();
        identityBuilder.AddSignInManager<SignInManager<ApplicationUser>>();

        return services;
    }

    public static IServiceCollection AddAuthenticationBuilder(this IServiceCollection services)
    {
        var jwtKey = Environment.GetEnvironmentVariable("JWT_KEY") ?? "asdfasdfasdf";
        var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtKey));
        var authenticationBuilder = services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme);
        authenticationBuilder.AddJwtBearer(options =>
        {
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = key,
                ValidateAudience = false,
                ValidateIssuer = false,
            };
        });

        return services;
    }

}
