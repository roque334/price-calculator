namespace Identity.Infrastructure.UserAccessors;

using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;

using Application.UserAccessors;

using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;

public class UserAccessor : IUserAccessor
{
    private readonly IHttpContextAccessor _httpContextAccessor;

    public UserAccessor(IHttpContextAccessor httpContextAccessor) => _httpContextAccessor = httpContextAccessor;

    public string GetCurrentToken()
    {
        var authorization = _httpContextAccessor
                                .HttpContext
                                .Request
                                .Headers[HeaderNames.Authorization];

        if (AuthenticationHeaderValue.TryParse(authorization, out var headerValue))
        {
            return headerValue.Parameter;
        }

        return string.Empty;
    }

    public string GetCurrentUsername() =>
        _httpContextAccessor
            .HttpContext
            .User?
            .Claims?
            .FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?
            .Value;
}
