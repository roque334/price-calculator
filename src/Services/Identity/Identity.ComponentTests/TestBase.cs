namespace Identity.ComponentTest;

using System.Threading.Tasks;

using NUnit.Framework;

using static Identity.ComponentTest.Testing;

[TestFixture]
public class TestBase
{
    [SetUp]
    public async Task SetUp()
    {
        await ResetStateAsync();
    }
}
