namespace Identity.ComponentTest;

using System;
using System.IO;
using System.Threading.Tasks;

using Identity.API;
using Identity.ComponentTests.Extensions;
using Identity.Data.Context;

using MediatR;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NUnit.Framework;

using Respawn;
using Respawn.Graph;

[SetUpFixture]
public class Testing
{
    private static IConfiguration _configuration;
    private static Checkpoint _checkpoint;
    private static IServiceScopeFactory _scopeFactory;

    [OneTimeSetUp]
    public async Task OneTimeSetUp()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables();
        _configuration = builder.Build();

        SetupEnvironmentVariables();

        var services = new ServiceCollection().AddLogging(logging => logging.AddConsole());
        var startup = new Startup(_configuration);
        startup.ConfigureServices(services);
        services.AddWebHostEnvironment()
                .AddMockedCustomServices();

        _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

        await InitializeDatabaseAsync();
        _checkpoint = new Checkpoint
        {
            TablesToIgnore = new Table[] { "__EFMigrationsHistory" }
        };
    }

    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        await TearDownDatabaseAsync();
        SetupEnvironmentVariables(up: false);
    }

    private static void SetupEnvironmentVariables(bool up = true)
    {
        Environment.SetEnvironmentVariable("JWT_KEY", up ? "ThisIsTheSecretJwt" : null);
        Environment.SetEnvironmentVariable("MSSQL_CONNECTIONSTRING", up ? "Server=localhost,1433;Database=test;User=SA;Password=Password_123;MultipleActiveResultSets=true;Encrypt=False" : null);
    }

    private static async Task InitializeDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;
        var logger = services.GetRequiredService<ILogger<Program>>();

        try
        {
            var context = services.GetRequiredService<ApplicationDbContext>();
            await context.ApplyMigrationsAsync().ConfigureAwait(false);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private static async Task TearDownDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetService<ApplicationDbContext>();
            await context.Database.EnsureDeletedAsync();
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
    {
        using var scope = _scopeFactory.CreateScope();
        var mediator = scope.ServiceProvider.GetService<IMediator>();

        return await mediator.Send(request);
    }

    public static async Task ResetStateAsync()
    {
        await _checkpoint.Reset(Environment.GetEnvironmentVariable("MSSQL_CONNECTIONSTRING"));
    }
}
