namespace Identity.ComponentTests.Extensions;

using System;
using System.Data.SqlClient;
using System.Threading.Tasks;

using Data.Context;

using Microsoft.EntityFrameworkCore;

using Polly;
using Polly.Retry;

public static class ApplicationDbContextExtensions
{
    public async static Task ApplyMigrationsAsync(this ApplicationDbContext context)
    {
        try
        {
            var policy = RetryPolicy
                .Handle<SqlException>()
                .WaitAndRetry(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)), (ex, time) =>
                {
                });

            await policy.Execute(async () =>
            {
                await context.Database.MigrateAsync().ConfigureAwait(false);
            });
        }
        catch (Exception)
        {
            throw;
        }
    }
}
