namespace Identity.ComponentTests.Extensions;

using System;

using Application.UserAccessors;

using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;

using Moq;

using static Identity.ComponentTest.Constants;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddWebHostEnvironment(this IServiceCollection services)
    {
        var webHostEnvironmentMock = new Mock<IWebHostEnvironment>(MockBehavior.Strict);
        webHostEnvironmentMock.Setup(w => w.EnvironmentName).Returns("Development");
        webHostEnvironmentMock.Setup(w => w.ApplicationName).Returns("Identity.API");
        services.AddSingleton(webHostEnvironmentMock.Object);

        return services;
    }

    public static IServiceCollection AddMockedCustomServices(this IServiceCollection services)
    {
        var userAccessorMock = new Mock<IUserAccessor>(MockBehavior.Strict);
        userAccessorMock.Setup(o => o.GetCurrentUsername()).Returns(USER_USERNAME);
        userAccessorMock.Setup(o => o.GetCurrentToken()).Returns(Guid.NewGuid().ToString());
        services.AddScoped(_ => userAccessorMock.Object);

        return services;
    }
}
