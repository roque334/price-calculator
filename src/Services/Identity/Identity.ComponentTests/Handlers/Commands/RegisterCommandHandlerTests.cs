namespace Identity.ComponentTest.Handlers.Commands;

using System.Threading.Tasks;

using Application.Exceptions;
using Application.Handlers.Commands;

using NUnit.Framework;

using Shouldly;

using static Constants;
using static Testing;

[TestFixture]
public class RegisterCommandHandlerTests : TestBase
{
    [Test]
    public async Task Handle_HappyFlow_UserCreated()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = USER_FIRSTNAME,
            LastName = USER_LASTNAME,
            Username = USER_USERNAME,
            Email = USER_EMAIL,
            Password = USER_PASSWORD,
        };

        // Act
        var user = await SendAsync(registerCommand);

        // Assert
        user.ShouldNotBeNull();
        user.FirstName.ShouldBe(registerCommand.FirstName);
        user.LastName.ShouldBe(registerCommand.LastName);
        user.Token.ShouldNotBeNull();
    }

    [Test]
    public async Task Handle_UserAlreadyExists_ThrowsException()
    {
        // Arrange
        var registerCommand = new RegisterCommand
        {
            FirstName = USER_FIRSTNAME,
            LastName = USER_LASTNAME,
            Username = USER_USERNAME,
            Email = USER_EMAIL,
            Password = USER_PASSWORD,
        };
        var user = await SendAsync(registerCommand);

        // Act + Assert
        await Should.ThrowAsync<AccountException>(() => SendAsync(registerCommand));
    }
}
