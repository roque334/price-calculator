namespace Identity.Application.Handlers.Commands;

using Model;

using MediatR;

public class RegisterCommand : IRequest<User>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string Password { get; set; }
}
