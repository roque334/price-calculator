namespace Identity.Application.Handlers.Commands;

using System.Net;
using System.Threading;
using System.Threading.Tasks;

using AutoMapper;

using Domain.Models;

using Exceptions;

using JwtGenerators;

using MediatR;

using Microsoft.AspNetCore.Identity;

using Model;

public class LoginCommandHandler : IRequestHandler<LoginCommand, User>
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly IJwtGenerator _jwtGenerator;
    private readonly IMapper _mapper;

    public LoginCommandHandler(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, IJwtGenerator jwtGenerator, IMapper mapper)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _jwtGenerator = jwtGenerator;
        _mapper = mapper;
    }

    public async Task<User> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        var applicationUser = await _userManager.FindByEmailAsync(request.Email);
        if (applicationUser == null)
        {
            throw new AccountException
            (
                HttpStatusCode.Unauthorized,
                new { message = "The email and password provided are incorrect" }
            );
        }

        var signInResult = await _signInManager.CheckPasswordSignInAsync(applicationUser, request.Password, false);
        if (signInResult.Succeeded)
        {
            var token = _jwtGenerator.CreateToken(applicationUser);
            var user = _mapper.Map<ApplicationUser, User>(applicationUser);
            user.Token = token;
            return user;
        }

        throw new AccountException
        (
            HttpStatusCode.Unauthorized,
            new { message = "The email and password provided are incorrect" }
        );
    }
}
