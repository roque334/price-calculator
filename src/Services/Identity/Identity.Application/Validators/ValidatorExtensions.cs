namespace Identity.Application.Validators;

using FluentValidation;

public static class ValidatorExtensions
{
    public static IRuleBuilder<T, string> Password<T>(this IRuleBuilder<T, string> ruleBuilder)
    {
        return ruleBuilder
            .NotEmpty()
            .MinimumLength(6).WithMessage("Password must be at least 6 characters")
            .Matches("[A-Z]").WithMessage("Password must have at leat 1 uppercase letter")
            .Matches("[a-z]").WithMessage("Password must have at leat 1 lowercase letter")
            .Matches("[0-9]").WithMessage("Password must contain a number")
            .Matches("[^a-zA-z0-9]").WithMessage("Password must contain non alphanumeric");
    }
}
