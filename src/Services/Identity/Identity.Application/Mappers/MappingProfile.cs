namespace Identity.Application.Mappers;

using AutoMapper;

using Domain.Models;

using Handlers.Commands;

using Model;

public class MappingProfile : Profile
{
    public MappingProfile()
    {
        CreateMap<ApplicationUser, User>();
        CreateMap<RegisterCommand, ApplicationUser>();
    }
}
