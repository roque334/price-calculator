namespace Identity.Application.JwtGenerators;

using Domain.Models;

public interface IJwtGenerator
{
    string CreateToken(ApplicationUser user);
}
