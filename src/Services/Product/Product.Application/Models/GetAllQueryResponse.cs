namespace Product.Application.Models;

public class GetAllQueryResponse
{
    public ProductDTO[] Products { get; set; }
    public int Total { get; set; }

    public GetAllQueryResponse(ProductDTO[] products, int total)
    {
        Products = products;
        Total = total;
    }
}
