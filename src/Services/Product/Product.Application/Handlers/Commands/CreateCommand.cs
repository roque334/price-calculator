namespace Product.Application.Handlers.Commands;

using Domain.Models.ProductAggregate;

using MediatR;

public class MaterialComposition
{
    public int Id { get; set; }
    public int Quantity { get; set; }
}

public class CreateCommand : IRequest<ProductEntity>
{
    public string Name { get; set; }
    public decimal LaborValue { get; set; }
    public MaterialComposition[] Materials { get; set; }

}
