namespace Product.Application.Handlers.Commands;

using MediatR;

public class DeleteCommand : IRequest<Unit>
{
    public int Id { get; set; }
}
