namespace Product.Application.Handlers.Queries;

using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Domain.Models.ProductAggregate;

using MediatR;

using Microsoft.Extensions.Logging;

using Models;

using Specifications;

using UserAccessors;

public class GetAllQueryHandler : IRequestHandler<GetAllQuery, GetAllQueryResponse>
{
    private readonly IProductRepository _productRepository;
    private readonly IUserAccessor _userAccessor;

    public GetAllQueryHandler(IProductRepository productRepository, IUserAccessor userAccessor)
    {
        _productRepository = productRepository;
        _userAccessor = userAccessor;
    }

    public async Task<GetAllQueryResponse> Handle(GetAllQuery request, CancellationToken cancellationToken)
    {
        var username = _userAccessor.GetCurrentUsername();

        var count = await _productRepository.CountProductsAsync(username);

        var products = await _productRepository.FindProductsAsync(new ProductsWithUsernameSpecification(username, request.PaginationParams.Skip, request.PaginationParams.Take));

        return new GetAllQueryResponse(
            products.Select(p => new ProductDTO(p.Id, p.Name, p.ProductMaterialLinks.Sum(pm => pm.Material.PricePerMeasurementUnit * pm.Quantity) + p.LaborValue)).ToArray(),
            count
        );
    }
}
