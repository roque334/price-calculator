namespace Product.Application.Handlers.Queries;

using Domain.Models.ProductAggregate;

using MediatR;

public class GetByIdQuery : IRequest<ProductEntity>
{
    public int Id { get; set; }
}
