namespace Product.Application.Handlers.IntegrationEvents;

using System;
using System.Threading.Tasks;

using Application.Specifications;

using Common.Events.Models;

using Core.EventBus.Domain.Handlers;

using Domain.Models.MaterialAggregate;

using Microsoft.Extensions.Logging;

public class MaterialUpdatedIntegrationEventHandler : IIntegrationEventHandler<MaterialUpdatedIntegrationEvent>
{
    private readonly IMaterialRepository _repository;
    private readonly ILogger<MaterialUpdatedIntegrationEventHandler> _logger;

    public MaterialUpdatedIntegrationEventHandler(IMaterialRepository repository, ILogger<MaterialUpdatedIntegrationEventHandler> logger)
    {
        _repository = repository;
        _logger = logger;
    }

    public async Task Handle(MaterialUpdatedIntegrationEvent @event)
    {
        _logger.LogInformation(
            "----- Handling event: {eventId} ({eventType}) from {assemblyName} - {event}",
            @event.Id, @event.GetType(), this.GetType().Assembly.GetName(), @event);

        var material = await _repository.FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(@event.MaterialId, @event.UserName));
        if (material == null)
        {
            throw new Exception($"Material {@event.Name} not found.");
        }

        material.MeasurementName = @event.MeasurementName;
        material.PricePerMeasurementUnit = @event.PricePerMeasurementUnit;
        material.SetVersion(@event.Version);

        _repository.Update(material);
        if (!await _repository.UnitOfWork.SaveEntitiesAsync(default))
        {
            throw new Exception("Problem saving changes");
        }
    }
}
