namespace Product.Application.Handlers.IntegrationEvents;

using System;
using System.Threading.Tasks;

using Application.Specifications;

using Common.Events.Models;

using Core.EventBus.Domain.Handlers;

using Domain.Models.MaterialAggregate;

using Microsoft.Extensions.Logging;

public class MaterialCreatedIntegrationEventHandler : IIntegrationEventHandler<MaterialCreatedIntegrationEvent>
{
    private readonly IMaterialRepository _repository;
    private readonly ILogger<MaterialCreatedIntegrationEventHandler> _logger;

    public MaterialCreatedIntegrationEventHandler(IMaterialRepository repository, ILogger<MaterialCreatedIntegrationEventHandler> logger)
    {
        _repository = repository;
        _logger = logger;
    }

    public async Task Handle(MaterialCreatedIntegrationEvent @event)
    {
        _logger.LogInformation(
            "----- Handling event: {eventId} ({eventType}) from {assemblyName} - {event}",
            @event.Id, @event.GetType(), this.GetType().Assembly.GetName(), @event);

        if (await _repository.FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(@event.MaterialId, @event.UserName)) != null)
        {
            throw new Exception($"The material {@event.Name} already exists.");
        }

        var material = new MaterialEntity
        (
            @event.MaterialId,
            @event.Name,
            @event.MeasurementName,
            @event.PricePerMeasurementUnit,
            @event.UserName,
            @event.Version
        );

        _repository.Add(material);
        if (!await _repository.UnitOfWork.SaveEntitiesAsync())
        {
            throw new Exception("Problem saving changes");
        }
    }
}
