namespace Product.Application.Handlers.IntegrationEvents;

using System;
using System.Threading.Tasks;

using Application.Specifications;

using Common.Events.Models;

using Core.EventBus.Domain.Handlers;

using Domain.Models.MaterialAggregate;
using Domain.Models.ProductAggregate;

using Microsoft.Extensions.Logging;

public class MaterialDeletedIntegrationEventHandler : IIntegrationEventHandler<MaterialDeletedIntegrationEvent>
{
    private readonly IMaterialRepository _materialRepository;
    private readonly IProductRepository _productRepository;
    private readonly ILogger<MaterialDeletedIntegrationEventHandler> _logger;

    public MaterialDeletedIntegrationEventHandler(IMaterialRepository materialRepository, IProductRepository productRepository, ILogger<MaterialDeletedIntegrationEventHandler> logger)
    {
        _materialRepository = materialRepository;
        _productRepository = productRepository;
        _logger = logger;
    }

    public async Task Handle(MaterialDeletedIntegrationEvent @event)
    {
        _logger.LogInformation(
            "----- Handling event: {eventId} ({eventType}) from {assemblyName} - {event}",
            @event.Id, @event.GetType(), this.GetType().Assembly.GetName(), @event);

        var material = await _materialRepository.FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(@event.MaterialId, @event.UserName));
        if (material == null)
        {
            throw new Exception($"Material {@event.Name} not found.");
        }

        var products = await _productRepository.FindProductsAsync(new ProductsWithMaterialIdSpecification(@event.MaterialId));
        foreach (var product in products)
        {
            if (product.RemoveMaterial(@event.MaterialId))
            {
                _productRepository.Update(product);
            }
        }

        _materialRepository.Delete(material);
        if (!await _productRepository.UnitOfWork.SaveEntitiesAsync() && await _materialRepository.UnitOfWork.SaveEntitiesAsync())
        {
            throw new Exception("Problem saving changes");
        }
    }
}
