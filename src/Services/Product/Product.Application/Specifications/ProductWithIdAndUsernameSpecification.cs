namespace Product.Application.Specifications;

using Core.Domain.Models;

using Domain.Models.ProductAggregate;

public class ProductWithIdAndUsernameSpecification : BaseSpecification<ProductEntity>
{
    public ProductWithIdAndUsernameSpecification(int id, string username) : base(p => p.Id == id && p.UserName == username)
    {
        AddInclude(p => p.ProductMaterialLinks);
        AddInclude($"{nameof(ProductEntity.ProductMaterialLinks)}.{nameof(ProductMaterial.Material)}");
    }
}
