namespace Product.Application.Exceptions;

using System;
using System.Net;

public class ProductException : Exception
{
    public readonly HttpStatusCode Code;
    public readonly object Error;
    public ProductException(HttpStatusCode code, object error = null)
    {
        Code = code;
        Error = error;
    }
}
