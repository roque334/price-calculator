namespace Product.UnitTests.Application.Handlers.IntegrationEvents;
using System;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using Core.Domain.Models;

using Microsoft.Extensions.Logging;

using Moq;

using NUnit.Framework;

using Product.Application.Handlers.IntegrationEvents;
using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;

using Shouldly;

[TestFixture]
public class MaterialCreatedIntegrationEventHandlerTests : HandlerFixture
{
    private readonly Mock<IUnitOfWork> _unitOfWorkMock = new(MockBehavior.Strict);
    private readonly Mock<IMaterialRepository> _materialRepositoryMock = new(MockBehavior.Strict);
    private readonly Mock<ILogger<MaterialCreatedIntegrationEventHandler>> _loggerMock = new(MockBehavior.Loose);

    private MaterialCreatedIntegrationEventHandler _sut;

    [SetUp]
    public void SetUp() => _sut = new(_materialRepositoryMock.Object, _loggerMock.Object);


    [TearDown]
    public void TearDown()
    {
        _unitOfWorkMock.Reset();
        _materialRepositoryMock.Reset();
        _loggerMock.Reset();
    }

    [Test]
    public async Task Handle_HappyFlow_Success()
    {
        // Arrange
        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        _materialRepositoryMock.Setup(x => x.Add(It.Is<MaterialEntity>(x => x.Name == materials[0].Name))).Returns(materials[0]);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(true);

        var materialCreatedIntegrationEvent = new MaterialCreatedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].MeasurementName, materials[0].PricePerMeasurementUnit, materials[0].UserName, 0);

        // Act
        await _sut.Handle(materialCreatedIntegrationEvent);

        // Assert
        Assert.Pass();

        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialAlreadyExists_Fail()
    {
        // Arrange
        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync(materials[0]);

        var materialCreatedIntegrationEvent = new MaterialCreatedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].MeasurementName, materials[0].PricePerMeasurementUnit, materials[0].UserName, 0);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(materialCreatedIntegrationEvent));

        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

    [Test]
    public async Task Handle_MaterialProblemSavingChanges_Fail()
    {
        // Arrange
        _materialRepositoryMock.Setup(o => o.FindMaterialAsync(It.IsAny<MaterialWithIdAndUsernameSpecification>())).ReturnsAsync((MaterialEntity)null);

        _materialRepositoryMock.Setup(x => x.Add(It.Is<MaterialEntity>(x => x.Name == materials[0].Name))).Returns(materials[0]);

        _materialRepositoryMock.Setup(o => o.UnitOfWork).Returns(_unitOfWorkMock.Object);

        _unitOfWorkMock.Setup(o => o.SaveEntitiesAsync(default)).ReturnsAsync(false);

        var materialCreatedIntegrationEvent = new MaterialCreatedIntegrationEvent(materials[0].Id, materials[0].Name, materials[0].MeasurementName, materials[0].PricePerMeasurementUnit, materials[0].UserName, 0);

        // Act + Assert
        await Should.ThrowAsync<Exception>(() => _sut.Handle(materialCreatedIntegrationEvent));

        _unitOfWorkMock.VerifyAll();
        _materialRepositoryMock.VerifyAll();
    }

}
