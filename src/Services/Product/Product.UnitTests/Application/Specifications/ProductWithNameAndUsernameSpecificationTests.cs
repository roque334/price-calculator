namespace Product.UnitTests.Application.Specifications;

using System.Linq;

using NUnit.Framework;

using Product.Application.Specifications;

using Shouldly;

[TestFixture]
public class ProductWithNameAndUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_ProductExists_Success()
    {
        // Arrange
        var product = products.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = products.FirstOrDefault(new ProductWithNameAndUsernameSpecification(product.Name, FIRST_USERNAME).Criteria.Compile());

        // Assert
        found.ShouldBe(product);
    }

    [Test]
    public void Evaluate_ProductDoesNotExists_Success()
    {
        // Arrange
        var product = products.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = products.FirstOrDefault(new ProductWithNameAndUsernameSpecification(product.Name, NON_EXISTING_USERNAME).Criteria.Compile());

        // Assert
        found.ShouldBeNull();
    }
}
