namespace Product.UnitTests.Application.Specifications;

using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using AutoFixture;

using Core.Domain.Models;

using NUnit.Framework;

using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

[TestFixture]
public class SpecificationFixture
{
    protected const string FIRST_USERNAME = "test1";
    protected const string SECOND_USERNAME = "test2";
    protected const string NON_EXISTING_USERNAME = "test3";
    protected IList<MaterialEntity> materials;
    protected IList<ProductEntity> products;

    [OneTimeSetUp]
    public void OneTimeSetUp()
    {
        materials = new List<MaterialEntity>
        {
            new MaterialEntity(1, "Envelope", "Units", 0.50M, FIRST_USERNAME, 0),
            new MaterialEntity(2, "Paper", "Units", 0.25M, FIRST_USERNAME, 0),
            new MaterialEntity(3, "Stamp", "Units", 0.25M, FIRST_USERNAME, 0),
            new MaterialEntity(4, "Envelope", "Units", 1.50M, SECOND_USERNAME, 0),
            new MaterialEntity(5, "Paper", "Units", 1.25M, SECOND_USERNAME, 0),
            new MaterialEntity(6, "Stamp", "Units", 1.25M, SECOND_USERNAME, 0),
        };

        products = new List<ProductEntity>();
        var product = new ProductEntity("Letter", 1.00M, FIRST_USERNAME);
        product.UpsertMaterial(1, 10);
        product.UpsertMaterial(2, 20);
        SetProductId(product, 1);
        products.Add(product);

        product = new ProductEntity("Maxi Letter", 5.00M, FIRST_USERNAME);
        product.UpsertMaterial(1, 50);
        product.UpsertMaterial(2, 100);
        SetProductId(product, 2);
        products.Add(product);

        product = new ProductEntity("Letter", 1.00M, SECOND_USERNAME);
        product.UpsertMaterial(3, 10);
        product.UpsertMaterial(4, 20);
        SetProductId(product, 3);
        products.Add(product);

        product = new ProductEntity("Maxi Letter", 5.00M, SECOND_USERNAME);
        product.UpsertMaterial(3, 50);
        product.UpsertMaterial(4, 100);
        SetProductId(product, 3);
        products.Add(product);
    }

    private static void SetProductId(ProductEntity product, int id) =>
        typeof(Entity)
            .GetProperty("Id")
            .SetValue(product, id);
}
