namespace Product.UnitTests.Application.Specifications;

using System.Linq;

using NUnit.Framework;

using Product.Application.Specifications;

using Shouldly;

[TestFixture]
public class MaterialWithIdAndUsernameSpecificationTests : SpecificationFixture
{
    [Test]
    public void Evaluate_MaterialExists_Success()
    {
        // Arrange
        var material = materials.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = materials.FirstOrDefault(new MaterialWithIdAndUsernameSpecification(material.Id, FIRST_USERNAME).Criteria.Compile());

        // Assert
        material.ShouldBe(material);
    }

    [Test]
    public void Evaluate_MaterialDoesNotExists_Success()
    {
        // Arrange
        var material = materials.FirstOrDefault(x => x.UserName == FIRST_USERNAME);

        // Act
        var found = materials.FirstOrDefault(new MaterialWithIdAndUsernameSpecification(material.Id, NON_EXISTING_USERNAME).Criteria.Compile());

        // Assert
        found.ShouldBeNull();
    }
}
