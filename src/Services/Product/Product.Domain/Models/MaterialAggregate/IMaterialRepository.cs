namespace Product.Domain.Models.MaterialAggregate;

using System.Threading.Tasks;

using Core.Domain.Models;

public interface IMaterialRepository : IRepository<MaterialEntity>
{
    MaterialEntity Add(MaterialEntity material);
    MaterialEntity Update(MaterialEntity material);
    MaterialEntity Delete(MaterialEntity material);
    Task<MaterialEntity> FindMaterialAsync(ISpecification<MaterialEntity> spec);
}
