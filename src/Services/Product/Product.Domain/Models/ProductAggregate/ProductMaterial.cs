namespace Product.Domain.Models.ProductAggregate;

using MaterialAggregate;

public class ProductMaterial
{
    public int ProductId { get; set; }
    public ProductEntity Product { get; set; }
    public int MaterialId { get; set; }
    public MaterialEntity Material { get; set; }
    public int Quantity { get; set; }
}
