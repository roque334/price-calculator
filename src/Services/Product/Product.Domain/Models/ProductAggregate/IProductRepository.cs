namespace Product.Domain.Models.ProductAggregate;

using System.Collections.Generic;
using System.Threading.Tasks;

using Core.Domain.Models;

public interface IProductRepository : IRepository<ProductEntity>
{
    ProductEntity Add(ProductEntity product);
    ProductEntity Update(ProductEntity product);
    ProductEntity Delete(ProductEntity product);
    Task<ProductEntity> FindProductAsync(ISpecification<ProductEntity> spec);
    Task<int> CountProductsAsync(string username);
    Task<IEnumerable<ProductEntity>> FindProductsAsync(ISpecification<ProductEntity> spec);
}
