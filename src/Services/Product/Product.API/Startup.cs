namespace Product.API;

using Application.Handlers.IntegrationEvents;

using Common.Events.Models;

using Core.EventBus.Domain.Bus;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using ServiceCollectionExtensions;

public class Startup
{
    public Startup(IConfiguration configuration) => Configuration = configuration;

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {

        services
            .AddCustomControllers()
            .AddSwagger()
            .AddCustomServices()
            .AddDbContext()
            .AddAuthenticationBuilder()
            .AddIntegrationServices()
            .AddEventBus();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app)
    {
        app.UseSwagger(c =>
                    {
                        c.RouteTemplate = "api/products/swagger/{documentname}/swagger.json";
                    });
        app.UseSwaggerUI(c =>
        {
            c.SwaggerEndpoint("/api/products/swagger/v1/swagger.json", "Product.API v1");
            c.RoutePrefix = "api/products/swagger";
        });

        app.UseRouting();
        app.UseCors("CorsPolicy");

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });

        ConfigureEventBus(app);
    }

    private static void ConfigureEventBus(IApplicationBuilder app)
    {
        var eventBus = app.ApplicationServices.GetRequiredService<IEventBus>();

        eventBus.Subscribe<MaterialCreatedIntegrationEvent, MaterialCreatedIntegrationEventHandler>();
        eventBus.Subscribe<MaterialUpdatedIntegrationEvent, MaterialUpdatedIntegrationEventHandler>();
        eventBus.Subscribe<MaterialDeletedIntegrationEvent, MaterialDeletedIntegrationEventHandler>();
    }
}
