namespace Product.API.Controllers;

using System.Net;
using System.Threading.Tasks;

using Application.Handlers.Commands;
using Application.Handlers.Queries;
using Application.Models;

using Domain.Models.ProductAggregate;

using MediatR;

using Microsoft.AspNetCore.Mvc;

public class ProductsController : ControllerBaseCustom
{
    public ProductsController(IMediator mediator) : base(mediator)
    {
    }

    [HttpGet]
    [ProducesResponseType(typeof(ProductDTO[]), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<GetAllQueryResponse>> GetAll([FromQuery] int skip, [FromQuery] int take) =>
        await Mediator.Send(new GetAllQuery(new PaginationParams(skip, take)));

    [HttpGet("{Id:int}")]
    [ProducesResponseType(typeof(ProductEntity), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<ProductEntity>> GetById([FromRoute] GetByIdQuery request) =>
        await Mediator.Send(request);

    [HttpPost]
    [ProducesResponseType(typeof(ProductEntity), (int)HttpStatusCode.Created)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<ProductEntity>> Create(CreateCommand request)
    {
        var created = await Mediator.Send(request);

        return CreatedAtAction(nameof(GetById), new { created.Id }, await GetById(new GetByIdQuery { Id = created.Id }));
    }

    [HttpPut]
    [ProducesResponseType(typeof(ProductEntity), (int)HttpStatusCode.OK)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<ActionResult<ProductEntity>> Update(UpdateCommand request)
    {
        var updated = await Mediator.Send(request);

        return Ok(await GetById(new GetByIdQuery { Id = updated.Id }));
    }
}
