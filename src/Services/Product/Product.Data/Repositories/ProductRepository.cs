namespace Product.Data.Repositories;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Context;

using Core.Domain.Models;

using Domain.Models.ProductAggregate;

using Microsoft.EntityFrameworkCore;

using Specifications;

public class ProductRepository : IProductRepository
{
    private readonly ProductContext _context;

    public IUnitOfWork UnitOfWork => _context;

    public ProductRepository(ProductContext context) => _context = context;

    public ProductEntity Add(ProductEntity product)
    {
        var added = _context
            .Products
            .Add(product)
            .Entity;

        _context
            .Entry(added)
            .Collection(p => p.ProductMaterialLinks)
            .Query()
            .Include(pm => pm.Material)
            .Load();

        return added;
    }

    public ProductEntity Update(ProductEntity product)
    {
        product.IncreaseVersion();
        var updated = _context
            .Products
            .Update(product)
            .Entity;
        _context.Entry(updated).Collection(p => p.ProductMaterialLinks).Load();

        return updated;
    }

    public ProductEntity Delete(ProductEntity product)
    {
        var deleted = _context
            .Products
            .Remove(product)
            .Entity;

        return deleted;
    }

    public async Task<ProductEntity> FindProductAsync(ISpecification<ProductEntity> spec)
    {
        var query = ApplySpecification(spec);
        return await query.FirstOrDefaultAsync();
    }

    public async Task<IEnumerable<ProductEntity>> FindProductsAsync(ISpecification<ProductEntity> spec)
    {
        var query = ApplySpecification(spec);
        return await query.ToArrayAsync();
    }

    public async Task<int> CountProductsAsync(string username) =>
        await _context.Products.CountAsync((x) => x.UserName == username);

    private IQueryable<ProductEntity> ApplySpecification(ISpecification<ProductEntity> spec) =>
        SpecificationEvaluator<ProductEntity>.GetQuery(_context.Products.AsQueryable(), spec);
}
