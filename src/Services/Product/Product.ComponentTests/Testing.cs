namespace Product.ComponentTests;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using Core.Domain.Models;
using Core.EventBus.Domain.Bus;
using Core.EventBus.Domain.Events;

using MediatR;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

using NUnit.Framework;

using Product.API;
using Product.Application.Handlers.IntegrationEvents;
using Product.ComponentTests.Extensions;
using Product.Data.Context;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using Respawn;
using Respawn.Graph;

[SetUpFixture]
public class Testing
{
    private static IConfiguration _configuration;
    private static Checkpoint _checkpoint;
    private static IServiceScopeFactory _scopeFactory;

    [OneTimeSetUp]
    public async Task Setup()
    {
        var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json")
            .AddEnvironmentVariables();
        _configuration = builder.Build();

        SetupEnvironmentVariables();

        var services = new ServiceCollection().AddLogging(logging => logging.AddConsole());
        var startup = new Startup(_configuration);
        startup.ConfigureServices(services);
        services.AddWebHostEnvironment()
                .AddMockedCustomServices();

        _scopeFactory = services.BuildServiceProvider().GetService<IServiceScopeFactory>();

        await InitializeDatabaseAsync();
        await InitializeMessageConsumersAsync();
        _checkpoint = new Checkpoint
        {
            TablesToIgnore = new Table[] { "__EFMigrationsHistory" }
        };
    }

    [OneTimeTearDown]
    public async Task OneTimeTearDown()
    {
        await TearDownDatabaseAsync();
        SetupEnvironmentVariables(up: false);
    }

    private static void SetupEnvironmentVariables(bool up = true)
    {
        Environment.SetEnvironmentVariable("JWT_KEY", up ? "ThisIsTheSecretJwt" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_HOSTNAME", up ? "localhost" : null);
        Environment.SetEnvironmentVariable("MSSQL_CONNECTIONSTRING", up ? "Server=localhost,1433;Database=test;User=SA;Password=Password_123;MultipleActiveResultSets=true;Encrypt=False" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_USERNAME", up ? "guest" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_PASSWORD", up ? "guest" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_RETRYCOUNT", up ? "5" : null);
        Environment.SetEnvironmentVariable("EVENT_BUS_SUBSCRIPTION_CLIENTNAME", up ? "price_calculator" : null);
    }

    private static async Task InitializeDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetService<ProductContext>();
            await context.ApplyMigrationsAsync();
        }
        catch (Exception)
        {

            throw;
        }
    }

    private static async Task TearDownDatabaseAsync()
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetService<ProductContext>();
            await context.Database.EnsureDeletedAsync();
        }
        catch (Exception)
        {

            throw;
        }
    }

    private static async Task InitializeMessageConsumersAsync(CancellationToken cancellationToken = default)
    {
        await new TaskFactory().StartNew(() =>
        {
            using var scope = _scopeFactory.CreateScope();
            var services = scope.ServiceProvider;
            var eventBus = services.GetRequiredService<IEventBus>();

            eventBus.Subscribe<MaterialCreatedIntegrationEvent, MaterialCreatedIntegrationEventHandler>();
            eventBus.Subscribe<MaterialUpdatedIntegrationEvent, MaterialUpdatedIntegrationEventHandler>();
            eventBus.Subscribe<MaterialDeletedIntegrationEvent, MaterialDeletedIntegrationEventHandler>();
        }, cancellationToken, TaskCreationOptions.LongRunning, TaskScheduler.Current);
    }

    public static async Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;
        var mediator = services.GetService<IMediator>();

        return await mediator.Send(request);
    }

    public static async Task ResetState()
    {
        await _checkpoint.Reset(Environment.GetEnvironmentVariable("MSSQL_CONNECTIONSTRING"));
    }

    public static async Task<MaterialEntity> AddMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IMaterialRepository>();
            var created = repository.Add(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return created;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> UpdateMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<ProductContext>();
            var repository = services.GetRequiredService<IMaterialRepository>();
            context.Entry<MaterialEntity>(material).State = EntityState.Modified;
            var updated = repository.Update(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return updated;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> DeleteMaterialAsync(MaterialEntity material)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<ProductContext>();
            var repository = services.GetRequiredService<IMaterialRepository>();
            context.Entry<MaterialEntity>(material).State = EntityState.Modified;
            var updated = repository.Delete(material);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return updated;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<MaterialEntity> FindMaterialAsync(ISpecification<MaterialEntity> specification)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IMaterialRepository>();
            var material = await repository.FindMaterialAsync(specification);

            return material;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<ProductEntity> AddProductAsync(ProductEntity product)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IProductRepository>();
            var created = repository.Add(product);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return created;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<ProductEntity> UpdateProductAsync(ProductEntity product)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<ProductContext>();
            var repository = services.GetRequiredService<IProductRepository>();
            context.Entry<ProductEntity>(product).State = EntityState.Modified;
            var updated = repository.Update(product);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return updated;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<ProductEntity> DeleteProductAsync(ProductEntity product)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<ProductContext>();
            var repository = services.GetRequiredService<IProductRepository>();
            context.Entry<ProductEntity>(product).State = EntityState.Modified;
            var deleted = repository.Delete(product);
            await repository.UnitOfWork.SaveEntitiesAsync();

            return deleted;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<ProductEntity> FindProductAsync(ISpecification<ProductEntity> specification)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IProductRepository>();
            var product = await repository.FindProductAsync(specification);

            return product;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static async Task<IEnumerable<ProductEntity>> FindProductsAsync(ISpecification<ProductEntity> specification)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var repository = services.GetRequiredService<IProductRepository>();
            var product = await repository.FindProductsAsync(specification);

            return product;
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static void ExecuteSqlInterpolated(FormattableString sql)
    {
        using var scope = _scopeFactory.CreateScope();
        var services = scope.ServiceProvider;

        try
        {
            var context = services.GetRequiredService<ProductContext>();
            context.Database.ExecuteSqlInterpolated(sql);
        }
        catch (Exception)
        {
            throw;
        }
    }

    public static void PublishIntegrationEvent(IntegrationEvent @event)
    {
        using var scope = _scopeFactory.CreateScope();
        var eventBus = scope.ServiceProvider.GetService<IEventBus>();

        eventBus.Publish(@event);
    }
}
