namespace Product.ComponentTests.Handlers.IntegrationEvents;
using System;
using System.Threading;
using System.Threading.Tasks;

using Common.Events.Models;

using NUnit.Framework;

using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;

using Shouldly;

using static Product.ComponentTests.Constants;
using static Product.ComponentTests.Testing;

[TestFixture]
public class MaterialDeletedIntegrationEventHandlerTests : TestBase
{
    [Test]
    public async Task Handle_HappyFlow_DeletesMaterial()
    {
        var envelope = new MaterialEntity(1, "Envelope", "Units", 1.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);

        var @event = new MaterialDeletedIntegrationEvent(1, "Envelope", USER_USERNAME);

        PublishIntegrationEvent(@event);

        var materialEntity = Execute(
            () =>
            {
                MaterialEntity found = null;
                do
                {
                    Thread.Sleep(200);
                    found = FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(@event.MaterialId, @event.UserName)).Result;
                } while (found != null);

                return found;
            },
            TimeSpan.FromSeconds(3),
            () => null);

        materialEntity.ShouldBeNull();
    }
}
