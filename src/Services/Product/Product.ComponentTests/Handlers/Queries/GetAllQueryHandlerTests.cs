namespace Product.ComponentTests.Handlers.Queries;
using System.Linq;
using System.Threading.Tasks;

using Product.Application.Handlers.Queries;
using Product.Application.Models;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using NUnit.Framework;

using Shouldly;

using static Product.ComponentTests.Constants;
using static Product.ComponentTests.Testing;

public class GetAllQueryHandlerTests : TestBase
{
    [Test]
    public async Task Handle_HappyFlow_GetsProducts()
    {
        // Arrange
        var paginationParams = new PaginationParams(0, 20);
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.30M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);

        var paper = new MaterialEntity(2, "Paper", "Units", 0.40M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);

        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.50M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var letter = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        letter.UpsertMaterial(1, 1);
        letter.UpsertMaterial(2, 2);
        letter.UpsertMaterial(3, 1);
        await AddProductAsync(letter);

        var maxiLetter = new ProductEntity("Maxi Letter", 1.50M, USER_USERNAME);
        maxiLetter.UpsertMaterial(1, 1);
        maxiLetter.UpsertMaterial(2, 4);
        maxiLetter.UpsertMaterial(3, 1);
        await AddProductAsync(maxiLetter);

        // Act
        var found = await SendAsync(new GetAllQuery(paginationParams));

        // Assert
        found.ShouldNotBeNull();
        found.Total.ShouldBe(2);
        found.Products.Length.ShouldBe(2);
        found.Products[0].Name.ShouldBe("Letter");
        found.Products[1].Name.ShouldBe("Maxi Letter");
    }

    [Test]
    public async Task Handle_ProductsDoesNotExists_ReturnsEmptyProductCollection()
    {
        // Arrange
        var paginationParams = new PaginationParams(0, 20);

        // Act
        var found = await SendAsync(new GetAllQuery(paginationParams));

        // Assert
        found.ShouldNotBeNull();
        found.Total.ShouldBe(0);
        found.Products.ShouldBeEmpty();
    }
}
