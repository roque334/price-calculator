namespace Product.ComponentTests.Repositories;
using System.Linq;
using System.Threading.Tasks;

using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;
using Product.Domain.Models.ProductAggregate;

using NUnit.Framework;

using Shouldly;

using static Product.ComponentTests.Testing;
using static Product.ComponentTests.Constants;

[TestFixture]
public class ProductRepositoryTests : TestBase
{
    [Test]
    public async Task Create_HappyFlow_CreatesProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);

        // Act
        var created = await AddProductAsync(product);

        // Assert
        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(product.Id);
        found.Name.ShouldBe(product.Name);
        found.LaborValue.ShouldBe(product.LaborValue);
        found.UserName.ShouldBe(product.UserName);
        found.Version.ShouldBe(0);

        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Create_ProductEntityAlreadyExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateException>(() => AddProductAsync(product));
    }

    [Test]
    public async Task Update_HappyFlow_UpdatesProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);

        // Act
        var updated = await UpdateProductAsync(created);

        // Assert
        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(product.Id);
        found.Name.ShouldBe(product.Name);
        found.LaborValue.ShouldBe(product.LaborValue);
        found.UserName.ShouldBe(product.UserName);
        found.Version.ShouldBe(1);

        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Update_ProductEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);
        await DeleteProductAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateProductAsync(product));

        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Update_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[products] SET LaborValue = 0.25, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateProductAsync(created));

        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(product.Id);
        found.Name.ShouldBe(product.Name);
        found.LaborValue.ShouldBe(0.25M);
        found.UserName.ShouldBe(product.UserName);
        found.Version.ShouldBe(2);

        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Delete_HappyFlow_DeletesProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);

        // Act
        var deleted = await DeleteProductAsync(created);

        // Assert
        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_ProductEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);
        await DeleteProductAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteProductAsync(created));

        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[products] SET LaborValue = 0.25, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteProductAsync(created));

        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(created.Id);
        found.Name.ShouldBe(created.Name);
        found.LaborValue.ShouldBe(0.25M);
        found.UserName.ShouldBe(created.UserName);
        found.Version.ShouldBe(2);
    }

    [Test]
    public async Task Find_ProductWithIdAndUsernameSpecification_FindsProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);

        // Act
        var found = await FindProductAsync(new ProductWithIdAndUsernameSpecification(created.Id, created.UserName));

        // Assert
        found.ShouldNotBeNull();
        found.Id.ShouldBe(product.Id);
        found.Name.ShouldBe(product.Name);
        found.LaborValue.ShouldBe(product.LaborValue);
        found.UserName.ShouldBe(product.UserName);
        found.Version.ShouldBe(0);

        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Find_ProductWithNameAndUsernameSpecification_FindsProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var product = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        product.UpsertMaterial(1, 1);
        product.UpsertMaterial(2, 2);
        product.UpsertMaterial(3, 1);
        var created = await AddProductAsync(product);

        // Act   
        var found = await FindProductAsync(new ProductWithNameAndUsernameSpecification(created.Name, created.UserName));

        // Assert
        found.ShouldNotBeNull();
        found.Id.ShouldBe(product.Id);
        found.Name.ShouldBe(product.Name);
        found.LaborValue.ShouldBe(product.LaborValue);
        found.UserName.ShouldBe(product.UserName);
        found.Version.ShouldBe(0);

        foreach (var link in found.ProductMaterialLinks)
        {
            link.MaterialId.ShouldBeOneOf(1, 2, 3);
        }
    }

    [Test]
    public async Task Find_ProductsWithMaterialIdSpecification_FindsProductEntity()
    {
        // Arrange
        var envelope = new MaterialEntity(1, "Envelope", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(envelope);
        var paper = new MaterialEntity(2, "Paper", "Units", 0.15M, USER_USERNAME, 0);
        await AddMaterialAsync(paper);
        var stamp = new MaterialEntity(3, "Stamp", "Units", 0.25M, USER_USERNAME, 0);
        await AddMaterialAsync(stamp);

        var letter = new ProductEntity("Letter", 1.50M, USER_USERNAME);
        letter.UpsertMaterial(1, 1);
        letter.UpsertMaterial(2, 2);
        letter.UpsertMaterial(3, 1);
        await AddProductAsync(letter);

        var miniLetter = new ProductEntity("Mini Letter", 0.50M, USER_USERNAME);
        miniLetter.UpsertMaterial(1, 1);
        miniLetter.UpsertMaterial(2, 1);
        await AddProductAsync(miniLetter);

        var maxiLetter = new ProductEntity("Maxi Letter", 2.50M, USER_USERNAME);
        maxiLetter.UpsertMaterial(1, 1);
        maxiLetter.UpsertMaterial(2, 5);
        maxiLetter.UpsertMaterial(3, 1);
        await AddProductAsync(maxiLetter);

        // Act   
        var found = await FindProductsAsync(new ProductsWithMaterialIdSpecification(stamp.Id));

        // Assert
        found.ShouldNotBeNull();
        found.Count().ShouldBe(2);
    }
}
