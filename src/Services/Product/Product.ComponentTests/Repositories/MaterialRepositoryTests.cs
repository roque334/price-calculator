namespace Product.ComponentTests.Repositories;

using System.Threading.Tasks;

using Product.Application.Specifications;
using Product.Domain.Models.MaterialAggregate;

using NUnit.Framework;

using Shouldly;

using static Product.ComponentTests.Testing;
using static Product.ComponentTests.Constants;

[TestFixture]
public class MaterialRepositoryTests : TestBase
{
    [Test]
    public async Task Create_HappyFlow_CreatesMaterialEntity()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);

        // Act
        var created = await AddMaterialAsync(material);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(material.PricePerMeasurementUnit);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(0);
    }

    [Test]
    public async Task Create_MaterialEntityAlreadyExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateException>(() => AddMaterialAsync(material));
    }

    [Test]
    public async Task Update_HappyFlow_UpdatesMaterialEntity()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);

        // Act
        var updated = await UpdateMaterialAsync(created);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(material.PricePerMeasurementUnit);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(0);
    }

    [Test]
    public async Task Update_MaterialEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);
        var updated = await DeleteMaterialAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateMaterialAsync(updated));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Update_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[materials] SET PricePerMeasurementUnit = 0.25, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => UpdateMaterialAsync(created));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(0.25M);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(2);
    }

    [Test]
    public async Task Delete_HappyFlow_DeletesMaterialEntity()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);

        // Act
        var updated = await DeleteMaterialAsync(created);

        // Assert
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_MaterialEntityDoesNotExists_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);
        var updated = await DeleteMaterialAsync(created);

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteMaterialAsync(updated));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(updated.Id, updated.UserName));

        found.ShouldBeNull();
    }

    [Test]
    public async Task Delete_ConcurrencyIssue_ThrowsDbUpdateConcurrencyException()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);
        ExecuteSqlInterpolated($" UPDATE [test].[dbo].[materials] SET PricePerMeasurementUnit = 0.25, Version = 2  WHERE Id = {created.Id}");

        // Act + Assert
        await Should.ThrowAsync<Microsoft.EntityFrameworkCore.DbUpdateConcurrencyException>(() => DeleteMaterialAsync(created));

        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(0.25M);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(2);
    }

    [Test]
    public async Task Find_MaterialWithIdAndUsernameSpecification_FindsMaterialEntity()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);

        // Act    
        var found = await FindMaterialAsync(new MaterialWithIdAndUsernameSpecification(created.Id, created.UserName));

        // Assert
        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(material.PricePerMeasurementUnit);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(0);
    }

    [Test]
    public async Task Find_MaterialWithNameAndUsernameSpecification_FindsMaterialEntity()
    {
        // Arrange
        var material = new MaterialEntity(1, "Envelope", "Units", 1.50M, USER_USERNAME, 0);
        var created = await AddMaterialAsync(material);

        // Act    
        var found = await FindMaterialAsync(new MaterialWithNameAndUsernameSpecification(created.Name, created.UserName));

        // Assert
        found.ShouldNotBeNull();
        found.Id.ShouldBe(material.Id);
        found.Name.ShouldBe(material.Name);
        found.PricePerMeasurementUnit.ShouldBe(material.PricePerMeasurementUnit);
        found.UserName.ShouldBe(material.UserName);
        found.Version.ShouldBe(0);
    }
}
