namespace Product.ComponentTests;
using System;
using System.Threading.Tasks;

using NUnit.Framework;

using static Product.ComponentTests.Testing;

[TestFixture]
public class TestBase
{
    [SetUp]
    public async Task SetUp()
    {
        await ResetState();
    }

    protected static T Execute<T>(Func<T> function, TimeSpan timeout, Func<T> onTimeout)
    {
        var task = Task.Run(function);
        if (task.Wait(timeout))
        {
            return task.Result;
        }
        else
        {
            return onTimeout();
        }
    }
}
