namespace Product.ComponentTests.Autofixture.Extensions;

using AutoFixture;
using AutoFixture.Dsl;

using Product.ComponentTests.Autofixture.SpecimenBuilders;

public static class FixtureExtensions
{
    public static void FreezeConstructor<T>(this Fixture fixture, object parameters)
    {
        var builder = new AutoFixtureCustomConstructorBuilder<T>();
        var propertiesInfo = parameters.GetType().GetProperties();
        foreach (var propertyInfo in propertiesInfo)
        {
            builder.AddParameters(propertyInfo.Name, propertyInfo.GetValue(parameters));
        }

        fixture.Customize((ICustomizationComposer<T> _) => builder);
    }

    public static T CreateWithFrozenConstructor<T>(this Fixture fixture, object parameters)
    {
        fixture.FreezeConstructor<T>(parameters);
        var result = fixture.Create<T>();
        return result;
    }
}

