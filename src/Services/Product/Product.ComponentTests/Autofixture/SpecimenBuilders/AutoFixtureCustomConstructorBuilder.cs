namespace Product.ComponentTests.Autofixture.SpecimenBuilders;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;

using AutoFixture.Kernel;

public class AutoFixtureCustomConstructorBuilder<T> : ISpecimenBuilder
{
    private readonly Dictionary<string, object> _ctorParameters = new();

    public object Create(object request, ISpecimenContext context)
    {
        var typeFromBuilder = typeof(T);
        var seededRequest = request as SeededRequest;
        if (seededRequest == null || seededRequest.Request.Equals(typeFromBuilder))
        {
            return new NoSpecimen();
        }

        var constructorInfo = typeFromBuilder.GetConstructors(BindingFlags.Instance | BindingFlags.Public).FirstOrDefault();
        if (constructorInfo == null)
        {
            return new NoSpecimen();
        }

        var nonExistingCtorParams = _ctorParameters.Keys.Except<string>(constructorInfo.GetParameters().Select(x => x.Name));
        if (nonExistingCtorParams.Any())
        {
            throw new InvalidOperationException($"The following ctor parameters were not found on '{typeFromBuilder.FullName}': {string.Join(",", nonExistingCtorParams)}");
        }

        var finalCtorParams = constructorInfo.GetParameters().Select(x => _ctorParameters.ContainsKey(x.Name) ? _ctorParameters[x.Name] : context.Resolve(x.ParameterType)).ToArray();

        var result = constructorInfo.Invoke(BindingFlags.CreateInstance, null, finalCtorParams, CultureInfo.InvariantCulture);
        return result;
    }

    public void AddParameters(string parameterName, object parameterValue)
    {
        _ctorParameters.Add(parameterName, parameterValue);
    }
}

