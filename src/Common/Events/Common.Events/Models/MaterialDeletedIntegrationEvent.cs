namespace Common.Events.Models;

using Core.EventBus.Domain.Events;

public class MaterialDeletedIntegrationEvent : IntegrationEvent
{
    public int MaterialId { get; set; }
    public string Name { get; set; }
    public string UserName { get; set; }

    public MaterialDeletedIntegrationEvent(int materialId, string name, string userName)
    {
        MaterialId = materialId;
        Name = name;
        UserName = userName;
    }
}
