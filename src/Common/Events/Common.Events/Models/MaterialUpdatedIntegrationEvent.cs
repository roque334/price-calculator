namespace Common.Events.Models;

using Core.EventBus.Domain.Events;

public class MaterialUpdatedIntegrationEvent : IntegrationEvent
{
    public int MaterialId { get; set; }
    public string Name { get; set; }
    public string MeasurementName { get; set; }
    public decimal PricePerMeasurementUnit { get; set; }
    public string UserName { get; set; }
    public long Version { get; set; }

    public MaterialUpdatedIntegrationEvent(int materialId, string name, string measurementName, decimal pricePerMeasurementUnit, string userName, long version)
        : base()
    {
        MaterialId = materialId;
        Name = name;
        MeasurementName = measurementName;
        PricePerMeasurementUnit = pricePerMeasurementUnit;
        UserName = userName;
        Version = version;
    }
}
