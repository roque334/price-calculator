namespace Core.Domain.Models;

public interface IRepository<T> where T : IAgreggateRoot
{
    IUnitOfWork UnitOfWork { get; }
}
