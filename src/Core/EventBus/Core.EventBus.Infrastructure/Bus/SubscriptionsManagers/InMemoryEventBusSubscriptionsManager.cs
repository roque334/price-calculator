namespace Core.EventBus.Infrastructure.Bus.SubscriptionsManagers;

using System;
using System.Collections.Generic;
using System.Linq;

using Core.EventBus.Domain.Bus;
using Core.EventBus.Domain.Events;
using Core.EventBus.Domain.Handlers;

public class InMemoryEventBusSubscriptionsManager : IEventBusSubscriptionsManager
{
    private readonly Dictionary<string, List<SubscriptionInfo>> _handlers;

    private readonly List<Type> _eventTypes;

    public event EventHandler<string> OnEventRemoved;

    public InMemoryEventBusSubscriptionsManager()
    {
        _handlers = new Dictionary<string, List<SubscriptionInfo>>();
        _eventTypes = new List<Type>();
    }

    public void AddSubscription<T, TH>()
        where T : IntegrationEvent
        where TH : IIntegrationEventHandler<T>
    {
        var eventName = GetEventName<T>();
        var eventHandlerType = typeof(TH);

        // Validate whether the event already exists
        if (!HasSubscriptionsForEvent(eventName))
        {
            _handlers[eventName] = new List<SubscriptionInfo>();
        }

        // Validate whether the event handler already exists
        if (_handlers[eventName].Any(s => s.HandlerType == eventHandlerType))
        {
            throw new Exception($"Handler Type {eventHandlerType.Name} already registered for '{eventName}'");
        }

        // Add event handler to event key
        _handlers[eventName].Add(new SubscriptionInfo(eventHandlerType));

        // Validate
        if (!_eventTypes.Contains(typeof(T)))
        {
            _eventTypes.Add(typeof(T));
        }
    }

    public void Clear() => _handlers.Clear();

    public string GetEventName<T>() => typeof(T).Name;

    public Type GetEventTypeByName(string eventName) =>
        _eventTypes.SingleOrDefault(t => t.Name == eventName);

    public IEnumerable<SubscriptionInfo> GetHandlersForEvent<T>() where T : IntegrationEvent
    {
        var eventName = GetEventName<T>();
        return GetHandlersForEvent(eventName);
    }

    public IEnumerable<SubscriptionInfo> GetHandlersForEvent(string eventName) => _handlers[eventName];

    public bool HasSubscriptionsForEvent<T>() where T : IntegrationEvent
    {
        var eventName = GetEventName<T>();
        return HasSubscriptionsForEvent(eventName);
    }

    public bool HasSubscriptionsForEvent(string eventName) => _handlers.ContainsKey(eventName);

    public bool isEmpty() => _handlers.Keys.Any();

    public void RemoveSubscription<T, TH>()
        where T : IntegrationEvent
        where TH : IIntegrationEventHandler<T>
    {
        var eventName = GetEventName<T>();
        var eventHandlerType = typeof(TH);
        SubscriptionInfo eventHandler = null;

        if (HasSubscriptionsForEvent(eventName))
        {
            eventHandler = _handlers[eventName].SingleOrDefault(s => s.HandlerType == eventHandlerType);
        }

        if (eventHandler != null)
        {
            _handlers[eventName].Remove(eventHandler);

            if (!_handlers[eventName].Any())
            {
                _handlers.Remove(eventName);

                var eventType = GetEventTypeByName(eventName);
                if (eventType != null)
                {
                    _eventTypes.Remove(eventType);
                }
                RaiseOnEventRemoved(eventName);
            }
        }
    }

    private void RaiseOnEventRemoved(string eventName)
    {
        var handler = OnEventRemoved;
        handler?.Invoke(this, eventName);
    }
}
