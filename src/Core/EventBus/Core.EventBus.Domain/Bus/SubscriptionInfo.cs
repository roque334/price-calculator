namespace Core.EventBus.Domain.Bus;

using System;

public class SubscriptionInfo
{
    public Type HandlerType { get; }

    public SubscriptionInfo(Type handlerType)
    {
        HandlerType = handlerType;
    }

}
