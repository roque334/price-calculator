namespace Core.EventBus.Domain.Handlers;

using System.Threading.Tasks;

using Core.EventBus.Domain.Events;

public interface IIntegrationEventHandler<in T> : IIntegrationEventHandler
    where T : IntegrationEvent
{
    Task Handle(T @event);
}

public interface IIntegrationEventHandler
{

}
